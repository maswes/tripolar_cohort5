%lteDLSCH Downlink shared channel
%   [CWOUT,CHINFO] = lteDLSCH(ENB,CHS,OUTLEN,TRBLKIN) applies the complete
%   Downlink Shared Channel (DL-SCH) transport channel coding chain to the
%   input data TRBLKIN and returns the codewords in CWOUT. The encoding
%   process includes type-24A CRC calculation, code block segmentation and
%   type-24B CRC attachment if any, turbo encoding, rate matching with
%   Redundancy Version (RV) and code block concatenation. Additional
%   information about the encoding process is returned in the fields of
%   structure CHINFO. The function is capable of processing both a single
%   transport block or pairs of blocks (contained in a cell array) for the
%   case of spatial multiplexing schemes transmitting two codewords. The
%   type of the return variable CWOUT will be the same as input TRBLKIN
%   i.e. if TRBLK is a cell array containing one or two transport blocks
%   then CWOUT will return a cell array of one or two codewords, and if
%   TRBLKIN is a vector of information bits then CWOUT will also return a
%   vector. If encoding a pair of transport blocks then pairs of modulation
%   schemes and RV indicators are required to be defined in the associated
%   parameter fields below.
%   
%   ENB is an input parameter structure containing the fields below. 
%   Only required if 'NSoftbits' is provided in CHS:
%      DuplexMode - Optional. Duplex mode ('FDD'(default),'TDD')
%   Only required for 'TDD' duplex mode:
%      TDDConfig  - Optional. Uplink/Downlink Configuration (0...6) 
%                   (default 0)
%   Only required for 'TxDiversity' transmission scheme (as defined in the 
%   parameter below):
%      CellRefP   - Number of cell-specific reference signal antenna ports
%                   (1,2,4).
%
%   CHS is an input parameter structure defining aspects of the Physical
%   Downlink Shared Channel (PDSCH) onto which the codeword(s) will be
%   mapped, and the DL-SCH soft buffer size and redundancy version(s) of
%   the generated codeword(s). The required fields are:
%   Modulation - Modulation type character vector or cell array of
%                character vectors (if 2 blocks) associated with each
%                transport block ('QPSK','16QAM','64QAM','256QAM')
%   NLayers    - Total number of transmission layers associated with the
%                transport block(s) (1..8)
%   TxScheme   - Optional. Transmission scheme, one of:
%                'Port0'       - Single-antenna port, Port 0 (default)
%                'TxDiversity' - Transmit diversity scheme
%                'CDD'         - Large delay CDD scheme
%                'SpatialMux'  - Closed-loop spatial multiplexing scheme
%                'MultiUser'   - Multi-user MIMO scheme
%                'Port5'       - Single-antenna port, Port 5
%                'Port7-8'     - Single-antenna port, port 7 (when 
%                                NLayers=1); Dual layer transmission, port
%                                7 and 8 (when NLayers=2)
%                'Port8'       - Single-antenna port, Port 8
%                'Port7-14'    - Up to 8 layer transmission, ports 7-14
%   RV         - Vector of 1 or 2 redundancy version indicators (0,1,2,3)
%   NSoftbits  - Optional. Total number of soft buffer bits 
%                (default=0=no buffer limit)
%   
%   OUTLEN is an input vector (one or two elements in length) defining the
%   codeword length(s) that the input transport block(s) should be rate
%   matched to. It represents the PDSCH capacity for the associated
%   codeword and therefore the length(s) of the vector(s) in CWOUT.
%  
%   TRBLKIN is an input parameter containing the transport block(s)
%   information bits to be encoded. It can either be a single vector or a
%   cell array containing one or two vectors. If the latter, then all rate
%   matching calculations assume that the pair will be transmitting on a
%   single PDSCH, distributed across the total number of layers defined in
%   CHS, as per TS 36.211. Note that the lowest order information bit of
%   TRBLKIN should be mapped to the most significant bit of the transport
%   block as defined in section 6.1.1 of TS 36.321.
% 
%   CWOUT is the output parameter containing the DL-SCH encoded codewords.
%   It is either a single vector or a cell array containing one or two
%   vectors depending on the type of the input data TRBLKIN.
%
%   The optional output CHINFO is a structure array containing code block
%   segmentation and rate matching related parameters:
%   C       - Total number of code blocks
%   Km      - Lower code block size (K-)
%   Cm      - Number of code blocks of size Km (C-)
%   Kp      - Upper code block size (K+)
%   Cp      - Number of code blocks of size Kp (C+)
%   F       - Number of filler bits in first block
%   L       - Number of segment CRC bits
%   Bout    - Total number of bits in all segments
%   NLayers - Number of layers associated with transport block/codeword
%   NL      - Number of layers related variable used in rate matching 
%             calculation
%   Qm      - Bits per symbol variable used in rate matching calculation
%   NIR     - Number of soft bits associated with transport block
%   RV      - RV value associated with one codeword (if RV present 
%             at input)
%   
%   Note that if two transport blocks are encoded then CHINFO will be an
%   structure array of two elements, one for each block. Note that code
%   block segmentation fields in this structure can also be created
%   independently using the <a href="matlab: help('lteDLSCHInfo')">lteDLSCHInfo</a> function.
%
%   Example:
%   % Generates the DL-SCH codeword as defined by TS 36.101 RMC R.7 for FDD
%   % duplexing mode
%   
%   rmc = lteRMCDL('R.7');
%   data = randi([0,1],rmc.PDSCH.TrBlkSizes(1),1);
%   codeWord = lteDLSCH(rmc,rmc.PDSCH,rmc.PDSCH.CodedTrBlkSizes(1),data);
%   codeWord(1:10)
%
%   See also lteDLSCHDecode, lteDLSCHInfo, ltePDSCH.

%   Copyright 2009-2016 The MathWorks, Inc.

function [encbits, chinfo] = lteDLSCHCustom(enb,dch,outblklen,trblks,epsilon)
    
    % Validate the number of input arguments
    narginchk(4,5);
    
    % Validate any optional parameters used directly in the MATLAB code
    dch = mwltelibrary('validateLTEParameters',dch,'TxScheme');
    
    % Establish if the input transport data is in a cell array, and if not,
    % place it in a cell array for uniform processing later
    cellout = iscell(trblks);
    if (~cellout)
        trblks = {trblks};
    end
    
    % Validate the input arguments
    outblklen = validateInputs(dch,outblklen,trblks);
    
    % Create the informational output 'chinfo'
    chinfo = lteDLSCHInfo(enb,dch,cellfun(@length,trblks));
    
    % Calculate the parameters for encoding each codeword
    chs = createCodewordParameters(dch,chinfo);
    
    % Encode each codeword
    ncw = length(trblks);
    encbits = cell(1,ncw);
    for i = 1:ncw
        encbits{i} = encode(chs(i),outblklen(i),trblks{i},epsilon);
    end
    
    % If the input transport data was not in a cell array, remove the cell
    % array on the output codeword
    if (~cellout)
        encbits = encbits{1};
    end
    
end

% Encode a single codeword
function cw = encode(chs,outlen,trblkin, epsilon)
    
    % Transport block CRC attachment
    crced = lteCRCEncode(trblkin,'24A');
    
%     % Code block segmentation and code block CRC attachment
%     segmented = lteCodeBlockSegment(crced);   
%     
%     % Channel coding
%     coded = lteTurboEncode(segmented);
%     
%     % Rate matching
%     cw_turbo = lteRateMatchTurbo(coded,outlen,chs.RV,chs);
%     
%     % cw = kron(crced, ones(3,1));
%     nrep = repmat(crced,1,3)';
%     nrep = nrep(:);
%     cw_nrep = [nrep; zeros(outlen-length(nrep),1)];
    
    % N=256; K=128;
    N=pow2(floor(log2(double(outlen))));
    K = length(crced);
%     designSNRdB = 0;
%     initPC(N,K,1,2, designSNRdB,1); %silent, no output
%     
%     padded = [crced; zeros(K-length(crced),1)];    
%     cw_polar = pencode(padded);
%     cw_polar = [cw_polar; zeros(outlen-N,1)];    

    if nargin < 4
      epsilon = 0.65;    
    end
    
    Ns = 4096;    
    cw_polarlsc = [];

    if (N > Ns)
        
        numSubBlock = floor(outlen ./ Ns);    
        Ks = ceil(K ./ numSubBlock);

        crced = [crced; randi([0 1], numSubBlock*Ks-K,1)];          
        
        % Add CRC per sub-block
        crc_size = 8;        
        polar_coder = PolarCode(Ns, Ks, epsilon, crc_size);        
        for i = 1:numSubBlock
            segment = crced((i-1)*Ks+1:i*Ks).';
            coded = polar_coder.encode(double(segment)).';
            cw_polarlsc = [cw_polarlsc; coded];
        end
    elseif N > 0      
        crc_size = 0;%24;
        polar_coder = PolarCode(N, K, epsilon, crc_size);
        cw_polarlsc = polar_coder.encode(double(crced')).';
    end
    
    % Rate matching
    %cw_polarlsc_rm = lteRateMatchTurbo(cw_polarlsc,outlen,chs.RV,chs);    
    cw_polarlsc = [cw_polarlsc; randi([0 1], outlen-length(cw_polarlsc),1)];          
    
    %cw = cw_nrep;
    %cw = cw_polar;
    cw = cw_polarlsc;
    %cw = cw_polarlsc_rm;
end

% Validate the input arguments
function outblklen = validateInputs(dch,outblklen,trblks)

    % Check that relevant parameters are the cell type
    if ~iscell(dch.Modulation)
        dch.Modulation = {dch.Modulation};
    end
    
    % Check that the number of transport blocks presented does not exceed 2
    if length(trblks)>2
        error('lte:error','The number of transport block vectors must be 1 or 2.');
    end
    
    % Check that input cell array (parameter) lengths are the same
    if length(dch.Modulation) < length(trblks)
        error('lte:error','The number of modulation types does not match the number of transport block vectors.');
    end
    if length(outblklen) ~= length(trblks)
        error('lte:error','The number of output codeword lengths (size of OUTLEN) does not match the number of transport block vectors.');
    end
    
    % If the block lengths were parameterized as a cell array then convert to a vector
    if (iscell(outblklen))
        outblklen = cell2mat(outblklen);
    end
    
end

% Create parameters for each codeword, in a structure array 'chs'
function chs = createCodewordParameters(dch,chinfo)

    % The 'modulations' array has modulation strings in the positions of
    % the corresponding number of bits per symbol 'Qm'
    modulations = {'' 'QPSK' '' '16QAM' '' '64QAM' '' '256QAM'};
    modulation = modulations([chinfo.Qm]);
    [chs(1:numel(chinfo)).Modulation] = modulation{:};
    [chs(:).TxScheme] = deal(dch.TxScheme);
    [chs(:).RV] = chinfo.RV;
    [chs(:).NIR] = chinfo.NIR;
    [chs(:).NLayers] = chinfo.NLayers;
    
end
