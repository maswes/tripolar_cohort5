%lteRMCDL Downlink reference measurement channel configuration
%   RMCCFGOUT = lteRMCDL(...) returns RMCCFGOUT, a structure containing the
%   configuration parameters required to generate a given reference channel
%   waveform using the RMC generator tool <a
%   href="matlab:help('lteRMCDLTool')">lteRMCDLTool</a>. The field names
%   and default values are in accordance with the definition found in
%   TS 36.101 Annex A.3.
%
%   RMCCFGOUT = lteRMCDL(RC,DUPLEXMODE,TOTSUBFRAMES) returns RMCCFGOUT, a
%   configuration structure for the reference channel defined by RC using a
%   channel-specific default configuration (for details, see <a
%   href="matlab:doc('lteRMCDL')">lteRMCDL</a>).
%   DUPLEXMODE and TOTSUBFRAMES are optional input parameters which define
%   the duplexing mode and total number of subframes to be generated.
%
%   The input RC identifies the reference measurement channel number as per
%   TS 36.101, must be one of following:
%   ('R.0','R.1','R.2','R.3','R.4','R.5','R.6','R.7','R.8','R.9','R.10',
%   'R.11','R.12','R.13','R.14','R.25','R.26','R.27','R.28','R.31-3A', 
%   'R.31-4','R.43','R.44','R.45','R.45-1','R.48','R.50','R.51','R.6-27RB',
%   'R.12-9RB','R.11-45RB')
%   Note that RCs 'R.31-3A' and 'R.31-4' are sustained data rate RMCs with
%   user data in subframe 5 and RC ('R.6-27RB','R.12-9RB','R.11-45RB') are
%   custom RMCs configured for non-standard bandwidths but with the same
%   code rate as the standardized versions.
%   
%   DUPLEXMODE is an optional input specifying the duplexing mode
%   ('FDD'(default),'TDD'). For 'R.25', 'R.26', 'R.27' and 'R.28', the
%   default duplexing mode is 'TDD'.
%   
%   TOTSUBFRAMES is an optional input specifying the total number of
%   subframes (default 10) that form the resource grid used by <a href="matlab: help('lteRMCDLTool')">lteRMCDLTool</a>
%   to generate the waveform.
%   
%   The output structure RMCCFGOUT contains the following fields:
%   RC            - Reference channel number
%   NDLRB         - Number of downlink resource blocks
%   CellRefP      - Number of cell-specific reference signal antenna ports 
%                   (1,2,4)
%   NCellID       - Physical layer cell identity
%   CyclicPrefix  - Cyclic prefix length ('Normal','Extended')
%   CFI           - Control format indicator (1,2,3) value. This can be a 
%                   scalar, or a vector of length 10 (corresponding to a
%                   frame) if the CFI varies per subframe. CFI varies per
%                   subframe for the RMCs ('R.0', 'R.5', 'R.6', 'R.6-27RB',
%                   'R.12-9RB') in TDD mode (see note below)
%   PCFICHPower   - PCFICH symbol power in dB (default 0)
%   Ng            - HICH group multiplier ('Sixth','Half','One','Two')
%   PHICHDuration - PHICH duration ('Normal','Extended')
%   HISet         - The HISET (default 112-by-3 matrix containing the  
%                   maximum PHICH groups (112) as per TS 36.211 Section 6.9
%                   with the first PHICH sequence of each group set to
%                   ACK). See <a href="matlab:help('ltePHICH')">ltePHICH</a> for details
%   PHICHPower    - PHICH symbol power in dB (default 0)
%   NFrame        - Initial frame number
%   NSubframe     - Initial subframe number
%   TotSubframes  - Total number of subframes to be generated
%   Windowing     - The number of time-domain samples over which windowing
%                   and overlapping of OFDM symbols is applied
%   DuplexMode    - Duplex mode ('FDD','TDD')
%   TDDConfig     - Present only for TDD duplex mode. Uplink/Downlink
%                   Configuration (0...6)
%   SSC           - Present only for TDD duplex mode. Special Subframe
%                   Configuration (0...9)
%   CSIRSConfig   - Present only for Port7-14 transmission scheme. The 
%                   CSI-RS configuration index
%   CSIRSPeriod   - Present only for Port7-14 transmission scheme. The 
%                   CSI-RS subframe configuration
%   CSIRefP       - Present only for Port7-14 transmission scheme. The 
%                   number of CSI-RS antenna ports (1,2,4,8)
%   ZeroPowerCSIRSPeriod - Present only for Port7-14 transmission scheme.
%                          The Zero power CSI-RS subframe configuration
%   ZeroPowerCSIRSConfig - Present only for Port7-14 transmission scheme.
%                          The zero power CSI-RS configuration index list
%   PDSCH           - PDSCH transmission configuration sub-structure
%   OCNGPDCCHEnable - Enable PDCCH OCNG ('Off'(default),'On')  
%   OCNGPDCCHPower  - PDCCH OCNG power in dB (default 0)
%   OCNGPDSCHEnable - Enable PDSCH OCNG ('Off'(default),'On')
%   OCNGPDSCHPower  - PDSCH OCNG power in dB. Defaults to PDSCH.Rho 
%   OCNGPDSCH       - PDSCH OCNG configuration sub-structure 
%   OCNG            - Deprecated. Enable PDSCH OCNG ('Off','Disable','On',
%                     'Enable'). This parameter is deprecated and will
%                     be removed in a later release. Use the above
%                     parameters instead.
% 
%   PDSCH is a sub-structure relating to the physical channel
%   configuration and contains the following fields:
%   PDSCH.TxScheme  - Transmission scheme, will be one of:
%                     'Port0'       - Single-antenna port, Port 0
%                     'TxDiversity' - Transmit diversity scheme
%                     'CDD'         - Large delay CDD scheme
%                     'SpatialMux'  - Closed-loop spatial multiplexing 
%                                     scheme
%                     'MultiUser'   - Multi-user MIMO scheme
%                     'Port5'       - Single-antenna port, Port 5
%                     'Port7-8'     - Single-antenna port, port 7 
%                                     (when NLayers=1); Dual layer
%                                     transmission, port 7 and 8
%                                     (when NLayers=2)
%                     'Port8'       - Single-antenna port, Port 8
%                     'Port7-14'    - Up to 8 layer transmission, ports
%                                     7-14
%   PDSCH.Modulation - A cell array of one or two character vectors
%                      specifying the modulation formats for one or two
%                      codewords ('QPSK','16QAM','64QAM','256QAM')
%   PDSCH.NLayers   - Number of transmission layers (1...8)
%   PDSCH.NTxAnts   - Number of transmission antenna ports (1 or more)
%                     (only present for UE-Specific demodulation 
%                     reference symbols RMCs)
%   PDSCH.Rho       - PDSCH resource element power allocation in dB 
%                     (default 0)
%   PDSCH.RNTI      - Radio Network Temporary Identifier (16-bit)
%   PDSCH.RVSeq     - A 1- or 2-row matrix (for one or two codewords) 
%                     specifying the sequence of Redundancy Version (RV) 
%                     indicators for each HARQ process. The number of 
%                     elements in each row is equal to the number of 
%                     transmissions in each HARQ process. If RVSeq is a row
%                     vector in a two codeword transmission then the same 
%                     RV sequence is applied to both codewords
%   PDSCH.RV        - A 1- or 2-column vector, specifying the redundancy
%                     version for one or two codewords used in the initial
%                     subframe number (NSubframe). This parameter field is
%                     only for informational purposes and is Read-Only.
%   PDSCH.NHARQProcesses - Number of HARQ processes (1...8)
%   PDSCH.NTurboDecIts   - Number of turbo decoder iteration cycles 
%                          (default 5)
%   PDSCH.PRBSet    - The 0-based Physical Resource Block (PRB) indices
%                     corresponding to the slot wise resource allocations
%                     for this PDSCH. This parameter can be a column
%                     vector, or cell array of length 10 (corresponding to
%                     a frame) if allocated physical resource blocks vary
%                     across subframes. PRBSet varies per subframe for the
%                     RMCs 'R.25'(TDD), 'R.26'(TDD), 'R.27'(TDD),
%                     'R.31-3A'(FDD), 'R.31-4', 'R.43'(FDD), 'R.44',
%                     'R.45', 'R.48', 'R.50' and 'R.51'
%   PDSCH.TargetCodeRate  - A scalar or 1- or 2-row matrix (corresponding  
%                           to 1 or 2 codewords) of target code rate(s) for
%                           calculating the transport block sizes as given
%                           by TS 36.101 Annex A.3.1. This will be equal to
%                           the ActualCodeRate parameter below if
%                           TargetCodeRate and TrBlkSizes are both not
%                           provided at the input and the RC does not have
%                           a single ratio target code rate in TS 36.101
%                           Table A.3.1.1-1
%   PDSCH.ActualCodeRate  - A 1- or 2-row matrix (corresponding to 1 or 2 
%                           codewords respectively) showing the actual code
%                           rates (max 0.93) for each subframe in a frame
%                           calculated according to TS 36.101 Annex A.3.1.
%                           This parameter field is only for information
%                           purposes and is Read-Only
%   PDSCH.TrBlkSizes      - A 1- or 2-row matrix, specifying the transport 
%                           block sizes for 1 or 2 codewords used in the
%                           transmission
%   PDSCH.CodedTrBlkSizes - A 1- or 2-row matrix, representing the coded 
%                           transport block sizes for 1 or 2 codewords.
%                           This parameter field is only for informational
%                           purposes and is Read-Only
%   PDSCH.DCIFormat       - DCI Format of PDCCH associated with PDSCH. See 
%                           <a href="matlab:help('lteDCI')">lteDCI</a> for details
%   PDSCH.PDCCHFormat     - Aggregation level of PDCCH associated with
%                           PDSCH (0,1,2,3)
%   PDSCH.PDCCHPower      - PDCCH power in dB
%   PDSCH.CSIMode   - CSI reporting mode
%   PDSCH.PMIMode   - PMI reporting mode 
%   PDSCH.PMISet    - Only available for 'SpatialMux' scheme. A vector of 
%                     precoder matrix indications. The vector may contain
%                     either a single value (corresponding to single PMI
%                     mode) or multiple values (corresponding to multiple
%                     or subband PMI mode)
%   PDSCH.NSCID     - Present only for 'Port7-8', 'Port8' and 'Port7-14' 
%                     transmission schemes. Scrambling identity 
%   PDSCH.W         - Present only for UE-specific beamforming ('Port5',
%                     'Port7-8','Port8','Port7-14'). The NLayers-by-NTxAnts
%                     precoding matrix chosen according to TS 36.101 Annex
%                     B.4. The resulting precoding matrix with index 0 is 
%                     selected from the set defined in TS 36.211 Section
%                     6.3.4 for 'Port5', 'Port7-8' and 'Port8' transmission
%                     schemes or from the set associated with CSI reporting
%                     as defined in TS 36.213 Section 7.2.4 for the
%                     'Port7-14' transmission scheme. 
%   
%   OCNGPDSCH is a sub-structure defining the OFDMA channel noise generator
%   (OCNG) patterns according to TS 36.101 Section A.5 and the associated
%   RMCs/tests. The following OCNG parameter fields can also be customized
%   with the full range of PDSCH specific values:
%   OCNGPDSCH.Modulation - OCNG symbol modulation scheme 
%                          ('QPSK','16QAM','64QAM','256QAM')
%   OCNGPDSCH.TxScheme   - OCNG transmission scheme 
%   OCNGPDSCH.RNTI       - RNTI value for OCNG (default 0)
% 
%   The PDCCH OCNG will fill the unused PDCCH resource elements with QPSK
%   symbols using either single port or transmit diversity depending on the
%   number of cell RS ports.
% 
%   Note that CFI is equal to the symbols allocated to PDCCH - 1 for NDLRB
%   <10, and CFI is equal to the symbols allocated to PDCCH for NDLRB >=10.
%   For the RMCs, 2 symbols are allocated to PDCCH for 20 MHz, 15 MHz and
%   10 MHz channel bandwidths; 3 symbols are allocated for 5 MHz and 3 MHz;
%   4 symbols are allocated for 1.4 MHz. In the TDD mode, only 2 OFDM
%   symbols are allocated to PDCCH in special subframes irrespective of the
%   channel bandwidth and therefore the CFI value will vary per subframe
%   for the 5 MHz and 3 MHz and 1.4 MHz channel bandwidths.
% 
%   RMCCFGOUT = lteRMCDL(RMCCFG,NCODEWORDS) returns RMCCFGOUT, a
%   configuration structure for the reference channel partially (or wholly)
%   defined by the input structure RMCCFG. The input structure RMCCFG can
%   define any of the above listed parameters or sub-structure parameters
%   and the output structure RMCCFGOUT will return the same parameter
%   values. Any parameters missing at the input will be initialized based
%   on the 'RC' field if present or 'R.0' otherwise. If the 'RC' field is
%   absent or set to empty, all downlink and special (if TDD mode)
%   subframes are assumed to be scheduled, otherwise the sheduling is as
%   given by the RMC. TrBlkSizes and CodedTrBlkSizes are set according to
%   the target code rate, the modulation scheme and the allocated
%   resources. TargetCodeRate defaults to the value defined for the RMC if
%   not present in the input. The value of RVSeq is set according to the
%   modulation scheme.
%   
%   NCODEWORDS is an optional parameter defining the number of PDSCH
%   codeword(s) to be modulated (1,2). The default used is the value
%   defined in TS 36.101 for the RMC configuration given by RC.
%
%   Example 1: 
%   % Create a configuration structure for RC R.11 as
%   % specified in TS 36.101.
%   
%   rmc.RC = 'R.11';
%   rmc.NCellID = 100;
%   rmc.PDSCH.TxScheme = 'SpatialMux';
%   rmcOut = lteRMCDL(rmc,2)
%   rmcOut.PDSCH
%   rmcOut.OCNGPDSCH
%
%   % The above example returns:   
%   % rmcOut =
%   %
%   %              RC: 'R.11'
%   %           NDLRB: 50
%   %        CellRefP: 2
%   %         NCellID: 100
%   %    CyclicPrefix: 'Normal'
%   %             CFI: 2
%   %     PCFICHPower: 0
%   %              Ng: 'Sixth'
%   %   PHICHDuration: 'Normal'
%   %           HISet: [112x3 double]
%   %      PHICHPower: 0
%   %          NFrame: 0
%   %       NSubframe: 0
%   %    TotSubframes: 10
%   %       Windowing: 0
%   %      DuplexMode: 'FDD'
%   %           PDSCH: [1x1 struct]
%   % OCNGPDCCHEnable: 'Off'
%   %  OCNGPDCCHPower: 0
%   % OCNGPDSCHEnable: 'Off'
%   %  OCNGPDSCHPower: 0
%   %       OCNGPDSCH: [1x1 struct]
%   %
%   %    rmcOut.PDSCH:
%   %
%   %        TxScheme: 'SpatialMux'
%   %      Modulation: {'16QAM'  '16QAM'}
%   %         NLayers: 2
%   %             Rho: 0
%   %            RNTI: 1
%   %           RVSeq: [2x4 double]
%   %              RV: [0 0]
%   %  NHARQProcesses: 8
%   %    NTurboDecIts: 5
%   %          PRBSet: [50x1 double]
%   %  TargetCodeRate: 0.5000
%   %  ActualCodeRate: [2x10 double]
%   %      TrBlkSizes: [2x10 double]
%   % CodedTrBlkSizes: [2x10 double]
%   %       DCIFormat: 'Format2'
%   %     PDCCHFormat: 2
%   %      PDCCHPower: 0
%   %         CSIMode: 'PUSCH 3-1'
%   %         PMIMode: 'Wideband'
%   %          PMISet: 0
%   %
%   % rmcOut.OCNGPDSCH:
%   %
%   %            RNTI: 0
%   %      Modulation: 'QPSK'
%   %        TxScheme: 'TxDiversity'
%
%   Example 2: 
%   % Create a configuration structure for RC R.0 as
%   % specified in TS 36.101. For this RMC, the value of CFI varies
%   % per subframe.
%
%   rmcOut = lteRMCDL('R.0','TDD')
%	
%   % The above example returns: 
%   % rmcOut =
%   %
%   %              RC: 'R.0'
%   %           NDLRB: 15
%   %        CellRefP: 1
%   %         NCellID: 0
%   %    CyclicPrefix: 'Normal'
%   %             CFI: [3 2 3 3 3 3 2 3 3 3]
%   %     PCFICHPower: 0
%   %              Ng: 'Sixth'
%   %   PHICHDuration: 'Normal'
%   %           HISet: [112x3 double]
%   %      PHICHPower: 0
%   %          NFrame: 0
%   %       NSubframe: 0
%   %    TotSubframes: 10
%   %       Windowing: 0
%   %      DuplexMode: 'TDD'
%   %           PDSCH: [1x1 struct]
%   % OCNGPDCCHEnable: 'Off'
%   %  OCNGPDCCHPower: 0
%   % OCNGPDSCHEnable: 'Off'
%   %  OCNGPDSCHPower: 0
%   %       OCNGPDSCH: [1x1 struct]
%   %             SSC: 4
%   %       TDDConfig: 1
%
%   Example 3: 
%   % Create a configuration structure for RC R.44 as
%   % specified in TS 36.101. For this RMC, the resource allocation
%   % varies per subframe and the PRBSet is a cell array.
%
%   rmcOut = lteRMCDL('R.44')
%   rmcOut.PDSCH
%   rmcOut.PDSCH.PRBSet
%
%   % The above example returns:
%   % rmcOut =
%   %
%   %                   RC: 'R.44'
%   %                NDLRB: 50
%   %             CellRefP: 2
%   %              NCellID: 0
%   %         CyclicPrefix: 'Normal'
%   %                  CFI: 2
%   %          PCFICHPower: 0
%   %                   Ng: 'Sixth'
%   %        PHICHDuration: 'Normal'
%   %                HISet: [112x3 double]
%   %           PHICHPower: 0
%   %               NFrame: 0
%   %            NSubframe: 0
%   %         TotSubframes: 10
%   %            Windowing: 0
%   %           DuplexMode: 'FDD'
%   %                PDSCH: [1x1 struct]
%   %      OCNGPDCCHEnable: 'Off'
%   %       OCNGPDCCHPower: 0
%   %      OCNGPDSCHEnable: 'Off'
%   %       OCNGPDSCHPower: 0
%   %            OCNGPDSCH: [1x1 struct]
%   %              CSIRefP: 4
%   %          CSIRSPeriod: [5 1]
%   %          CSIRSConfig: 6
%   % ZeroPowerCSIRSPeriod: 'Off'
%   % ZeroPowerCSIRSConfig: '0010000000000000'
%   %
%   % rmcOut.PDSCH:
%   %
%   %             TxScheme: 'Port7-14'
%   %           Modulation: {'QPSK'}
%   %              NLayers: 1
%   %                  Rho: 0
%   %                 RNTI: 1
%   %                RVSeq: [0 1 2 3]
%   %                   RV: 0
%   %       NHARQProcesses: 8
%   %         NTurboDecIts: 5
%   %               PRBSet: {1x10 cell}
%   %       TargetCodeRate: 0.3333
%   %       ActualCodeRate: [1x10 double]
%   %           TrBlkSizes: [1x10 double]
%   %      CodedTrBlkSizes: [1x10 double]
%   %            DCIFormat: 'Format2C'
%   %          PDCCHFormat: 2
%   %           PDCCHPower: 0
%   %              CSIMode: 'PUSCH 3-1'
%   %              PMIMode: 'Wideband'
%   %                NSCID: 0
%   %                    W: [1x4 double]
%   %              NTxAnts: 4
%   %
%   % rmcOut.PDSCH.PRBSet:
%   %
%   %    Columns 1 through 3
%   %      [41x1 double]    [50x1 double]    [50x1 double]
%   %    Columns 4 through 6
%   %      [50x1 double]    [50x1 double]    []
%   %    Columns 7 through 9
%   %      [50x1 double]    [50x1 double]    [50x1 double]
%   %    Column 10
%   %      [50x1 double]
%
%   Example 4: 
%   % Create a new customized parameter set by overriding
%   % selected values of an existing preset RMC. Suppose you want to
%   % define a single codeword full-band 10MHz PDSCH using 4 CRS port
%   % spatial multiplexing, 64QAM modulation and 1/3 rate. Looking at TS 
%   % 36.101 Table A.3.1.1-1: Overview of DL reference measurement channels
%   % R.13 matches this criteria but with QPSK modulation:
%
%   rmcOverride.RC = 'R.13';
%   rmc = lteRMCDL(rmcOverride,1); 
%   rmc.PDSCH
%
%   % The above code returns:
%   % rmc.PDSCH =
%   %
%   %         TxScheme: 'SpatialMux'
%   %       Modulation: {'QPSK'}
%   %          NLayers: 1
%   %              Rho: 0
%   %             RNTI: 1
%   %            RVSeq: [0 1 2 3]
%   %               RV: 0
%   %   NHARQProcesses: 8
%   %     NTurboDecIts: 5
%   %           PRBSet: [50x1 double]
%   %   TargetCodeRate: 0.3333
%   %   ActualCodeRate: [0.3032 0.3450 0.3450 0.3450 0.3450 0 0.3450 0.3450
%   %                    0.3450 0.3450]
%   %       TrBlkSizes: [3624 4392 4392 4392 4392 0 4392 4392 4392 4392]
%   %  CodedTrBlkSizes: [12032 12800 12800 12800 12800 0 12800 12800 
%   %                    12800 12800]
%   %        DCIFormat: 'Format2'
%   %      PDCCHFormat: 2
%   %       PDCCHPower: 0
%   %          CSIMode: 'PUCCH 1-2'
%   %          PMIMode: 'Wideband'
%   %           PMISet: 0
%
%   % Now override the PDSCH modulation prior to calling lteRMCDL. The
%   % returned PDSCH transport block sizes and physical channel
%   % capacities will also be updated to maintain the same coding rate
%   % R=1/3:
%
%   rmcOverride.PDSCH.Modulation = '64QAM';
%   rmcOverride.PDSCH.TargetCodeRate = 1/3;
%   rmc = lteRMCDL(rmcOverride,1); 
%   rmc.PDSCH
%
%   % The above code returns:
%   % rmc.PDSCH =
%   %
%   %         TxScheme: 'SpatialMux'
%   %       Modulation: {'64QAM'}
%   %          NLayers: 1
%   %              Rho: 0
%   %             RNTI: 1
%   %            RVSeq: [0 0 1 2]
%   %               RV: 0
%   %   NHARQProcesses: 8
%   %     NTurboDecIts: 5
%   %           PRBSet: [50x1 double]
%   %   TargetCodeRate: 0.3333
%   %   ActualCodeRate: [0.4255 0.4000 0.4000 0.4000 0.4000 0 0.4000 0.4000 
%   %                    0.4000 0.4000]
%   %       TrBlkSizes: [15264 15264 15264 15264 15264 0 15264 15264 
%   %                    15264 15264]
%   %  CodedTrBlkSizes: [36096 38400 38400 38400 38400 0 38400 38400 
%   %                    38400 38400]
%   %        DCIFormat: 'Format2'
%   %      PDCCHFormat: 2
%   %       PDCCHPower: 0
%   %          CSIMode: 'PUCCH 1-2'
%   %          PMIMode: 'Wideband'
%   %           PMISet: 0
%
%   % Note that the RV sequence and actual code rates are also updated to
%   % reflect values used for 64QAM modulation. The actual code rate
%   % differs from the target code rate due to modulation order restriction 
%   % on transport block sizes.
%
%   See also lteRMCDLTool, lteRMCUL, lteTestModel.

%   Copyright 2009-2017 The MathWorks, Inc.

function RMCConfig = lteRMCDLCustom(varargin)

    narginchk(1, 3);

    if(isstruct(varargin{1}))
        % Getting input configuration structure
        rmc = varargin{1};

        rmc.tempRC = false;
        % Testing the presence of RC field
        if(~isfield(rmc,'RC')) || isempty(rmc.RC)
            rmc.tempRC = true;
            rmc.RC = 'R.0';
        end
        
        % Creating reference configuration structure
        refRMC = lteRMCDL(rmc.RC);

        if(nargin>2)
            configValidation = varargin{3};
        else
            configValidation = false;
        end

        % Getting RMC configuration for given inputs
        if(nargin > 1)
            NCodewords = varargin{2};
            if(~isnumeric(NCodewords) || (NCodewords~=1 && NCodewords~=2))
                error('lte:error','The function call resulted in an error: The number of codewords must be 1 or 2');
            end
            [RMCConfig,~,targetIsActual] = getRMCConfig(rmc,NCodewords,configValidation,refRMC);
        else
            [RMCConfig,NCodewords,targetIsActual] = getRMCConfig(rmc);
        end
    elseif(ischar(varargin{1}))
        % Validation of RC number
        rmc.RC = varargin{1};
        validateRMC(rmc.RC);
        rmc.tempRC = false;
        configValidation = false;
        
        if(nargin >= 2)
            rmc.DuplexMode = varargin{2};   % Read DuplexMode
        end
        if(nargin >= 3)
            rmc.TotSubframes = varargin{3}; % Read total subframes to generate
        end
        [RMCConfig,NCodewords,targetIsActual] = getRMCConfig(rmc);
    else
        error('lte:error','The function call resulted in an error: The first input argument is neither a structure or a character vector.');
    end

    if(configValidation)
        if(strcmpi(RMCConfig.PDSCH.TxScheme,'CDD'))
            if(NCodewords ~= 2)
                error('lte:error','The function call resulted in an error: The NCodewords should be 2 for "CDD" txScheme');
            end
        end

        if(strcmpi(RMCConfig.PDSCH.TxScheme,'SpatialMux') || strcmpi(RMCConfig.PDSCH.TxScheme,'MultiUser'))
            if(isfield(RMCConfig.PDSCH,'PMISet'))
                if(isempty(RMCConfig.PDSCH.PMISet))
                    error('lte:error','The function call resulted in an error: The vector of PMI set is empty');
                end
            else
                error('lte:error','The function call resulted in an error: Could not find a structure field called PMISet.');
            end
        end
    end

    % Set the modulation scheme according to the number of codewords
    modulation = RMCConfig.PDSCH.Modulation;
    if(~iscell(modulation))
        RMCConfig.PDSCH.Modulation = {modulation};
    end
    if(NCodewords == 2) && (numel(RMCConfig.PDSCH.Modulation) == 1)
        RMCConfig.PDSCH.Modulation = [RMCConfig.PDSCH.Modulation RMCConfig.PDSCH.Modulation];
    end

    % Configure the PRBSet
    if(~isfield(RMCConfig.PDSCH,'PRBSet'))
        RMCConfig.PDSCH.PRBSet = getPRBSet(RMCConfig);
    end

    % Calculate the TrBlkSizes, read-only CodedTrBlkSizes and ActualCodeRate
    RMCConfig = getBlkSizesAndRatesCustom(RMCConfig,targetIsActual);

    RMCConfig = rearrangeFields(RMCConfig);
    % Now remove the temporary variables
    if(RMCConfig.tempRC)
        RMCConfig = rmfield(RMCConfig,'RC');
    end
    RMCConfig = rmfield(RMCConfig,'tempRC');
end


%% ------------------------------------------------------------------------
%        Helper Functions
%  ------------------------------------------------------------------------

function [rmc,Ncodewords,targetIsActual] = getRMCConfig(rmc,ncw,validateConfig,varargin)
    %This function returns configuration for given RMC number, cell-wide
    %settings, modulation parameter and number of codewords
    rmcNo = rmc.RC;

    customRmcNo = {'R.6-27RB','R.12-9RB','R.11-45RB'};
    custom = any(strcmpi(rmc.RC,customRmcNo));

    if (~isfield(rmc,'DuplexMode'))
        if(isUeRS(rmcNo))
            rmc.DuplexMode = 'TDD';
        else
            rmc.DuplexMode = 'FDD';
        end
    end
    
    if(strcmpi(rmc.DuplexMode,'TDD'))
        if(~isfield(rmc,'TDDConfig'))
            rmc.TDDConfig = 1;
        end
        if(~isfield(rmc,'SSC'))
            rmc.SSC = 4;
        end
    end
    
    % Get NCW and transmission scheme. The choice of transmission scheme also
    % depends on the number of codewords. So if number of codewords is
    % specified by user, use that value
    if(nargin > 1)
        Ncodewords = ncw;
    else
        Ncodewords = [];
    end
    if ~(isfield(rmc,'PDSCH') && isfield(rmc.PDSCH,'TxScheme'))
        rmc.PDSCH.TxScheme = getTxScheme(rmcNo,rmc.DuplexMode,Ncodewords);
    end
    if(nargin == 1)
        % If no user specified number of codewords
        Ncodewords = getNcodewords(rmcNo,rmc.PDSCH.TxScheme);
    end

    % validating the combination of rmcNo, txScheme and number of codewords
    if nargin<3
        validateConfig = false;
    end
    if(validateConfig)
        if(isfield(rmc.PDSCH,'TxScheme'))
            validateTxScheme(rmcNo,rmc.PDSCH.TxScheme,Ncodewords);
        end
    end
    
    % Check if TrBlkSizes is present and TargetCodeRate is absent. If so,
    % the target code rate is the actual code rate
    if isfield(rmc.PDSCH,'TrBlkSizes') && ~isfield(rmc.PDSCH,'TargetCodeRate')
        targetIsActual = true;
    else
        targetIsActual = false;
    end
    
    if(~isfield(rmc,'PCFICHPower'))
        rmc.PCFICHPower = 0;
    end
    
    if(~isfield(rmc,'PHICHPower'))
        rmc.PHICHPower = 0;
    end

    if(~isfield(rmc.PDSCH,'Rho'))
        rmc.PDSCH.Rho = 0;
    end

    % Setting default values to parameters
    if(~isfield(rmc,'NSubframe'))
        rmc.NSubframe = 0;
    end

    if(~isfield(rmc,'NFrame'))
        rmc.NFrame = 0;
    end

    % If the user provided deprecated 'OCNG' parameter, issue the deprecated
    % warning and map the value to the new interface for OCNG setup 
    if isfield(rmc,'OCNG') 
        warning('lte:deprecated',['''OCNG'' parameter is deprecated and will be '...
            'removed in a later release. Use the ''OCNGPDSCHEnable'' and '...
            '''OCNGPDCCHEnable'' parameters instead. Additional OCNG parameterization '...
            'is provided by the OCNGPDSCHPower, OCNGPDCCHPower and OCNGPDSCH parameters.']);

        rmc.OCNGPDSCHEnable = rmc.OCNG; 
        if strcmpi(rmc.OCNG,'Enable')
            rmc.OCNGPDSCHEnable = 'On';
        end
        if strcmpi(rmc.OCNG,'Disable')
            rmc.OCNGPDSCHEnable = 'Off';
        end
        rmc.OCNGPDSCH = struct('RNTI',0,'Modulation','QPSK');

        % Set the PDCCH OCNG to 'Off' for backward compatibility
        rmc.OCNGPDCCHEnable = 'Off';

        % Now remove the deprecated parameter
        rmc = rmfield(rmc,'OCNG');
    end

    if(~isfield(rmc,'OCNGPDSCHEnable'))
        rmc.OCNGPDSCHEnable = 'Off';
    end

    if(~isfield(rmc,'OCNGPDSCHPower'))
        rmc.OCNGPDSCHPower = rmc.PDSCH.Rho;
    end

    % Define the OCNGPDSCH to be a structure if not
    if isfield(rmc,'OCNGPDSCH') && ~isstruct(rmc.OCNGPDSCH)
        rmc.OCNGPDSCH = struct();
    end

    if(~(isfield(rmc,'OCNGPDSCH') && isfield(rmc.OCNGPDSCH,'RNTI')) || isempty(rmc.OCNGPDSCH.RNTI))
        rmc.OCNGPDSCH.RNTI = 0;
    end

    if(~isfield(rmc.OCNGPDSCH,'Modulation'))
        rmc.OCNGPDSCH.Modulation = 'QPSK';
    end

    if(~isfield(rmc,'OCNGPDCCHEnable'))
        rmc.OCNGPDCCHEnable = 'Off';
    end

    if(~isfield(rmc,'OCNGPDCCHPower'))
        rmc.OCNGPDCCHPower = 0;
    end

    if(~isfield(rmc,'TotSubframes'))
        rmc.TotSubframes = 10;
    end

    if(~isfield(rmc,'NCellID'))
        if(custom)
            rmc.NCellID = 10;
        else
            rmc.NCellID = 0;
        end
    end

    if(~isfield(rmc,'Windowing'))
        rmc.Windowing = 0;
    end

    switch upper(rmcNo)
        case 'R.0'            
            enbConfig.NDLRB = 15;
            enbConfig.CyclicPrefix = 'Normal';
            enbConfig.PHICHDuration = 'Normal';
            enbConfig.Ng = 'Sixth';
            enbConfig.CellRefP = 1;
            PDSCH.NLayers = 1;
            PDSCH.Modulation = '16QAM';
            PDSCH.TargetCodeRate = 1/2;
        case 'R.1'
            enbConfig.NDLRB = 50;
            enbConfig.CyclicPrefix = 'Normal';
            enbConfig.PHICHDuration = 'Normal';
            enbConfig.Ng = 'Sixth';
            enbConfig.CellRefP = 1;
            PDSCH.NLayers = 1;
            PDSCH.Modulation = '16QAM';
            PDSCH.TargetCodeRate = 1/2;
        case 'R.2'
            enbConfig.NDLRB = 50;
            enbConfig.CyclicPrefix = 'Normal';
            enbConfig.PHICHDuration = 'Normal';
            enbConfig.Ng = 'Sixth';
            enbConfig.CellRefP = 1;
            PDSCH.NLayers = 1;
            PDSCH.Modulation = 'QPSK';
            PDSCH.TargetCodeRate = 1/3;
        case 'R.3'
            enbConfig.NDLRB = 50;
            enbConfig.CyclicPrefix = 'Normal';
            enbConfig.PHICHDuration = 'Normal';
            enbConfig.Ng = 'Sixth';
            enbConfig.CellRefP = 1;
            PDSCH.NLayers = 1;
            PDSCH.Modulation = '16QAM';
            PDSCH.TargetCodeRate = 1/2;
        case 'R.4'
            enbConfig.NDLRB = 6;
            enbConfig.CyclicPrefix = 'Normal';
            enbConfig.PHICHDuration = 'Normal';
            enbConfig.Ng = 'Sixth';
            enbConfig.CellRefP = 1;
            PDSCH.NLayers = 1;
            PDSCH.Modulation = 'QPSK';
            PDSCH.TargetCodeRate = 1/3;
        case 'R.5'
            enbConfig.NDLRB = 15;
            enbConfig.CyclicPrefix = 'Normal';
            enbConfig.PHICHDuration = 'Normal';
            enbConfig.Ng = 'Sixth';
            enbConfig.CellRefP = 1;
            PDSCH.NLayers = 1;
            PDSCH.Modulation = '64QAM';
            PDSCH.TargetCodeRate = 3/4;
        case 'R.6'
            enbConfig.NDLRB = 25;
            enbConfig.CyclicPrefix = 'Normal';
            enbConfig.PHICHDuration = 'Normal';
            enbConfig.Ng = 'Sixth';
            enbConfig.CellRefP = 1;
            PDSCH.NLayers = 1;
            PDSCH.Modulation = '64QAM';
            PDSCH.TargetCodeRate = 3/4;
        case 'R.7'
            enbConfig.NDLRB = 50;
            enbConfig.CyclicPrefix = 'Normal';
            enbConfig.PHICHDuration = 'Normal';
            enbConfig.Ng = 'Sixth';
            enbConfig.CellRefP = 1;
            PDSCH.NLayers = 1;
            PDSCH.Modulation = '64QAM';
            PDSCH.TargetCodeRate = 3/4;
        case 'R.8'
            enbConfig.NDLRB = 75;
            enbConfig.CyclicPrefix = 'Normal';
            enbConfig.PHICHDuration = 'Normal';
            enbConfig.Ng = 'Sixth';
            enbConfig.CellRefP = 1;
            PDSCH.NLayers = 1;
            PDSCH.Modulation = '64QAM';
            PDSCH.TargetCodeRate = 3/4;
        case 'R.9'
            enbConfig.NDLRB = 100;
            enbConfig.CyclicPrefix = 'Normal';
            enbConfig.PHICHDuration = 'Normal';
            enbConfig.Ng = 'Sixth';
            enbConfig.CellRefP = 1;
            PDSCH.NLayers = 1;
            PDSCH.Modulation = '64QAM';   
            PDSCH.TargetCodeRate = 3/4;
        case 'R.10'
            if(strcmpi(rmc.PDSCH.TxScheme,'TxDiversity'))
                enbConfig.NDLRB = 50;
                enbConfig.CyclicPrefix = 'Normal';
                enbConfig.PHICHDuration = 'Normal';
                enbConfig.Ng = 'Sixth';
                enbConfig.CellRefP = 2;
                PDSCH.NLayers = 2;
                PDSCH.Modulation = 'QPSK';
            elseif(strcmpi(rmc.PDSCH.TxScheme,'SpatialMux'))
                enbConfig.NDLRB = 50;
                enbConfig.CyclicPrefix = 'Normal';
                enbConfig.PHICHDuration = 'Normal';
                enbConfig.Ng = 'Sixth';
                enbConfig.CellRefP = 2;
                PDSCH.NLayers = 1;
                PDSCH.Modulation = 'QPSK';
                PDSCH.PMISet = 0;
            else
                enbConfig.NDLRB = 50;
                enbConfig.CyclicPrefix = 'Normal';
                enbConfig.PHICHDuration = 'Normal';
                enbConfig.Ng = 'Sixth';
                enbConfig.CellRefP = 2;
                PDSCH.NLayers = 1;
                PDSCH.Modulation = 'QPSK';
            end
            PDSCH.TargetCodeRate = 1/3;
        case 'R.11'
            enbConfig.NDLRB = 50;
            enbConfig.CyclicPrefix = 'Normal';
            enbConfig.PHICHDuration = 'Normal';
            enbConfig.Ng = 'Sixth';
            enbConfig.CellRefP = 2;
            PDSCH.NLayers = 2;
            PDSCH.Modulation = '16QAM';
            if(strcmpi(rmc.PDSCH.TxScheme,'SpatialMux'))
                PDSCH.PMISet = 0;
            end
            PDSCH.TargetCodeRate = 1/2;
        case 'R.12'
            enbConfig.NDLRB = 6;
            enbConfig.CyclicPrefix = 'Normal';
            enbConfig.PHICHDuration = 'Normal';
            enbConfig.Ng = 'Sixth';
            enbConfig.CellRefP = 4;
            PDSCH.NLayers = 4;
            PDSCH.Modulation = 'QPSK';
            PDSCH.TargetCodeRate = 1/3;
        case 'R.13'
            enbConfig.NDLRB = 50;
            enbConfig.CyclicPrefix = 'Normal';
            enbConfig.PHICHDuration = 'Normal';
            enbConfig.Ng = 'Sixth';
            enbConfig.CellRefP = 4;
            PDSCH.NLayers = 1;
            PDSCH.Modulation = 'QPSK';
            PDSCH.PMISet = 0;
            PDSCH.TargetCodeRate = 1/3;
        case 'R.14'
            if(strcmpi(rmc.PDSCH.TxScheme,'SpatialMux') && Ncodewords == 1)
                enbConfig.NDLRB = 50;
                enbConfig.CyclicPrefix = 'Normal';
                enbConfig.PHICHDuration = 'Normal';
                enbConfig.Ng = 'Sixth';
                enbConfig.CellRefP = 4;
                PDSCH.NLayers = 2;
                PDSCH.Modulation = '16QAM';
                PDSCH.PMISet = 0;
            else
                enbConfig.NDLRB = 50;
                enbConfig.CyclicPrefix = 'Normal';
                enbConfig.PHICHDuration = 'Normal';
                enbConfig.Ng = 'Sixth';
                enbConfig.CellRefP = 4;
                PDSCH.NLayers = 2;
                PDSCH.Modulation = '16QAM';
                if(strcmpi(rmc.PDSCH.TxScheme,'SpatialMux'))
                    PDSCH.PMISet = 0;
                end
            end
            PDSCH.TargetCodeRate = 1/2;
        case 'R.12-9RB'
            enbConfig.NDLRB = 9;
            enbConfig.CyclicPrefix = 'Normal';
            enbConfig.PHICHDuration = 'Normal';
            enbConfig.Ng = 'Sixth';
            enbConfig.CellRefP = 4;
            PDSCH.NLayers = 4;
            PDSCH.Modulation = 'QPSK';
            PDSCH.TargetCodeRate = 1/3;
        case 'R.6-27RB'
            enbConfig.NDLRB = 27;
            enbConfig.CyclicPrefix = 'Normal';
            enbConfig.PHICHDuration = 'Normal';
            enbConfig.Ng = 'Sixth';
            enbConfig.CellRefP = 1;
            PDSCH.NLayers = 1;
            PDSCH.Modulation = '64QAM';
            PDSCH.TargetCodeRate = 3/4;
        case 'R.11-45RB'
            enbConfig.NDLRB = 45;
            enbConfig.CyclicPrefix = 'Normal';
            enbConfig.PHICHDuration = 'Normal';
            enbConfig.Ng = 'Sixth';
            enbConfig.CellRefP = 2;
            PDSCH.NLayers = 2;
            PDSCH.Modulation = '16QAM';
            if(strcmpi(rmc.PDSCH.TxScheme,'SpatialMux'))
                PDSCH.PMISet = 0;
            end
            PDSCH.TargetCodeRate = 1/2;
       case 'R.25'
            enbConfig.NDLRB = 50;
            enbConfig.CyclicPrefix = 'Normal';
            enbConfig.PHICHDuration = 'Normal';
            enbConfig.Ng = 'Sixth';
            enbConfig.CellRefP = 1;
            PDSCH.NTxAnts = 2;
            PDSCH.NLayers = 1;
            PDSCH.Modulation = 'QPSK';
            PDSCH.W=lteDLPrecode(eye(PDSCH.NLayers),PDSCH.NTxAnts,'SpatialMux',0);
            PDSCH.TargetCodeRate = 1/3;
        case 'R.26'
            enbConfig.NDLRB = 50;
            enbConfig.CyclicPrefix = 'Normal';
            enbConfig.PHICHDuration = 'Normal';
            enbConfig.Ng = 'Sixth';
            enbConfig.CellRefP = 1;
            PDSCH.NTxAnts = 2;
            PDSCH.NLayers = 1;
            PDSCH.Modulation = '16QAM';
            PDSCH.W=lteDLPrecode(eye(PDSCH.NLayers),PDSCH.NTxAnts,'SpatialMux',0);
            PDSCH.TargetCodeRate = 1/2;
        case 'R.27'
            enbConfig.NDLRB = 50;
            enbConfig.CyclicPrefix = 'Normal';
            enbConfig.PHICHDuration = 'Normal';
            enbConfig.Ng = 'Sixth';
            enbConfig.CellRefP = 1;
            PDSCH.NTxAnts = 2;
            PDSCH.NLayers = 1;
            PDSCH.Modulation = '64QAM';
            PDSCH.W=lteDLPrecode(eye(PDSCH.NLayers),PDSCH.NTxAnts,'SpatialMux',0);
            PDSCH.TargetCodeRate = 3/4;
        case 'R.28'
            enbConfig.NDLRB = 50;
            enbConfig.CyclicPrefix = 'Normal';
            enbConfig.PHICHDuration = 'Normal';
            enbConfig.Ng = 'Sixth';
            enbConfig.CellRefP = 1;
            PDSCH.NTxAnts = 2;
            PDSCH.NLayers = 1;
            PDSCH.Modulation = '16QAM';
            PDSCH.W=lteDLPrecode(eye(PDSCH.NLayers),PDSCH.NTxAnts,'SpatialMux',0);
            PDSCH.TargetCodeRate = 1/2;
        case 'R.31-3A'
            enbConfig.CyclicPrefix = 'Normal';
            enbConfig.PHICHDuration = 'Normal';
            enbConfig.Ng = 'Sixth';
            enbConfig.CellRefP = 2;
            PDSCH.NLayers = 2;
            PDSCH.Modulation = '64QAM';
            if strcmpi(rmc.DuplexMode,'FDD')
                enbConfig.NDLRB = 50;
                PDSCH.TargetCodeRate = [0.90 0.85 0.85 0.85 0.85 0.89 0.85 0.85 0.85 0.85];
            else
                enbConfig.NDLRB = 75;
                PDSCH.TargetCodeRate = [0.90 0 0 0 0.87 0.88 0 0 0 0.87];
            end
            if ~isfield(rmc.PDSCH,'TargetCodeRate')
                % Target is Actual if no target specified (R.31-3A does not have a single fraction TCR)
                targetIsActual = true; 
            end
        case 'R.31-4'
            enbConfig.NDLRB = 100;
            enbConfig.CyclicPrefix = 'Normal';
            enbConfig.PHICHDuration = 'Normal';
            enbConfig.Ng = 'Sixth';
            enbConfig.CellRefP = 2;
            PDSCH.NLayers = 2;
            PDSCH.Modulation = '64QAM';
            if strcmpi(rmc.DuplexMode,'FDD')
                PDSCH.TargetCodeRate = [0.90 0.88 0.88 0.88 0.88 0.87 0.88 0.88 0.88 0.88];
            else
                PDSCH.TargetCodeRate = [0.90 0 0 0 0.88 0.87 0 0 0 0.88];
            end
            if ~isfield(rmc.PDSCH,'TargetCodeRate')
                % Target is Actual if no target specified (R.31-4 does not have a single fraction TCR)
                targetIsActual = true; 
            end
        case 'R.43'
            if strcmpi(rmc.DuplexMode,'FDD')
                enbConfig.NDLRB = 50;
                enbConfig.CyclicPrefix = 'Normal';
                enbConfig.PHICHDuration = 'Normal';
                enbConfig.Ng = 'Sixth';
                enbConfig.CellRefP = 2;
                enbConfig.CSIRefP = 4;
                enbConfig.CSIRSConfig = 0;
                enbConfig.CSIRSPeriod = [5 2];
                enbConfig.ZeroPowerCSIRSPeriod = 3;
                enbConfig.ZeroPowerCSIRSConfig = '0001000000000000';
                PDSCH.Modulation = 'QPSK';
                PDSCH.NTxAnts = enbConfig.CSIRefP;
                PDSCH.NLayers = 1;
                PDSCH.W = transpose(lteCSICodebook(PDSCH.NLayers,PDSCH.NTxAnts,0)); 
                PDSCH.NSCID = 0;
                PDSCH.TargetCodeRate = 1/3;
            else
                enbConfig.NDLRB = 100;
                enbConfig.CyclicPrefix = 'Normal';
                enbConfig.PHICHDuration = 'Normal';
                enbConfig.Ng = 'Sixth';
                enbConfig.CellRefP = 4;
                PDSCH.Modulation = '16QAM';
                PDSCH.NLayers = 1;
                PDSCH.PMISet = 0;
                PDSCH.TargetCodeRate = 1/2;
            end
        case 'R.44'
            enbConfig.NDLRB = 50;
            enbConfig.CyclicPrefix = 'Normal';
            enbConfig.PHICHDuration = 'Normal';
            enbConfig.Ng = 'Sixth';
            enbConfig.CellRefP = 2;
            enbConfig.CSIRefP = 4;
            if strcmpi(rmc.DuplexMode,'FDD')
                PDSCH.Modulation = 'QPSK';
                enbConfig.CSIRSConfig = 6;
                enbConfig.CSIRSPeriod = [5 1];
                enbConfig.ZeroPowerCSIRSPeriod = 'Off';
                PDSCH.TargetCodeRate = 1/3;
            else
                PDSCH.Modulation = '64QAM';
                enbConfig.CSIRSConfig = 3;
                enbConfig.CSIRSPeriod = [5 4];
                enbConfig.ZeroPowerCSIRSPeriod = 4;
                enbConfig.ZeroPowerCSIRSConfig = '0010000000000000';
                PDSCH.TargetCodeRate = 1/2;
            end
            PDSCH.NTxAnts = enbConfig.CSIRefP;
            PDSCH.NLayers = 1;
            PDSCH.W = transpose(lteCSICodebook(PDSCH.NLayers,PDSCH.NTxAnts,0)); 
            PDSCH.NSCID = 0;
        case 'R.45'
            enbConfig.NDLRB = 50;
            enbConfig.CyclicPrefix = 'Normal';
            enbConfig.PHICHDuration = 'Normal';
            enbConfig.Ng = 'Sixth';
            enbConfig.CellRefP = 2;
            if strcmpi(rmc.DuplexMode,'FDD')
                PDSCH.Modulation = '16QAM';
                enbConfig.CSIRefP = 4;
                enbConfig.CSIRSConfig = 8;
                enbConfig.CSIRSPeriod = [5 1];
                enbConfig.ZeroPowerCSIRSPeriod = 'Off';
            else
                PDSCH.Modulation = '16QAM';
                enbConfig.CSIRefP = 8;
                enbConfig.CSIRSConfig = 0;
                enbConfig.CSIRSPeriod = [5 4];
                enbConfig.ZeroPowerCSIRSPeriod = 'Off';
            end
            PDSCH.NTxAnts = enbConfig.CSIRefP;
            PDSCH.NLayers = 1;
            PDSCH.W = transpose(lteCSICodebook(PDSCH.NLayers,enbConfig.CSIRefP,[0 0]));
            PDSCH.NSCID = 0;
            PDSCH.TargetCodeRate = 1/2;
        case 'R.45-1'
            enbConfig.NDLRB = 50;
            enbConfig.CyclicPrefix = 'Normal';
            enbConfig.PHICHDuration = 'Normal';
            enbConfig.Ng = 'Sixth';
            enbConfig.CellRefP = 2;
            if strcmpi(rmc.DuplexMode,'FDD')
                PDSCH.Modulation = '16QAM';
                enbConfig.CSIRefP = 4;
                enbConfig.CSIRSConfig = 8;
                enbConfig.CSIRSPeriod = [5 1];
                enbConfig.ZeroPowerCSIRSPeriod = 'Off';
            else
                PDSCH.Modulation = '16QAM';
                enbConfig.CSIRefP = 8;
                enbConfig.CSIRSConfig = 0;
                enbConfig.CSIRSPeriod = [5 4];
                enbConfig.ZeroPowerCSIRSPeriod = 'Off';
            end
            PDSCH.NTxAnts = enbConfig.CSIRefP;
            PDSCH.NLayers = 1;
            PDSCH.W = transpose(lteCSICodebook(PDSCH.NLayers,enbConfig.CSIRefP,[0 0]));
            PDSCH.NSCID = 0;
            PDSCH.TargetCodeRate = 1/2;        
        case 'R.48'
            enbConfig.NDLRB = 50;
            enbConfig.CyclicPrefix = 'Normal';
            enbConfig.PHICHDuration = 'Normal';
            enbConfig.Ng = 'Sixth';
            enbConfig.CellRefP = 2;
            enbConfig.CSIRefP = 4;
            enbConfig.CSIRSConfig = 0;
            if strcmpi(rmc.DuplexMode,'FDD')
                enbConfig.CSIRSPeriod = [5 2];
            else
                enbConfig.CSIRSPeriod = [5 4];
            end
            enbConfig.ZeroPowerCSIRSPeriod = 'Off';
            PDSCH.NTxAnts = enbConfig.CSIRefP;
            PDSCH.NLayers = 1;
            PDSCH.Modulation = 'QPSK';
            PDSCH.W = transpose(lteCSICodebook(PDSCH.NLayers,PDSCH.NTxAnts,0)); % UE recommended PMI
            PDSCH.NSCID = 0;
            if ~isfield(rmc.PDSCH,'TargetCodeRate')
                % Target is Actual if no target specified (R.48 do not have a single fraction TCR)
                targetIsActual = true; 
            end
            PDSCH.TargetCodeRate = 1/2;
        case 'R.50'
            enbConfig.NDLRB = 50;
            enbConfig.CyclicPrefix = 'Normal';
            enbConfig.PHICHDuration = 'Normal';
            enbConfig.Ng = 'Sixth';
            enbConfig.CellRefP = 2;
            if strcmpi(rmc.DuplexMode,'FDD')
                enbConfig.CSIRefP = 4;
                enbConfig.CSIRSConfig = 3;
                enbConfig.CSIRSPeriod = [5 2];
                enbConfig.ZeroPowerCSIRSPeriod = 3;
                enbConfig.ZeroPowerCSIRSConfig = '0001000000000000';
                PDSCH.Modulation = '64QAM';
                PDSCH.TargetCodeRate = 1/2;
            else
                enbConfig.CSIRefP = 8;
                enbConfig.CSIRSConfig = 1;
                enbConfig.CSIRSPeriod = [5 4];
                enbConfig.ZeroPowerCSIRSPeriod = 4;
                enbConfig.ZeroPowerCSIRSConfig = '0010000100000000';
                PDSCH.Modulation = 'QPSK';
                PDSCH.TargetCodeRate = 1/3;
            end
            PDSCH.NTxAnts = enbConfig.CSIRefP;
            PDSCH.NLayers = 1;
            PDSCH.W = transpose(lteCSICodebook(PDSCH.NLayers,enbConfig.CSIRefP,[0 0]));
            PDSCH.NSCID = 0;    
        case 'R.51'
            enbConfig.NDLRB = 50;
            enbConfig.CyclicPrefix = 'Normal';
            enbConfig.PHICHDuration = 'Normal';
            enbConfig.Ng = 'Sixth';
            enbConfig.CellRefP = 2;
            enbConfig.CSIRefP = 2;
            enbConfig.CSIRSConfig = 8;
            if strcmpi(rmc.DuplexMode,'FDD')
                enbConfig.CSIRSPeriod = [5 2];
                enbConfig.ZeroPowerCSIRSPeriod = 3;
                enbConfig.ZeroPowerCSIRSConfig = '0010000000000000';
            else
                enbConfig.CSIRSPeriod = [5 4];
                enbConfig.ZeroPowerCSIRSPeriod = 4;
                enbConfig.ZeroPowerCSIRSConfig = '0010000000000000';
            end
            PDSCH.NTxAnts = enbConfig.CSIRefP;
            PDSCH.NLayers = 1;
            PDSCH.Modulation = '16QAM';
            PDSCH.W = transpose(lteCSICodebook(PDSCH.NLayers,PDSCH.NTxAnts,0)); 
            PDSCH.NSCID = 0;
            PDSCH.TargetCodeRate = 1/2;
        otherwise
            enbConfig = struct();
            PDSCH = struct();
    end

    % Set the CFI depending on the RMC, NDLRB, Duplex mode and 'tempRC'
    % flag indicating whether or not user provided the RC number. If no RC
    % was specified, no RMC specific CFI adjustments are done
    enbConfig.CFI = getCFI(enbConfig.NDLRB,rmc);

    userCellwideFields = fieldnames(rmc);
    requiredCellwideFields = fieldnames(enbConfig);
    missingFields = setdiff(requiredCellwideFields,userCellwideFields);
    for idx=1:length(missingFields)
        fieldVal = enbConfig.(missingFields{idx});
        rmc.(missingFields{idx}) = fieldVal;
    end

    userPdschFields = fieldnames(rmc.PDSCH);
    requiredPdschFields = fieldnames(PDSCH);
    missingPdschFields = setdiff(requiredPdschFields,userPdschFields);
    for idx=1:length(missingPdschFields)
        fieldVal = PDSCH.(missingPdschFields{idx});
        rmc.PDSCH.(missingPdschFields{idx}) = fieldVal;
    end

    if(~isfield(rmc.PDSCH,'RNTI'))
        rmc.PDSCH.RNTI = 1;
    end

    if (~iscell(rmc.PDSCH.Modulation))
        modulation = {rmc.PDSCH.Modulation};
    else
        modulation = rmc.PDSCH.Modulation;
    end

    if(~isfield(rmc.PDSCH,'RVSeq'))
        rvseqs = arrayfun(@(i)getRVSeq(modulation{min(length(modulation),i)}),1:Ncodewords,'UniformOutput',false);
        rmc.PDSCH.RVSeq = cat(1,rvseqs{:});
    end

    if(~isfield(rmc.PDSCH,'NHARQProcesses'))
        if(strcmpi(rmc.DuplexMode,'TDD'))
            hProcessTDD = [4 7 10 9 12 15 6]; % Max HARQ Processses for TDD
            rmc.PDSCH.NHARQProcesses = hProcessTDD(rmc.TDDConfig+1);
        else
            rmc.PDSCH.NHARQProcesses = 8;
        end
    end

    if(~isfield(rmc.PDSCH,'NTurboDecIts'))
        rmc.PDSCH.NTurboDecIts = 5;
    end

    if(Ncodewords == 2)
        RV = [0 0];
    elseif(Ncodewords == 1)
        RV = 0;
    end

    if(~isfield(rmc.PDSCH,'RV'))
        rmc.PDSCH.RV = RV;
    end

    if (~isfield(rmc,'HISet'))
        % Define HISET with the maximum number of groups allowed as per TS
        % 36.211 Section 6.9. Each group will have the first seqence set to
        % ACK. The lteRMCDLTool can then extract the values according to
        % the number of groups for the input configuration.
        
        % The HI set is defined by the format [ngroup, nseq, hi].
        % Create a set with the first HI in sequence per group set to ACK.
        ngroup = (0:2*2*ceil(2*110/8)-1)'; % List all possible group indices
        nseq = zeros(size(ngroup));        % Set first PHICH index in each group
        hi = ones(size(ngroup));           % Set ACK HI value for each PHICH
        rmc.HISet = [ngroup nseq hi];
    end

    if (~isfield(rmc.PDSCH,'PMIMode'))
        rmc.PDSCH.PMIMode = getPMIMode(rmcNo,rmc.DuplexMode);
    end

    if (~isfield(rmc.PDSCH,'CSIMode'))
        rmc.PDSCH.CSIMode = getCSIMode(rmcNo,rmc.DuplexMode);
    end

    % If single port, use 'Port0', else use 'TxDiversity'. For transmission
    % mode 10, 'Port7-14' is the OCNG transmission scheme to use, and should be
    % set externally by the user as all existing RMCs uses OP.1 OCNG pattern
    % where the transmission scheme is either 'Port0' or 'TxDiversity'
    if (~isfield(rmc.OCNGPDSCH,'TxScheme')) 
        rmc.OCNGPDSCH.TxScheme = 'Port0';
        if rmc.CellRefP>1
            rmc.OCNGPDSCH.TxScheme = 'TxDiversity';
        end
    end

    % Ensure that the PMISet field is set for SpatialMux and MultiUser
    if any(strcmpi(rmc.PDSCH.TxScheme,{'SpatialMux','MultiUser'}))
        if ~isfield(rmc.PDSCH,'PMISet')
            rmc.PDSCH.PMISet = 0;
        end
    end
    
    % Ensure that for Port7-14 transmission scheme, the relevant
    % parameters are set
    if strcmpi(rmc.PDSCH.TxScheme,'Port7-14')
        if ~isfield(rmc,'CSIRSPeriod')
            rmc.CSIRSPeriod = 'Off';
        end
        if ~isfield(rmc,'CSIRSConfig')
            rmc.CSIRSConfig = 0;
        end
        if ~isfield(rmc,'ZeroPowerCSIRSPeriod')
            rmc.ZeroPowerCSIRSPeriod = 'Off';
        end
        if ~isfield(rmc,'ZeroPowerCSIRSConfig')
            rmc.ZeroPowerCSIRSConfig = '0010000000000000';
        end
        if ~isfield(rmc,'CSIRefP')
            rmc.CSIRefP = rmc.CellRefP;
        end
        if ~isfield(rmc.PDSCH,'W')
            if rmc.CSIRefP == 1
                % If CSIRefP is 1, no precoding
                rmc.PDSCH.W = 1;
            elseif rmc.CSIRefP == 8
                % Use indices [0 0] for CSIRefP=8
                rmc.PDSCH.W = transpose(lteCSICodebook(rmc.PDSCH.NLayers,rmc.CSIRefP,0,0));
            else
                % Use index 0 for CSIRefP=2 or CSIRefP=4
                rmc.PDSCH.W = transpose(lteCSICodebook(rmc.PDSCH.NLayers,rmc.CSIRefP,0));
            end
        end
        if ~isfield(rmc.PDSCH,'NSCID')
            rmc.PDSCH.NSCID = 0;
        end
    end
    
    % Get the DCIFormat
    if(~isfield(rmc.PDSCH,'DCIFormat'))
        rmc.PDSCH.DCIFormat = getDCIFormat(rmc.PDSCH,Ncodewords);
    end

    % Get aggregation level
    if(~isfield(rmc.PDSCH,'PDCCHFormat'))
        rmc.PDSCH.PDCCHFormat = getPDCCHFormat(rmc);
    end

    if(~isfield(rmc.PDSCH,'PDCCHPower'))
        rmc.PDSCH.PDCCHPower = 0;
    end

end


function txScheme = getTxScheme(rmcNo,DuplexMode,ncw)

    txScheme = 'Port0';

    if (isUeRS(rmcNo))
        txScheme = 'Port5';
    end

    if (isCSIRS(rmcNo,DuplexMode))
        txScheme = 'Port7-14';
    end

    switch upper(rmcNo)
        case {'R.12','R.12-9RB','R.10','R.11','R.11-45RB'}
            txScheme = 'TxDiversity';
        case {'R.13','R.14'}
            txScheme = 'SpatialMux';
        case {'R.31-3A','R.31-4'}
            txScheme = 'CDD';
        case {'R.43'}
            if strcmpi(DuplexMode,'TDD')
                txScheme = 'SpatialMux';
            end
    end
    if(ncw == 2)
        switch upper(rmcNo)
            case {'R.11','R.14'}
                txScheme = 'CDD';
        end
    end
end

function ncw = getNcodewords(rmcNo,txScheme)
    ncw = 1;
    if strcmpi(txScheme,'CDD') || (strcmpi(txScheme,'SpatialMux') && any(strcmpi(rmcNo,{'R.11','R.14'})))
        ncw = 2;
    end
end

% This function validates valid TxScheme and corresponding allowed number
% of codewords as specified in TS 36.101 A.3.
function [] = validateTxScheme(rmcNo,TxScheme,NCodewords)

    rmcGroup1 = {'R.0','R.1','R.2','R.3','R.4','R.5','R.6','R.7' ...
                  'R.8','R.9','R.6-27RB'};

    if(sum(strcmpi(rmcNo,rmcGroup1)))
        if(strcmpi(TxScheme,'Port0') ~= 1)
            error('lte:error','TxScheme should be "Port0" for given RC');
        elseif(NCodewords ~= 1)
            error('lte:error','Number of codewords should be 1 for given RC and TxScheme');
        end
    elseif(strcmpi(rmcNo,'R.10')) 
        if((strcmpi(TxScheme,'TxDiversity') || strcmpi(TxScheme,'SpatialMux')) ~= 1)
            error('lte:error','TxScheme should be either "TxDiversity" or "SpatialMux" for given RC');
        elseif(NCodewords ~= 1)
            error('lte:error','Number of codewords should be 1 for given RC and TxScheme');
        end
    elseif(strcmpi(rmcNo,'R.11')  || strcmpi(rmcNo,'R.11-45RB'))
        if((strcmpi(TxScheme,'TxDiversity') || strcmpi(TxScheme,'SpatialMux') || strcmpi(TxScheme,'CDD')) ~= 1)
            error('lte:error','TxScheme should be one of {TxDiversity, SpatialMux, CDD} for given RC');
        elseif((strcmpi(TxScheme,'SpatialMux') && NCodewords == 1) || (strcmpi(TxScheme,'CDD') && NCodewords == 1))
            error('lte:error','Number of codewords should be 2 for given RC and TxScheme');
        elseif((strcmpi(TxScheme,'TxDiversity') && NCodewords == 2))
            error('lte:error','Number of codewords should be 1 for given RC and TxScheme');
        end
    elseif(strcmpi(rmcNo,'R.12') || strcmpi(rmcNo,'R.12-9RB')) 
        if((strcmpi(TxScheme,'TxDiversity') ~=1))
            error('lte:error','TxScheme should be "TxDiversity" for given RC');
        elseif(NCodewords == 2)
            error('lte:error','Number of codewords should be 1 for given RC and TxScheme');
        end
    elseif(strcmpi(rmcNo,'R.13')) 
        if((strcmpi(TxScheme,'SpatialMux')) ~= 1)
            error('lte:error','TxScheme should be "SpatialMux" for given RC');
        elseif(NCodewords == 2)
            error('lte:error','Number of codewords should be 1 for given RC and TxScheme');
        end
    elseif(strcmpi(rmcNo,'R.14')) 
        if((strcmpi(TxScheme,'CDD') || strcmpi(TxScheme,'SpatialMux') || strcmpi(TxScheme,'TxDiversity')) ~= 1)
            error('lte:error','TxScheme should be {TxDiversity, SpatialMux, CDD} for given RC');
        elseif((strcmpi(TxScheme,'CDD') && NCodewords == 1))
            error('lte:error','Number of codewords should be 2 for given RC and TxScheme');
        end
    end
end

% This function checks whether given RC number is valid.
function [] = validateRMC(rmcNo)
    validRMC = {'R.0','R.1','R.2','R.3','R.4','R.5','R.6','R.7' ...
                'R.8','R.9','R.10','R.11','R.12','R.13','R.14','R.25','R.26','R.27','R.28','R.31-3A','R.31-4','R.43','R.44','R.45','R.45-1','R.48','R.50','R.51'};
    validRMCCustom = {'R.12-9RB','R.6-27RB','R.11-45RB'};

    result = strcmpi(rmcNo,validRMC);
    resultCustom = strcmpi(rmcNo,validRMCCustom);

    if((sum(result) || sum(resultCustom)) ~= 1)
        error('lte:error',['The function call resulted in an error: The RC number ''%s'' is not valid, it must be one of ' sprintf('''%s'', ',validRMC{:}) sprintf('''%s'', ',validRMCCustom{:}) '\b\b' '.'],rmcNo);
    end
end

% This function provides valid transport block sizes and coded transport block sizes 
% (number of channel bits or physical channel allocation) for the given RC 
% and subframe number, as specified in TS 36.101 A.3 and TS 36.213
function rmcConfigOut = getBlkSizesAndRatesCustom(rmcConfig,targetIsActual)
    rmcConfigOut = rmcConfig;
    targetRate = rmcConfig.PDSCH.TargetCodeRate;
    % The target code rate can be a scalar, vector of size 1x10 or matrix
    % of size 2x10
    if ~isnumeric(targetRate) || ~(isequal(size(targetRate),[1 1]) || isequal(size(targetRate),[1 10]) || isequal(size(targetRate),[2 10]))
       error('lte:error','TargetCodeRate must be a numeric scalar or matrix of size 1-by-10 or 2-by-10'); 
    end
    rmcNo = rmcConfig.RC;
    duplexMode = rmcConfig.DuplexMode;
    if isscalar(targetRate)
       % Convert to an array of length 10
       targetRate = ones(1,10)* targetRate;
    end
    % If PRBSet is empty, we need to calculate the transport block sizes and
    % set the coded transport block sizes to be empty. So get the PRBSet for
    % the RMC as reference
    refPRBSet = getPRBSet(rmcConfig);
    if ~iscell(refPRBSet)
        % Convert to a cell array of length 10
        refPRBSet = repmat({refPRBSet},1,10);
    end
    if ~iscell(rmcConfig.PDSCH.PRBSet)
        % Convert to a cell array of length 10
        rmcConfig.PDSCH.PRBSet = repmat({rmcConfig.PDSCH.PRBSet},1,10);
    end

    % Compare the above cell arrays to find if any of the expected PRBSet are empty
    emptyIdx = cellfun('isempty',rmcConfig.PDSCH.PRBSet)>0;
    % If there are unexpected empty PRBSet, use the values given by RMC
    rmcConfig.PDSCH.PRBSet(emptyIdx) = refPRBSet(emptyIdx);

    % Calculate the transport blocksize and coded transport blocksize for 1
    % frame. We only need to calculate values in subframes where there is
    % non-zero allocation as per the RMCs supported by the function. If no
    % RMC was provided, assume all Downlink & Special subframes scheduled
    if rmcConfig.tempRC
        % Identify all active subframes (from downlink and special subframes)
        info = arrayfun(@(x)lteDuplexingInfo(setfield(rmcConfig,'NSubframe',x)),0:9); %#ok<*SFLD>
        dlandspecialsfs = arrayfun(@(x)any(strcmpi(x.SubframeType,{'Downlink','Special'})),info);
        if isfield(rmcConfig.PDSCH,'TrBlkSizes')
            sfsActive = rmcConfig.PDSCH.TrBlkSizes(1,:)~=0;
        else
            sfsActive = ones(1,10,'logical'); % No TrBlkSize masking
        end
        sfs = find(dlandspecialsfs.*sfsActive) - 1;
    else
        if strcmpi(duplexMode,'FDD')
            if any(strcmpi(rmcNo,{'R.31-3A','R.31-4'}))
                sfs = 0:9;
            else
                sfs = [0:4 6:9];
            end
        else
            if any(strcmpi(rmcNo,{'R.4','R.12','R.12-9RB'}))
                sfs = [0 4 9];
            elseif any(strcmpi(rmcNo,{'R.14','R.43'}))
                sfs = [1 4 6 9];
            elseif any(strcmpi(rmcNo,{'R.31-3A','R.31-4'}))
                sfs = [0 4 5 9];
            else
                sfs = [0 1 4 6 9];
            end
        end
    end
    ncw = numel(rmcConfig.PDSCH.Modulation);
    codedTbSize = zeros(ncw,10);
    tbSize = zeros(ncw,10);
    % Calculate transport block size(s) for reference PDSCH instance in this subframe
    if (ncw == 2) && (size(targetRate,1) == 1)
        targetRate(2,:) = targetRate; % Use the same rate(s) for 2 cws
    end

    % Calculate the number of spatial layers per codeword for use in the TBS lookup
    slayers = ones(1,ncw);
    % If using a transmission scheme capable of supporting multiple spatial layers 
    % then divide the total number of spatial layers across the codewords
    if any(strcmpi(rmcConfig.PDSCH.TxScheme,{'CDD','SpatialMux','Port7-8','Port7-14'}))
        slayers = rmcConfig.PDSCH.NLayers*slayers;
        % Divide the total number of layers between the codewords, rounding 
        % up on the second codeword     
        if ncw==2
            slayers(1) = fix(slayers(1)/2);
            slayers(2) = slayers(2)-slayers(1);
        end
    end   
    
    % Pre-calculate the potential sets of valid I_TBS for each codeword across the subframes
    % The valid I_TBS are deduced from the PDSCH MCS tables (the second PDSCH table will only be used for 256QAM cases)
    validITBS = cell(1,ncw);
    tables = {'PDSCH','PDSCHTable2'};
    for i=1:ncw
        for t=1:length(tables)
            [itbs,modulation] = lteMCS(tables{t});
            itbs(isnan(itbs)) = -1;         % Reassign the reserved I_TBS for use with lteTBS
            validITBS{i} = itbs(strcmpi(modulation,rmcConfig.PDSCH.Modulation{i})); % Find the MCS entries (I_TBS) that match the modulation type
            if ~isempty(validITBS{i})       % Only move to the other table if no valid I_TBS were found
                break;
            end
        end
    end
    
    % Get the original CFI value
    cfi = rmcConfig.CFI;
    for s = sfs+1   % 1 based index to access the MATLAB containers representing the subframes in a frame
        
        % If CFI is vector, use the correct value
        if ~isscalar(cfi)
            rmcConfig.CFI = cfi(s);
        end

        tempprbset = rmcConfig.PDSCH.PRBSet{s};
        rmcConfig.NSubframe = s-1;  % 0 based number
        
        % Calculate the coded transport block size (channel bit capacity)
        [~,info] = ltePDSCHIndices(rmcConfig,rmcConfig.PDSCH,tempprbset);
        codedTbSize(:,s) = transpose(info.G);
        
        % Calculate the transport block sizes if not present in rmcConfig
        if ~isfield(rmcConfig.PDSCH,'TrBlkSizes') 
            % Calculate number of PRB to be used in the transport block size calculation
            ndashprb = size(tempprbset,1);
            nprb = ndashprb;
            dupinfo = lteDuplexingInfo(rmcConfig);
            if (ndashprb>0) && strcmpi(dupinfo.SubframeType,'Special') && strcmpi(duplexMode,'TDD')
                % Apply rules in 36.213 Section 7.1.7 for selecting the TB Size
                if ((rmcConfig.SSC == 9) && strcmpi(rmcConfig.CyclicPrefix,'Normal')) || ((rmcConfig.SSC == 7) && strcmpi(rmcConfig.CyclicPrefix,'Extended'))
                    nprb = max(floor(ndashprb*0.375),1);
                else
                    nprb = max(floor(ndashprb*0.75),1);
                end
            end
            for i=1:ncw
                % Lookup up the set of TBS candidates for the codewords (from the appropriate TBS tables for 
                % the number of spatial layers per codeword) given the set of allowable I_TBS and PRB allocation in subframe       
                tbSizes = lteTBS(nprb,validITBS{i},slayers(i));
                % Given the target coding rate for RMC, select final TBS from these candidates 
                tbSize(i,s) = calcTBS(targetRate(i,s),tbSizes,codedTbSize(i,s));
            end
        end
    end

    % Now set the coded transport block size to 0 if PDSCH.PRBSet was empty
    codedTbSize(:,emptyIdx) = 0;

    % Explicit overrides for special cases where direct application of the formulae 
    % does not result in the required transport block sizes for subframes 0,1 and 6. 
    if isfield(rmcConfig.PDSCH,'TrBlkSizes') 
        % If TrBlkSizes was present, then pass it to the output
        tbSize = rmcConfig.PDSCH.TrBlkSizes;
    else
        if strcmpi(duplexMode,'TDD')
            % TDD modifications
            switch upper(rmcNo)
                case 'R.2',  tbSize(:,[2,7]) = 3240;   % See R4-090813
                case 'R.7',  tbSize(:,1) = 30576;  
                case 'R.8',  tbSize(:,[2,7]) = 35160;  % See R4-090813
                case 'R.11', tbSize(:,[2,7]) = 9528;   % See R4-090813
                case 'R.14', tbSize(:,[2,7]) = 9528;   % See R4-090813
                case 'R.25', tbSize(:,1) = 2984;       % See R4-091884
                case 'R.26', tbSize(:,[1,2,7]) = 9528; % See R4-091884,R4-091885
                case 'R.27', tbSize(:,1) = 22152;      % See R4-091884
                case 'R.48', tbSize(:,[2,7]) = 4264;   % See R4-124508
            end
        else
            % FDD modifications
            switch upper(rmcNo)
                case 'R.8', tbSize(:,1) = 45352;
            end
        end
    end
    
    % Now calculate the actual code rate, note that if the user provided
    % the transport block sizes, the code rate can exceed 0.93
    actCoderate = calcActualCodeRate(tbSize,codedTbSize,ncw);
    % If CodedtrBlkSize is 0, set code rate to 0 as well
    actCoderate(actCoderate==Inf)=0; 
    
    % Now set the values to o/p
    rmcConfigOut.PDSCH.TrBlkSizes = tbSize;
    rmcConfigOut.PDSCH.CodedTrBlkSizes = codedTbSize;
    rmcConfigOut.PDSCH.ActualCodeRate = actCoderate;
    if targetIsActual
        rmcConfigOut.PDSCH.TargetCodeRate = rmcConfigOut.PDSCH.ActualCodeRate;
    end
    
    rmcConfigOut.PDSCH.PolarCodedTrBlkSizes = pow2(floor(log2(double(codedTbSize))));
    rmcConfigOut.PDSCH.ActualPolarCodeRate = tbSize ./ rmcConfigOut.PDSCH.PolarCodedTrBlkSizes;
end

% Select the TBS from a set of TBS candidates that best matches the target coding rate
function tbs = calcTBS(targetRate,tbSizes,codedTbSize)
    % TS 36.101 Section A.3.1 - Algorithm for determining the payload size 
    % Account for CRC added to the TrBlk and individual code segments if appropriate 
    if codedTbSize==0
        % If CodedTrBlkSize is 0 due to the combination of parameters (e.g.
        % SS=4, extended cyclic prefix), set TrBlkSize to 0
        tbs = 0; 
    else
        % Calculate TBS avoiding floating point division. 
        dlschinfo = arrayfun(@lteDLSCHInfo,tbSizes);
        tbplus = double([dlschinfo.Bout]+[dlschinfo.F]);    % Payload size plus any CRC and filler bits
        actualrate = tbplus./codedTbSize;
        % If both tbplus and codedTbSize are zero then actualrate will be
        % NaN, so change NaN to Inf for uniform handling with the case of
        % non-zero tbplus and zero codedTbSize.
        actualrate(isnan(actualrate)) = Inf;
        % Now exclude the sizes which would result in actual code rate >0.93
        tbplus(actualrate > 0.93) = [];
        tbSizes(actualrate > 0.93) = [];
        [n,d] = rat(targetRate);
        absnumerators = abs(codedTbSize*n-d*tbplus);
        % Select all the transport blocks corresponding to the minimum
        % difference between target and actual coding rates
        possibleSizes = tbSizes(absnumerators == min(absnumerators));
        tbs = max(possibleSizes);
    end
end
        
function uers = isUeRS(rmcNo)
    uers=any(strcmpi(rmcNo,{'R.25','R.26','R.27','R.28'}));
end

function csirs = isCSIRS(rmcNo,DuplexMode)
    csirs=any(strcmpi(rmcNo,{'R.43','R.44','R.45','R.45-1','R.48','R.50','R.51'}));  
    if strcmpi(rmcNo,'R.43') && strcmpi(DuplexMode,'TDD')
        csirs = 0;
    end
end    
        
function RMCConfigNew = rearrangeFields(RMCConfig)
    % Create an empty variable of the same type as the input parameter config 
    ft = str2func(class(RMCConfig));
    RMCConfigNew = ft();
    % Defining the parameters arrangement for cell-wide settings
    arrangedFields = {'RC','NDLRB','CellRefP','NCellID','CyclicPrefix','CFI','PCFICHPower','Ng','PHICHDuration','HISet','PHICHPower','NFrame','NSubframe',...
        'TotSubframes','Windowing','DuplexMode','PDSCH', 'OCNGPDCCHEnable', 'OCNGPDCCHPower', 'OCNGPDSCHEnable','OCNGPDSCHPower','OCNGPDSCH'};
    if strcmpi(RMCConfig.PDSCH.TxScheme,'Port7-14')
        arrangedFields = [arrangedFields {'CSIRefP','CSIRSPeriod','CSIRSConfig','ZeroPowerCSIRSPeriod','ZeroPowerCSIRSConfig'}];
    end
    for idx=1:length(arrangedFields)
        RMCConfigNew.(arrangedFields{idx}) = RMCConfig.(arrangedFields{idx});
    end
    
    % Any additional parameter field will be copied without any specific
    % order
    arrangedFields = fieldnames(RMCConfigNew);
    requiredCellwideFields = fieldnames(RMCConfig);
    missingFields = setdiff(requiredCellwideFields,arrangedFields);
    for idx=1:length(missingFields)
        RMCConfigNew.(missingFields{idx}) = RMCConfig.(missingFields{idx});
    end

    % Defining the parameters arrangement for channel specific
    % configuration
    arrangedPDSCH = {'TxScheme','Modulation','NLayers','Rho','RNTI','RVSeq','RV' ...
            'NHARQProcesses','NTurboDecIts','PRBSet','TargetCodeRate','ActualCodeRate','TrBlkSizes','CodedTrBlkSizes','DCIFormat','PDCCHFormat','PDCCHPower','CSIMode','PMIMode'};
    if isfield(RMCConfig.PDSCH,'NSCID')
        arrangedPDSCH = [arrangedPDSCH 'NSCID'];
    end
    if isfield(RMCConfig.PDSCH,'W')
        arrangedPDSCH = [arrangedPDSCH 'W'];
    end 
    ft = str2func(class(RMCConfig.PDSCH));  
    RMCConfigNew.PDSCH = ft();  
    for idx=1:length(arrangedPDSCH)
        RMCConfigNew.PDSCH.(arrangedPDSCH{idx}) = RMCConfig.PDSCH.(arrangedPDSCH{idx});
    end
    
    % Any additional parameter field will be copied without any specific
    % order
    requiredPDSCHFields = fieldnames(RMCConfig.PDSCH);
	missingPDSCHFields = setdiff(requiredPDSCHFields,arrangedPDSCH);
    for idx=1:length(missingPDSCHFields)
        RMCConfigNew.PDSCH.(missingPDSCHFields{idx}) = RMCConfig.PDSCH.(missingPDSCHFields{idx});
    end
end

% Function to calculate the PRBSet for all RMCs if not specified by user
function prbset = getPRBSet(RMCConfig)
% If there is only one PRBSet for the subframes where there are valid PDSCH
% transmission, PRBSet will be a column vector or matrix. Otherwise it
% willbe a row cell array (this is usually for the UE-specific beamforming
% cases)
    rmcNo = RMCConfig.RC; 
    prbset = (0:RMCConfig.NDLRB-1)';
    if strcmpi(RMCConfig.DuplexMode,'FDD')
        switch upper(rmcNo)
            case {'R.0','R.1','R.28'}
                prbset = 0; 
            case {'R.31-3A'}
                prbset  = {(0:RMCConfig.NDLRB-1)' (0:RMCConfig.NDLRB-1)' (0:RMCConfig.NDLRB-1)' (0:RMCConfig.NDLRB-1)' (0:RMCConfig.NDLRB-1)' (3:RMCConfig.NDLRB-1)' (0:RMCConfig.NDLRB-1)' (0:RMCConfig.NDLRB-1)' (0:RMCConfig.NDLRB-1)' (0:RMCConfig.NDLRB-1)'};
            case {'R.31-4'}
                prbset  = {(0:RMCConfig.NDLRB-1)' (0:RMCConfig.NDLRB-1)' (0:RMCConfig.NDLRB-1)' (0:RMCConfig.NDLRB-1)' (0:RMCConfig.NDLRB-1)' (4:RMCConfig.NDLRB-1)' (0:RMCConfig.NDLRB-1)' (0:RMCConfig.NDLRB-1)' (0:RMCConfig.NDLRB-1)' (0:RMCConfig.NDLRB-1)'};
            case {'R.25' 'R.26' 'R.27' 'R.43' 'R.44' 'R.45' 'R.48' 'R.50' 'R.51'}
                prbset = {[0:20 30:49]' (0:RMCConfig.NDLRB-1)' (0:RMCConfig.NDLRB-1)' (0:RMCConfig.NDLRB-1)' (0:RMCConfig.NDLRB-1)' [] (0:RMCConfig.NDLRB-1)' (0:RMCConfig.NDLRB-1)' (0:RMCConfig.NDLRB-1)' (0:RMCConfig.NDLRB-1)'};
            case {'R.45-1'}
                prbset = [0:20 30:47]'; % Only one value
        end
        
    else
        switch upper(rmcNo)
            case {'R.0','R.1','R.28'}
                prbset = 0; 
            case {'R.25' 'R.26' 'R.27'}
                prbset = {[0:20 30:49]' (0:RMCConfig.NDLRB-1)' [] [] (0:RMCConfig.NDLRB-1)' [] (0:RMCConfig.NDLRB-1)' [] [] (0:RMCConfig.NDLRB-1)'};
            case {'R.31-3A'}
                prbset  = (4:71)';
            case {'R.31-4'}
                prbset  = {(0:RMCConfig.NDLRB-1)' [] [] [] (0:RMCConfig.NDLRB-1)' (4:RMCConfig.NDLRB-1)' [] [] [] (0:RMCConfig.NDLRB-1)'};
            case {'R.45-1'}
                prbset = [0:20 30:47]'; % Only one value
            case {'R.44' 'R.45' 'R.48' 'R.50' 'R.51'}
                prbset = {[0:20 30:49]' [0:20 30:49]' [] [] (0:RMCConfig.NDLRB-1)' [] [0:20 30:49]' [] [] (0:RMCConfig.NDLRB-1)'};   
        end
    end
end
    
function pmimode = getPMIMode(rmcNo,DuplexMode)   
% Function to calculate the PMIMode according to the test definition
    pmimode = 'Wideband';
    if (strcmpi(DuplexMode,'TDD'))
        if (strcmpi(rmcNo,'R.43'))
            pmimode = 'Subband';
        end
    else
        if any(strcmpi(rmcNo,{'R.45','R.45-1'}))
            pmimode = 'Subband';
        end
    end
end

function csimode = getCSIMode(rmcNo,DuplexMode)
% Function to calculate the CSIMode according to the test definition
    csimode = 'PUCCH 1-1';
    if any(strcmpi(rmcNo,{'R.10','R.13','R.14','R.43','R.45','R.45-1'}))
        csimode = 'PUSCH 1-2';
    elseif any(strcmpi(rmcNo,{'R.11','R.44'}))
        csimode = 'PUSCH 3-1';
    end
    
    if strcmpi(DuplexMode,'TDD') 
       if any(strcmpi(rmcNo,{'R.45','R.45-1'}))
           csimode = 'PUSCH 3-1';
       end
    end
end        
    

function cfi = getCFI(NDLRB,rmc)
    % Function to calculate the CFI scalar (fixed cfi values (FDD)) or vector
    % (varying cfi values (TDD)) for standard BWs (6, 15, 25, 50, 75, 100) and
    % non-standard BWs (9,27,45). 2 symbols allocated to PDCCH for 20 MHz, 15
    % MHz and 10 MHz channel BW; 3 symbols allocated to PDCCH for 5 MHz and 3
    % MHz; 4 symbols allocated to PDCCH for 1.4 MHz.

    % For NDLRB<10, CFI = symbols allocated to PDCCH - 1
    % For NDLRB>=10, CFI = symbols allocated to PDCCH
    if any(strcmpi(rmc.RC,{'R.31-3A','R.31-4'}))
        cfi = 1;
    else
        if any(NDLRB == [45 50 75 100])
            cfi = 2;
        else
            cfi = 3;
        end

        % For TDD special subframes, only 2 OFDM symbols are allocated to PDCCH
        if strcmpi(rmc.DuplexMode,'TDD')
            % Identify all special subframes
            rmc.CyclicPrefix = 'Normal'; % Assign cyclic prefix value to avoid default warning
            info = arrayfun(@(x)lteDuplexingInfo(setfield(rmc,'NSubframe',x)),0:9); %#ok<*SFLD>
            ssfs = arrayfun(@(x)strcmpi(x.SubframeType,'Special'),info);
            
            %Calculate the CFI for special subframes
            if NDLRB < 10
                cfispecial = 1;
            else
                cfispecial = 2;
            end

            if (cfispecial~=cfi) && ~any(strcmpi(rmc.RC,{'R.4','R.12'}))
                % If cfi values for special subframes is different from
                % other subframes create a cell array of CFI values.
                % Exclude 'R.4' and 'R.12' as the special subframes are not
                % scheduled
                cfi = ones(1,10)*cfi;
                cfi(ssfs) = cfispecial;
            end
        end
    end
end
    
% Set the DCIFormat
function dciFormat = getDCIFormat(PDSCH,Ncodewords)
    % Ensure that for all transmission schemes, there is a suitable
    % DCI format as per TS 36.213 Table 7.1-5
    if any(strcmpi(PDSCH.TxScheme,{'Port0','Port5','TxDiversity'})) 
        dciFormat = 'Format1';  % Mode 1(Port0), 2(TxDiversity), 7(Port5)
    elseif strcmpi(PDSCH.TxScheme,'MultiUser')
        dciFormat = 'Format1D'; % Mode 5 (Multi-user MIMO)
    elseif any(strcmpi(PDSCH.TxScheme,'SpatialMux')) 
        dciFormat = 'Format2';  % Mode 4 (Closed-loop spatial multiplexing)
    elseif any(strcmpi(PDSCH.TxScheme,'CDD')) 
        dciFormat = 'Format2A'; % Mode 3 (Large delay CDD)
    elseif any(strcmpi(PDSCH.TxScheme,'Port7-8')) && (Ncodewords == 2) && (PDSCH.NLayers==2) % 1Layer/CW
        dciFormat = 'Format2B'; % Mode 8 (Dual layer transmission, port 7 and 8)
    elseif any(strcmpi(PDSCH.TxScheme,{'Port8','Port7-8','Port7-14'}))
        dciFormat = 'Format2C'; % Mode 9 (Port7, Port8, Ports7-14)
    end
end

% Set the PDCCH aggregation level
function pdcchFormat = getPDCCHFormat(rmc)
    % Set the aggregation level to ensure that the size of candidates do
    % not exceed the total number of bits available for PDCCH
    pdcchFormat = 2;
    pdcchInfo = ltePDCCHInfo(rmc);
    if(pdcchInfo.NCCE==2)
        pdcchFormat = 1;
    elseif(pdcchInfo.NCCE==1)
        pdcchFormat = 0;
    end
end

% Set the RV sequence for one codeword
function rvseq = getRVSeq(modulation)
    if(any(strcmpi(modulation,{'64QAM','256QAM'})))
        rvseq = [0 0 1 2];
    else
        rvseq = [0 1 2 3];
    end
end

% Calculate the actual coderate according to TS 36.101 Section A.3.1
function actcoderate = calcActualCodeRate(tbSizes,codedTbSize,ncw)
    % For two codewords, prepare the tbSize to correspond to two codewords
    if ncw==2 && size(tbSizes,1)==1
        tbSizes(2,:) = tbSizes;
    end
    for n= 1:ncw
        dlschinfo = arrayfun(@lteDLSCHInfo,tbSizes(n,:));
        tbplus = double([dlschinfo.Bout]+[dlschinfo.F]); % Payload size plus any CRC and filler bits
        actcoderate(n,:) = tbplus./codedTbSize(n,:); %#ok<AGROW>
        % If both tbplus and codedTbSize are zero then actcoderate will be
        % NaN, so change NaN to Inf for uniform handling with the case of
        % non-zero tbplus and zero codedTbSize.
        actcoderate(n,isnan(actcoderate(n,:))) = Inf; %#ok<AGROW>
    end
end