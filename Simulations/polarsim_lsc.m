function [ber, numBits] = polarsim_lsc(EbNo, maxNumErrs, maxNumBits)
    %VITERBISIM Viterbi decoder simulation example for BERTool.
    %
    %   [BER, NUMBITS] = VITERBISIM(EBNO, MAXNUMERRS, MAXNUMBITS) simulates a
    %   Quaternary Phase Shift Keying (QPSK) or Binary PSK (BPSK) system over
    %   an additive white Gaussian noise (AWGN) channel using convolutional
    %   encoding and the Viterbi decoding algorithm with hard decision
    %   decoding.  EBNO is a vector of Eb/No values, MAXNUMERRS is the maximum
    %   number of errors to collect before stopping, and MAXNUMBITS is the
    %   maximum number of bits to run before stopping.  BER is the computed bit
    %   error rate, and NUMBITS is the actual number of bits run.
    %
    %   This function shows how to write a MATLAB simulation function
    %   for BERTool, and cannot run without BERTool.
    %
    %   It illustrates how to use the following System objects:
    %   comm.ConvolutionalEncoder, comm.PSKModulator, comm.PSKDemodulator,
    %   comm.AWGNChannel, comm.ViterbiDecoder and comm.ErrorRate.

    %   Copyright 1996-2016 The MathWorks, Inc.

    % Import Java class of BERTool
    import com.mathworks.toolbox.comm.BERTool;

    simParams.enb = lteRMCDLCustom('R.4');

    % Define number of bits per symbol (k). M = 4 for QPSK or 2 for BPSK.
    M = 4;
    k = log2(M);

    % Code rate
    %codeRate = 1/3; 
    codeRate = simParams.enb.PDSCH.TargetCodeRate;
    
    % Create a rate 1/3, constraint length 7 ConvolutionalEncoder System
    % object. This encoder takes one-bit symbols as inputs and generates 2-bit
    % symbols as outputs.
    enc = comm.ConvolutionalEncoder(poly2trellis(7, [171 133]));

    % Create a M-ary comm.PSKModulator and a comm.PSKDemodulator System object.
    % The 'SymbolMapping' property of both the objects is by default set to
    % employ Gray coding.
    qpskMod = comm.PSKModulator(M,...
        'BitInput',true);
    qpskDemod = comm.PSKDemodulator(M,...
        'BitOutput',true, 'DecisionMethod', 'Log-likelihood ratio');

    %     simParams.channelSNR =  5;         % dBs
    %     simParams.channelGain = 0.9;       % gain term
    %     simParams.channelDelay = 100;      % samples at modulator output sample rate
    %     simParams.frequencyOffset = 3000;  % Hz

    % Create a comm.AWGNChannel System object. 
    chan = comm.AWGNChannel(...
        'NoiseMethod', 'Signal to noise ratio (Eb/No)',...
        'SignalPower', 1,...
        'SamplesPerSymbol', 1);

    %phase = comm.PhaseFrequencyOffset('FrequencyOffset',3000);

    % Adjust SNR for coded bits and multi-bit symbols and set the 'EbNo'
    % property of the comm.AWGNChannel object to this adjusted value.
    chan.EbNo = EbNo - 10*log10(1/codeRate) + 10*log10(k);
    %chan.EbNo = EbNo;

    % Configure a comm.ViterbiDecoder System object to act as the decoder.
    % Since the convolutional decoder makes hard decisions in this example, set
    % the 'InputFormat' property to 'Hard'.
    dec = comm.ViterbiDecoder(poly2trellis(7, [171 133]), ...
      'InputFormat', 'Hard');

    % This example uses a traceback depth of 32; set the 'TraceBackDepth'
    % property of the comm.ViterbiDecoder object to 32.
    %dec.TracebackDepth = 32;

    % Create a comm.ErrorRate System object to compare the decoded bits to the
    % original transmitted bits. The output of the comm.ErrorRate object is a
    % three-element vector containing the calculated bit error rate (BER), the
    % number of errors observed, and the number of bits processed. The Viterbi
    % decoder creates a delay in the output decoded bit stream equal to the
    % traceback length. To account for this delay set the 'ReceiveDelay'
    % property of the comm.ErrorRate object to the value of the
    % 'TraceBackDepth' property of the comm.ViterbiDecoder object.
    %errorCalc = comm.ErrorRate('ReceiveDelay', dec.TracebackDepth);
    errorCalc = comm.ErrorRate;

    % Create a vector to store current values of the bit error rate, errors
    % incurred and number of bits processed.

    % simParams = hdlcoder_lteofdm_modDetectref_init;
    % simParams.enb = lteRMCDLCustom('R.2');   
    epsilon = 0.32;
    crc_size = 0;
    chs = 0;
    
    %polar_coders(10) = PolarCode(simParams.enb.PDSCH.PolarCodedTrBlkSizes(10), simParams.enb.PDSCH.TrBlkSizes(10)+24, epsilon, crc_size);    
    %polar_coders = cell(1,10);
    polar_coder = PolarCode(simParams.enb.PDSCH.PolarCodedTrBlkSizes(10), ...
                            simParams.enb.PDSCH.TrBlkSizes(10)+24, epsilon, crc_size);    
   
    for i = 1:10        
        if (simParams.enb.PDSCH.TrBlkSizes(i) > 0 && simParams.enb.PDSCH.TrBlkSizes(i) ~=simParams.enb.PDSCH.TrBlkSizes(10))            
            polar_coders(i) = PolarCode(simParams.enb.PDSCH.PolarCodedTrBlkSizes(i), ...
                                    simParams.enb.PDSCH.TrBlkSizes(i)+24, epsilon, crc_size);    
        else
            polar_coders(i) = polar_coder;
        end
    end
    % Set the number of bits per iteration
    bitsPerIter = sum(simParams.enb.PDSCH.TrBlkSizes(:));

    v = zeros(1,3);
    
    % Exit loop when either the number of bit errors exceeds 'maxNumErrs'
    % or the maximum number of iterations have been completed
    while ((v(2) < maxNumErrs) && (v(3) <= maxNumBits))

        % Check if the user has clicked the Stop button of BERTool
        if (BERTool.getSimulationStop)
            break;
        end

%        simParams = hdlcoder_lteofdm_modDetectref_init;

%         channelOutput = chan(simParams.LTErefOutputUnfiltered);          % AWGN channel   
%         %phaseOffsetOutput = phase(channelOutput);
% 
%         decData = DemodulateDecodeLte(simParams.enb, channelOutput);
%         %decData = DemodulateDecodeLte(simParams.enb, phaseOffsetOutput);
% 
%         numBits = min(length(decData), length(simParams.txData));
%         v = errorCalc(decData(1:numBits), simParams.txData(1:numBits));

         data = randi([0 1], bitsPerIter, 1);    % Generate message bits                                           
    %     encData = enc(data);                    % Convolutionally encode   
    %     modData = qpskMod(encData);             % Modulate   
    %     channelOutput = chan(modData);          % AWGN channel   
    %     demodData = qpskDemod(channelOutput);   % Demodulate 
    %     decData = dec(demodData);               % Viterbi decode   
    %     v = errorCalc(data, decData);           % Count errors         
         curr = 1;
         %encData = [];
         for i = 1:10
             TrBlkSize = simParams.enb.PDSCH.TrBlkSizes(i);
             %CodedTrBlkSize = simParams.enb.PDSCH.CodedTrBlkSizes(i);
             CodedTrBlkSize = simParams.enb.PDSCH.PolarCodedTrBlkSizes(i);
             trBlk = data(curr:curr+TrBlkSize-1);
             if (TrBlkSize > 0)
                 %encData = pc_encode(chs,(TrBlkSize+24)*3,trBlk);
                 encData = pc_encode(chs,CodedTrBlkSize,trBlk,polar_coders(i));
                 %encData = [encData; pc_encode(0,(i+24)*3,data(curr:curr+i))];
                 modData = qpskMod(encData);             % Modulate
                 channelOutput = chan(modData);          % AWGN channel
                 demodData = qpskDemod(channelOutput);   % Demodulate
                 decData = pc_decode(chs,TrBlkSize,demodData,polar_coders(i));
                 %decData2 = pc_decode(chs,TrBlkSize,encData);
                 v = errorCalc(double(trBlk), double(decData));           % Count errors                                     
                 curr = curr + TrBlkSize;
             end
         end
    end

% Assign values to ber and numBits
ber = v(1);
numBits = v(3);

end 

% Encode a single codeword
function cw = pc_encode(chs,outlen,trblkin, polar_coder)
    
    % Transport block CRC attachment
     crced = lteCRCEncode(trblkin,'24A');
%     
%     % Code block segmentation and code block CRC attachment
%     segmented = lteCodeBlockSegment(crced);   
%     
%     % Channel coding
%     coded = lteTurboEncode(segmented);
    
    % Rate matching
    %cw_turbo = lteRateMatchTurbo(coded,outlen,chs.RV,chs);
    
%     % cw = kron(crced, ones(3,1));
%     nrep = repmat(crced,1,3)';
%     nrep = nrep(:);
%     cw_nrep = [nrep; randi([0 1], outlen-length(nrep),1)];
    
    % N=256; K=128;
    N=pow2(floor(log2(double(outlen))));
    %K = N/2;
    K = length(crced);
%     designSNRdB = 0;
%     initPC(N,K,1,2, designSNRdB,1); %silent, no output
    
%     Ec=1; N0=2; designSNRdB=0;silentflag=1;%0dB SNR
%     %initPC(N,K,Ec,N0);
%     initPC(N,K,Ec,N0,designSNRdB,silentflag);
%     
%     padded = [crced; zeros(K-length(crced),1)];    
%     cw_polar = pencode(padded);
%     %cw_polar = [cw_polar; randi([0 1], outlen-N,1)];
%     cw_polar = [cw_polar; zeros(outlen-N,1)];
    
    % polar_code = PolarCode(block_length, info_length, epsilon, crc_size);
    epsilon = 0.32;
    crc_size = 0;
    %polar_coder = PolarCode(N, K, epsilon, crc_size);
    cw_polarlsc = polar_coder.encode(double(crced'));
    cw_polarlsc = [cw_polarlsc.'; zeros(outlen-N,1)];
    
    %cw = cw_nrep;
    %cw = cw_polar;
    cw = cw_polarlsc;
end

% Decode a single codeword
% function [out,err,cbsbuffers,segerr] = pc_decode(chs,trblklen,sbits,cbsbuffers)
function [out,err] = pc_decode(chs,trblklen,sbits, polar_coder)

%     % Rate recovery
%     raterecovered = lteRateRecoverTurbo(sbits,trblklen,chs.RV,chs,cbsbuffers);
%     
%     % Channel decoding
%     turbodecoded = lteTurboDecode(raterecovered,chs.NTurboDecIts);
%     
%     % Code block desegmentation and code block CRC decoding
%     [desegmented_turbo,segerr] = lteCodeBlockDesegment(turbodecoded,trblklen+24);
%     segerr = (segerr~=0);
    
%     decoded  = sbits(1:3:length(sbits)) + ...
%                sbits(3:3:length(sbits)) + ...
%                sbits(3:3:length(sbits));
%     desegmented  = decoded > 2;
%     desegmented_nrep  = desegmented(1:trblklen+24);
    
        % N=256; K=128;
    N=pow2(floor(log2(double(length(sbits)))));
    %K = N/2;
    K = trblklen+24;    
    %designSNRdB = 0;
    %initPC(N,K,1,2, designSNRdB,1); %silent, no output
    
%     Ec=1; N0=2; designSNRdB=0;silentflag=1;%0dB SNR
%     %initPC(N,K,Ec,N0);    
%     initPC(N,K,Ec,N0,designSNRdB,silentflag);
%     
%     %padded = [crced; zeros(K-length(crced),1)];      
%     %cw_polar = pencode(padded);
%     %cw_polar = [cw_polar; zeros(outlen-N,1)];    
% 
%     polardecoded = pdecode(sbits(1:N));
%     desegmented_polar  = polardecoded(1:trblklen+24);
    
    %decoded = polar_code.decode_scl_p1(1, 1, 32);
    
    epsilon = 0.32;
    crc_size = 0;
    %polar_coder = PolarCode(N, K, epsilon, crc_size);
    list_size = 32;
    polarlscdecoded = polar_coder.decode_scl_llr(sbits(1:N), list_size);
    desegmented_polarlsc  = polarlscdecoded(1:trblklen+24);
    
    %print size
    %size(desegmented_turbo)
    %size(desegmented_nrep)
    %size(desegmented_polar)
    
    %desegmented = desegmented_polar;
    desegmented = desegmented_polarlsc;
                    
    % Transport block CRC decoding
    [out,err] = lteCRCDecode(desegmented,'24A');
    if (isempty(out))
        % treat decoding to a TBS of zero as a non-existent rather than
        % empty transport block, so set the CRC to a pass
        err = false;
    else
        err = (err~=0);
    end
    
%     % For an empty data input, the code block soft buffer state should be
%     % empty, otherwise it is the output of the rate recovery
%     % rate recovery
%     if (isempty(sbits))
%         cbsbuffers = cell(1,0);
%     else
%         cbsbuffers = raterecovered;
%     end
    
end


% EOF
   