figure
hold on

ep = [0:0.05:1]
legendInfo = {};

for i = 1:length(ep)
    c0 = PolarCode.calculate_channel_polarization(ep(i), 12);
    plot(sort(c0))
    
    legendInfo{i} = ['epsilon = ' num2str(ep(i))]; 
end

legend(legendInfo)
hold off