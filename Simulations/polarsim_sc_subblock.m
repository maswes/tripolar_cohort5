function [ber, numBits] = polarsim_sc_subblock(EbNo, maxNumErrs, maxNumBits)
    % Polar decoder simulation with List successive cancellation decoding for BERTool.
    % Import Java class of BERTool
    import com.mathworks.toolbox.comm.BERTool;

    simParams.enb = lteRMCDLCustom('R.2');

    % Define number of bits per symbol (k). M = 4 for QPSK or 2 for BPSK.
    M = 4;
    k = log2(M);

    % Code rate
    codeRate = simParams.enb.PDSCH.TargetCodeRate;
    
    % Create a M-ary comm.PSKModulator and a comm.PSKDemodulator System object.
    % The 'SymbolMapping' property of both the objects is by default set to
    % employ Gray coding.
    qpskMod = comm.PSKModulator(M,...
        'BitInput',true);
    qpskDemod = comm.PSKDemodulator(M,...
        'BitOutput',true, 'DecisionMethod', 'Log-likelihood ratio');

    % Create a comm.AWGNChannel System object. 
    chan = comm.AWGNChannel(...
        'NoiseMethod', 'Signal to noise ratio (Eb/No)',...
        'SignalPower', 1,...
        'SamplesPerSymbol', 1);

    % Adjust SNR for coded bits and multi-bit symbols and set the 'EbNo'
    % property of the comm.AWGNChannel object to this adjusted value.
    chan.EbNo = EbNo - 10*log10(1/codeRate) + 10*log10(k);

    % Create a comm.ErrorRate System object to compare the decoded bits to the
    % original transmitted bits. The output of the comm.ErrorRate object is a
    % three-element vector containing the calculated bit error rate (BER), the
    % number of errors observed, and the number of bits processed. The Viterbi
    % decoder creates a delay in the output decoded bit stream equal to the
    % traceback length. To account for this delay set the 'ReceiveDelay'
    % property of the comm.ErrorRate object to the value of the
    % 'TraceBackDepth' property of the comm.ViterbiDecoder object.
    %errorCalc = comm.ErrorRate('ReceiveDelay', dec.TracebackDepth);
    errorCalc = comm.ErrorRate;

    % Create a vector to store current values of the bit error rate, errors
    % incurred and number of bits processed.

    epsilon = 0.32;
    crc_size = 24;
    chs = 0;
    
    Ns = 4096;
    N = min(Ns, simParams.enb.PDSCH.PolarCodedTrBlkSizes(10));
    nS = floor(simParams.enb.PDSCH.CodedTrBlkSizes(10) ./ N);
    K = floor((simParams.enb.PDSCH.TrBlkSizes(10)+24) ./ nS);
    polar_coder = PolarCode(N, K, epsilon, crc_size);    
   
    for i = 1:10        
        if (simParams.enb.PDSCH.TrBlkSizes(i) > 0 && simParams.enb.PDSCH.TrBlkSizes(i) ~=simParams.enb.PDSCH.TrBlkSizes(10))            
            N = min(Ns, simParams.enb.PDSCH.PolarCodedTrBlkSizes(i));
            nS = floor(simParams.enb.PDSCH.CodedTrBlkSizes(i) ./ N);
            K = floor((simParams.enb.PDSCH.TrBlkSizes(i)+24) ./ nS);
            polar_coders(i) = PolarCode(N, K, epsilon, crc_size);    
        else
            polar_coders(i) = polar_coder;
        end
    end
    
    % Set the number of bits per iteration
    bitsPerIter = sum(simParams.enb.PDSCH.TrBlkSizes(:));

    v = zeros(1,3);
    
    % Exit loop when either the number of bit errors exceeds 'maxNumErrs'
    % or the maximum number of iterations have been completed
    while ((v(2) < maxNumErrs) && (v(3) <= maxNumBits))

        % Check if the user has clicked the Stop button of BERTool
        if (BERTool.getSimulationStop)
            break;
        end

         data = randi([0 1], bitsPerIter, 1);    % Generate message bits                                           
         curr = 1;
         for i = 1:10
             TrBlkSize = simParams.enb.PDSCH.TrBlkSizes(i);
             CodedTrBlkSize = simParams.enb.PDSCH.CodedTrBlkSizes(i);
             %CodedTrBlkSize = simParams.enb.PDSCH.PolarCodedTrBlkSizes(i);
             trBlk = data(curr:curr+TrBlkSize-1);
             if (TrBlkSize > 0)
                 encData = pc_encode(chs,CodedTrBlkSize,trBlk,polar_coders(i));
                 modData = qpskMod(encData);             % Modulate
                 channelOutput = chan(modData);          % AWGN channel
                 demodData = qpskDemod(channelOutput);   % Demodulate
                 decData = pc_decode(chs,TrBlkSize,demodData,polar_coders(i));
                 v = errorCalc(double(trBlk), double(decData));           % Count errors                                     
                 curr = curr + TrBlkSize;
             end
         end
    end

% Assign values to ber and numBits
ber = v(1);
numBits = v(3);

end 

% Encode a single codeword
function cw = pc_encode(chs,outlen,trblkin, polar_coder)
    
    % Transport block CRC attachment
     crced = lteCRCEncode(trblkin,'24A');
     
    N=pow2(floor(log2(double(outlen))));
    K = length(crced);
        
    epsilon = 0.32;    
    Ns = 4096;    
    cw_polarlsc = [];

    if (N > Ns)
        
        numSubBlock = floor(outlen ./ Ns);    
        Ks = K ./ numSubBlock;
        
        % Add CRC per sub-block
        crc_size = 24;        
        polar_coder = PolarCode(Ns, Ks, epsilon, crc_size);        
        for i = 1:numSubBlock
            segment = crced((i-1)*Ks+1:i*Ks).';
            coded = polar_coder.encode(double(segment)).';
            cw_polarlsc = [cw_polarlsc; coded];
        end
    else       
        crc_size = 24;
        polar_coder = PolarCode(N, K, epsilon, crc_size);
        cw_polarlsc = polar_coder.encode(double(crced')).';
    end
    
    %cw_polarlsc = [cw_polarlsc.'; zeros(outlen-N,1)];
    cw_polarlsc = [cw_polarlsc; randi([0 1], outlen-length(cw_polarlsc),1)];          
    
    %cw = cw_nrep;
    %cw = cw_polar;
    cw = cw_polarlsc;
end

% Decode a single codeword
% function [out,err,cbsbuffers,segerr] = pc_decode(chs,trblklen,sbits,cbsbuffers)
function [out,err] = pc_decode(chs,trblklen,sbits, polar_coder)

%     % Rate recovery
%     raterecovered = lteRateRecoverTurbo(sbits,trblklen,chs.RV,chs,cbsbuffers);
%     
%     % Channel decoding
%     turbodecoded = lteTurboDecode(raterecovered,chs.NTurboDecIts);
%     
%     % Code block desegmentation and code block CRC decoding
%     [desegmented_turbo,segerr] = lteCodeBlockDesegment(turbodecoded,trblklen+24);
%     segerr = (segerr~=0);
    
    N=pow2(floor(log2(double(length(sbits)))));
    K = trblklen+24;    
    
    epsilon = 0.32;    
    list_size = 1;
    blkLen = length(sbits);
    
    Ns = 4096;    
    polarlscdecoded = [];

    %if (blkLen - N > Ns)
    if (N > Ns)

        numSubBlock = floor(blkLen ./ Ns);    
        Ks = K ./ numSubBlock;
        
        % Add CRC per sub-block
        crc_size = 24;        
        polar_coder = PolarCode(Ns, Ks, epsilon, crc_size);        
        for i = 1:numSubBlock
            subBlock = sbits((i-1)*Ns+1:i*Ns);            
            polarlscdecoded = [polarlscdecoded; polar_coder.decode_scl_llr(subBlock, list_size).'];
        end
    else       
        crc_size = 24;
        polar_coder = PolarCode(N, K, epsilon, crc_size);
        polarlscdecoded = polar_coder.decode_scl_llr(sbits(1:N), list_size);
    end

    desegmented_polarlsc  = polarlscdecoded(1:trblklen+24);
    
    %desegmented = desegmented_polar;
    desegmented = desegmented_polarlsc;
                    
    % Transport block CRC decoding
    [out,err] = lteCRCDecode(desegmented,'24A');
    if (isempty(out))
        % treat decoding to a TBS of zero as a non-existent rather than
        % empty transport block, so set the CRC to a pass
        err = false;
    else
        err = (err~=0);
    end

end


% EOF
   