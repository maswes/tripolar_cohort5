function [ber, numBits] = lteturbosim(EbNo, maxNumErrs, maxNumBits)

    % Import Java class of BERTool
    import com.mathworks.toolbox.comm.BERTool;

    enb = lteRMCDLCustom('R.2');

    % Define number of bits per symbol (k). M = 4 for QPSK or 2 for BPSK.
    M = 4;
    k = log2(M);

    % Code rate
    codeRate = enb.PDSCH.TargetCodeRate;
    
    % Create a M-ary comm.PSKModulator and a comm.PSKDemodulator System object.
    % The 'SymbolMapping' property of both the objects is by default set to
    % employ Gray coding.
    qpskMod = comm.PSKModulator(M,...
        'BitInput',true);
    qpskDemod = comm.PSKDemodulator(M,...
        'BitOutput',true, 'DecisionMethod', 'Log-likelihood ratio');

    % Create a comm.AWGNChannel System object. 
    chan = comm.AWGNChannel(...
        'NoiseMethod', 'Signal to noise ratio (Eb/No)',...
        'SignalPower', 1,...
        'SamplesPerSymbol', 1);

    % Adjust SNR for coded bits and multi-bit symbols and set the 'EbNo'
    % property of the comm.AWGNChannel object to this adjusted value.
    chan.EbNo = EbNo - 10*log10(1/codeRate) + 10*log10(k);

    % Create a comm.ErrorRate System object to compare the decoded bits to the
    % original transmitted bits. The output of the comm.ErrorRate object is a
    % three-element vector containing the calculated bit error rate (BER), the
    % number of errors observed, and the number of bits processed. The Viterbi
    % decoder creates a delay in the output decoded bit stream equal to the
    % traceback length. To account for this delay set the 'ReceiveDelay'
    % property of the comm.ErrorRate object to the value of the
    % 'TraceBackDepth' property of the comm.ViterbiDecoder object.
    %errorCalc = comm.ErrorRate('ReceiveDelay', dec.TracebackDepth);
    errorCalc = comm.ErrorRate;

    % Create a vector to store current values of the bit error rate, errors
    % incurred and number of bits processed.

    % Set the number of bits per iteration
    bitsPerIter = sum(enb.PDSCH.TrBlkSizes(:));

    v = zeros(1,3);
        
    % Exit loop when either the number of bit errors exceeds 'maxNumErrs'
    % or the maximum number of iterations have been completed
    while ((v(2) < maxNumErrs) && (v(3) <= maxNumBits))

        % Check if the user has clicked the Stop button of BERTool
        if (BERTool.getSimulationStop)
            break;
        end

         data = randi([0 1], bitsPerIter, 1);    % Generate message bits                                           
         curr = 1;
         
         for sf = 1:10
             TrBlkSize = enb.PDSCH.TrBlkSizes(sf);
             CodedTrBlkSize = enb.PDSCH.CodedTrBlkSizes(sf);
             %CodedTrBlkSize = enb.PDSCH.PolarCodedTrBlkSizes(i);
             trBlk = data(curr:curr+TrBlkSize-1);
             if (TrBlkSize > 0)
                 
                 encData = lteDLSCH(enb,enb.PDSCH,CodedTrBlkSize,trBlk);
                 modData = qpskMod(encData);             % Modulate
                 channelOutput = chan(modData);          % AWGN channel
                 demodData = -1 * qpskDemod(channelOutput);   % Demodulate
                 [decData , blkcrc] = lteDLSCHDecode(enb,enb.PDSCH, TrBlkSize, demodData);                 
                 v = errorCalc(double(trBlk), double(decData));           % Count errors                                     
                 curr = curr + TrBlkSize;
             end
         end
    end

% Assign values to ber and numBits
ber = v(1);
numBits = v(3);

end

% EOF