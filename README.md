# README #

New methodologies and technologies are needed to meet the ever-increasing consumer demand for big data transfer. In the last two decades, breakthroughs have been made in wireless communications with the advent of techniques such as Orthogonal Frequency Division Multiplexing (OFDM), Receive and Transmit diversity (e.g. Multiple Input Multiple Output (MIMO)), and better channel coding techniques. In today�s smartphones, channel coding is one of the most complex and energy consuming modules in the baseband system. Turbo codes and Low Density Parity Check (LDPC) codes were key to the development of 3G and 4G, providing the wireless performance consumers enjoy today. For the next wireless network generation, 5G, polar coding is being explored to enable the networks as it promises to achieve the maximum theoretical channel capacity (ie. Shannon capacity), required to drive big data. In this project, we implemented an OFDM modem with polar coding. We examined an optimized polar coding algorithm implemented using High Level Synthesis (HLS), and analyzed the performance compared to other coding schemes under real world scenarios.


* Capstone project
