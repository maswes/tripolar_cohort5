#ifndef _HLS_VECTOR_
#define _HLS_VECTOR_

template <class X> void hls_sort(X *items, int count);

template <class X> void hls_sort(X *items, int count)
{
	X t;
	for (int a = 1; a<count; a++)
#pragma HLS LOOP_TRIPCOUNT min=1 max=32
		for (int b = count - 1; b >= a; b--)
#pragma HLS LOOP_TRIPCOUNT min=1 max=16
			if (items[b - 1] < items[b]) {
				t = items[b - 1];
				items[b - 1] = items[b];
				items[b] = t;
			}
}
#ifdef _MSC_VER
//#define __SYNTHESIS__
//#define USE_STL_VECTOR
#endif

#ifdef USE_STL_VECTOR
#include <vector>
using namespace std;
#define hls_vector vector

#else // hls_vector

#define MAX_ELEMENT 16UL

template <class T>
class hls_vector
{
public:
  // Constructors
  hls_vector();
  hls_vector(unsigned count);
  hls_vector(unsigned count, T const &);

  //Member functions
  void push_back (T const &);
  void pop_back ();
  unsigned int size(void);
  void resize(unsigned int new_size);
  void resize(unsigned int new_size, T const &);
  T top ();
  bool empty ();
  T & at(unsigned int);
  T & operator[](unsigned int index);  
  T & back();
  void sort();

private:
  T elements[MAX_ELEMENT];
  unsigned int vector_size;
};


template <class T>
hls_vector <T>::hls_vector ()
{
  vector_size = 0;
}

template <class T>
hls_vector <T>::hls_vector (unsigned count)
{
#ifndef __SYNTHESIS__
if (count > MAX_ELEMENT)
  {
    while (1);
  }
#endif
  vector_size = count;
}

template <class T>
hls_vector <T>::hls_vector (unsigned count, T const &elem)
{
#ifndef __SYNTHESIS__
if (count > MAX_ELEMENT)
  {
    while (1);
  }
#endif
  vector_size = count;

  for (unsigned int i = 0; i < count; i++)
    elements[i] = elem;
}

template <class T>
void hls_vector <T>::push_back (T const &elem)
{
  elements[vector_size] = elem;
  vector_size++;
}

template <class T>
void hls_vector <T>::pop_back ()
{
#ifndef __SYNTHESIS__
  if (vector_size == 0)
    {
      while(1);
    }
#endif
  vector_size--;
}

template <class T>
unsigned int hls_vector <T>:: size(void)
{
  return vector_size;
}

template <class T> 
void hls_vector <T>:: resize(unsigned int new_size)
{
#ifndef DISABLE_BROKEN
  //for (unsigned int i =0; i < new_size - vector_size; i++)
  //  elements[vector_size+i] = 0;

  vector_size = new_size;
#endif
}

template <class T> 
void hls_vector <T>:: resize(unsigned int new_size, T const &elem)
{
  for (unsigned int i =0; i < new_size - vector_size; i++)
    elements[vector_size+i] = elem;

  vector_size = new_size;
}

template <class T>
T hls_vector <T>::top ()
{
#ifndef __SYNTHESIS__
  if (vector_size == 0)
  {
      while(1);
  }
#endif
  return elements[vector_size];
}

template <class T>
bool hls_vector <T>::empty ()
{
  if (vector_size == 0) return 1;
    else return 0;
}

template <class T>
T &hls_vector <T>::at (unsigned int i)
{
#ifndef __SYNTHESIS__
  if (vector_size == 0)
    {
      while(1);
    }
#endif
  return elements[i];
}

template<class T>
T& hls_vector<T>::operator[](unsigned int index)
{
    return elements[index];
}
  
template<class T>
T& hls_vector<T>::back ()
{
    return elements[vector_size-1];
}  

template <class T>
void hls_vector <T>::sort()
{
#ifndef __SYNTHESIS__
	if (vector_size == 0)
	{
		while (1);
	}
#endif
	hls_sort(elements, vector_size);
   
}


/*-----------------------------------------------Other utility functions ------------------------------------------- */
template<class InputIt, class OutputIt>
OutputIt hls_copy(InputIt first, InputIt last,
	OutputIt d_first)
{
	while (first != last) {
		*d_first++ = *first++;
	}
	return d_first;
}

template<class T>
const T& hls_max(const T& a, const T& b)
{
	return (a < b) ? b : a;
}

template<class T>
const T& hls_min(const T& a, const T& b)
{
	return (b < a) ? b : a;
}
#endif //USE_HLS_VECTOR

#endif
