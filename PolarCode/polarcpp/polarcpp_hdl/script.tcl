############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2016 Xilinx, Inc. All Rights Reserved.
############################################################
open_project polarcpp_hdl
set_top xillybus_wrapper
add_files ../PolarCode.cpp
add_files ../PolarCode.h
add_files ../hls_stack.h
add_files ../hls_vector.h
add_files ../xillybus_wrapper.cpp
add_files -tb ../main.cpp
open_solution "solution1"
set_part {xc7z020clg484-1}
create_clock -period 10 -name default
#source "./polarcpp_hdl/solution1/directives.tcl"
#csim_design
#csynth_design
#cosim_design
#export_design -format ip_catalog
