#ifndef _HLS_STACK_
#define _HLS_STACK_

#include "hls_vector.h"

#ifdef _MSC_VER
//#define __SYNTHESIS__
//#define USE_STL_VECTOR
#endif

#ifdef _DEBUG_
#include <iostream>
#endif

template <class T>
class hls_stack
{
private:
  hls_vector <T> elements;

public:
  hls_stack();
  void push (T const &);
  void pop ();
  T top ();
  bool empty ();
  unsigned int size();
  void reset ();
};

template <class T>
hls_stack <T>::hls_stack ()
{
  elements.resize(0);
}

template <class T>
void hls_stack <T>::push (T const &elem)
{
  elements.push_back (elem);
}

template <class T> 
void hls_stack <T>::pop ()
{
#ifndef __SYNTHESIS__
  if (elements.empty ())
    {
      while (1);
    }
#endif

  elements.pop_back ();
}

template <class T>
T hls_stack <T>::top ()
{
#ifndef __SYNTHESIS__
  if (empty ())
    {
      //std::cout<<"empty stack vector"<<std::endl;
      while (1);
    }
#endif
  return elements.back ();
}

template <class T>
bool hls_stack <T>::empty ()
{
  return elements.empty ();
}

template <class T>
unsigned int hls_stack <T>::size ()
{
  return elements.size();
}

template <class T>
void hls_stack <T>::reset ()
{
  elements.resize(0);
}
#endif

