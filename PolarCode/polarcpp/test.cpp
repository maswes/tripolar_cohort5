#include "hls_vector.h"
#include "hls_stack.h"
#include <iostream>

using namespace std;

// resizing vector
#include <iostream>
#include <vector>

int resize_test ()
{
  int x = 0;
  hls_vector<int> myvector;

  cout<<"resize_test ...."<<endl;
  // set some initial content:
  for (int i=1;i<10;i++) myvector.push_back(i);

  myvector.resize(5);
  x = myvector.size();
  cout<<"Hello ...resize 5" <<x<<endl;

  myvector.resize(8,100);
  x = myvector.size();
  cout<<"Hello ...resize 8,100" <<x<<endl;

  myvector.resize(12);
  x = myvector.size();
  cout<<"Hello ...resize(12)" <<x<<endl;

  std::cout << "myvector contains:";
  for (unsigned int i=0;i<myvector.size();i++)
    std::cout << ' ' << myvector[i];
  std::cout << '\n';

  return 0;
}

void
stack_test()
{
  int x = 0;
  hls_stack <int>mystack;

  mystack.push(1);
  mystack.push(2);
  mystack.push(3);
  mystack.push(4);
  mystack.push(5);

  x = mystack.top();
  cout<<"Stack top 5: "<<x<<endl;

  x = mystack.size();
  cout<<"Stack Size 5: "<<x<<endl;
  mystack.pop();
  mystack.pop();
  x = mystack.size();
  cout<<"Stack Size 3: "<<x<<endl;

}

int main ()
{
  hls_vector<int> myvector;
  float x;

  stack_test();
  resize_test();

  cout << "start .."<<endl;
  x = myvector.size();
  cout<<"Hello ...Init size" <<x<<endl;

  myvector.push_back(1);
  x = myvector.size();
  cout<<"Hello ...size" <<x<<endl;

  myvector.push_back(2);
  x = myvector.size();
  cout<<"Hello ...size" <<x<<endl;

  myvector.push_back(3);
  x = myvector.size();
  cout<<"Hello ...size" <<x<<endl;
  
  myvector.push_back(4);
  x = myvector.size();
  cout<<"Hello ...br size" <<x<<endl;

  cout<<"Resize ... 3 "<<endl;
  myvector.resize(3);
  x = myvector.size();
  cout<<"Hello ...ar size" <<x<<endl;
  
  myvector.push_back(55);
  x = myvector.size();
  cout<<"Hello ... size" <<x<<endl;

  cout<<"Pop test ..."<<endl;
  myvector.pop_back();
  x = myvector.size();
  cout<<"Hello ... size " <<x<<endl;

  cout<<".at ..."<<endl;

  float data = 0;
  
  myvector.resize(10);
  x = myvector.size();
  cout<<"Hello ... resized to size " <<x<<endl;

  for (unsigned int m=0;m<myvector.size(); m++) 
  {
    //myvector[m]= 11 * m;
    myvector.at(m)= 11 * m;
  }
  for (unsigned int m = 0; m<10; m++)
  {
	  myvector.push_back(11.2 * -m);
  }
  x = myvector.size();
  cout<<"Hello ... size " <<x<<endl;

  cout << "before sort ..." << endl;
  for (unsigned int m=0;m<myvector.size(); m++)
  {
    data = myvector.at(m);
    cout << data << endl;
  }

  myvector.sort();

  cout << "after sort ..." << endl;
  for (unsigned int m = 0; m<myvector.size(); m++)
  {
	  data = myvector.at(m);
	  cout << data << endl;
  }

  return 0;
}
