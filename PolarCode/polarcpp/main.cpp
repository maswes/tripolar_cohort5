#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>
#include <cmath>
#include <vector>
#include <cstdlib>
#include <cmath>
#include <limits>

#ifdef _MSC_VER
#include <chrono>
#include <functional>
#include <random>
#endif

using namespace std;

#include "PolarCode.h"


double generateGaussianNoise(double mu, double sigma)
{
	//static const double epsilon = std::numeric_limits<double>::min();
	static const double epsilon = 1.175494e-38;
	static const double two_pi = 2.0*3.14159265358979323846;

	static double z0, z1;
	static bool generate;
	generate = !generate;

	if (!generate)
		return z1 * sigma + mu;

	double u1, u2;
	do
	{
		u1 = rand() * (1.0 / RAND_MAX);
		u2 = rand() * (1.0 / RAND_MAX);
	} while (u1 <= epsilon);

	z0 = sqrt(-2.0 * log(u1)) * cos(two_pi * u2);
	z1 = sqrt(-2.0 * log(u1)) * sin(two_pi * u2);
	return z0 * sigma + mu;
}

extern PolarCode polar_code;


hls_vector<hls_vector<dtype> > PolarCode::get_bler_quick(hls_vector<dtype> ebno_vec,
		hls_vector<uint8_t> list_size_vec) {

    int max_err = 100;
    int max_runs = 1000;

    hls_vector<hls_vector<dtype> > bler;
    hls_vector<hls_vector<dtype> > num_err;
    hls_vector<hls_vector<dtype> > num_run;

    bler.resize(list_size_vec.size());
    num_err.resize((list_size_vec.size()));
    num_run.resize(list_size_vec.size());

    for (unsigned l = 0; l < list_size_vec.size(); ++l) {
        bler.at(l).resize(ebno_vec.size(), 0);
        num_err.at(l).resize(ebno_vec.size(), 0);
        num_run.at(l).resize(ebno_vec.size(), 0);
    }

	dtype bpsk[MAX_BLOCK_LEN];
	dtype received_signal[MAX_BLOCK_LEN];

	uint8_t coded_bits[MAX_BLOCK_LEN];
	uint8_t info_bits[MAX_INFO_LEN];

    dtype N_0  = 1.0;
	dtype noise[MAX_BLOCK_LEN];

#ifdef _MSC_VER
    std::normal_distribution<dtype> gauss_dist(0.0f, N_0);
    std::default_random_engine generator;
	std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
#endif

	dtype llr[MAX_BLOCK_LEN];



    for (int run = 0; run < max_runs; ++run) {
#ifdef _MSC_VER
        if ((run % (max_runs/100)) == 0) {
            std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
            auto duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
            std::cout << "Running iteration " << run << "; time elapsed = " << duration/1000/1000 << " seconds"
                "; percent complete = " << (100 * run)/max_runs << "." << std::endl;
        }
#endif
        if ( (run % 100) == 0) {
            for(uint16_t i = 0; i < _info_length; ++ i ){
                //TODO: info_bits.at(i) = (uint8_t) ( rand() % 2);
                info_bits[i] = (uint8_t) ( i % 2);
            }
        }
        for(uint16_t i = 0; i < _block_length; ++ i ) {
//#ifdef _MSC_VER
#if 0
			noise[i] = (dtype)gauss_dist(generator);
#endif
			noise[i] = generateGaussianNoise(0.0f, 1.0f);
 
        }

        encode(info_bits, coded_bits);

        for(uint16_t i = 0; i < _block_length; ++ i ) {
            bpsk[i] = 2.0f * ((dtype) coded_bits[i]) - 1.0f;
        }

        for (unsigned l_index = 0; l_index < list_size_vec.size(); ++l_index) {

        	hls_vector<bool> prev_decoded(0);
            prev_decoded.resize(ebno_vec.size(), false);

            for (unsigned i_ebno = 0; i_ebno < ebno_vec.size(); ++i_ebno) {

                if ( num_err.at(l_index).at(i_ebno) > max_err )
                    continue;

                num_run.at(l_index).at(i_ebno)++;

                bool run_sim = true;

                for(unsigned i_ebno2 = 0; i_ebno2 < i_ebno; ++i_ebno2) {
                    if (prev_decoded.at(i_ebno2)) {
                        //  This is a hack to speed up simulations -- it assumes that this run will be decoded
                        // correctly since it was decoded correctly for a lower EbNo
                        run_sim = false;
                    }
                }

                 if (!run_sim) {
                     continue;
                 }

                dtype snr_sqrt_linear = std::pow(10.0f, ebno_vec.at(i_ebno)/20)
                                         * std::sqrt( ((dtype) _info_length )/((dtype) (_block_length) )) ;
                for (uint16_t i = 0; i < _block_length; ++i) {
                    received_signal[i] = snr_sqrt_linear * bpsk[i] + std::sqrt(N_0 / 2) * noise[i];
                }
                for (uint16_t i = 0; i < _block_length; ++i) {
					llr[i] = -4 * received_signal[i] * snr_sqrt_linear / N_0;
				}

				uint8_t decoded_info_bits[MAX_INFO_LEN];
				decode_scl_llr(llr, list_size_vec.at(l_index), decoded_info_bits);

                bool err = false;
                for (uint16_t i = 0; i < _info_length; ++i) {
					if (info_bits[i] != decoded_info_bits[i]) {
                        err = true;
                        break;
                    }
                }

                if (err)
                    num_err.at(l_index).at(i_ebno)++;
                else
                    prev_decoded.at(i_ebno) = true;

            } // EbNo Loop End

        } //List_size loop end

    } // run loop end

    for (unsigned l_index = 0; l_index < list_size_vec.size(); ++l_index) {
        for (unsigned i_ebno = 0; i_ebno < ebno_vec.size(); ++i_ebno) {
            bler.at(l_index).at(i_ebno) = num_err.at(l_index).at(i_ebno)/num_run.at(l_index).at(i_ebno);
        }
    }

    return bler;

}


int main()
{
    dtype ebno_log_min = 1.00f;
    dtype ebno_log_max = 2.01f;
    dtype ebno_log_increment = 0.80f;
    hls_vector <dtype> ebno_vec;

    for (dtype ebno_log = ebno_log_min; ebno_log <= ebno_log_max; ebno_log += ebno_log_increment)
        ebno_vec.push_back(ebno_log);

    hls_vector<uint8_t> list_size_vec(0);
    list_size_vec.push_back(1);
#if 0    
    list_size_vec.push_back(2);
    list_size_vec.push_back(4);
    list_size_vec.push_back(8);
    list_size_vec.push_back(32);
#endif
    hls_vector<hls_vector<dtype> > bler = polar_code.get_bler_quick(ebno_vec, list_size_vec);

    for (unsigned i_ebno = 0; i_ebno < ebno_vec.size(); ++i_ebno) {
        std::cout <<"SNR(dB) " << std::fixed  << std::setprecision(3) << ebno_vec.at(i_ebno) << "\t \t";
        for (unsigned i_list = 0; i_list < list_size_vec.size(); ++i_list) {
            std::cout <<"BLER "<< std::fixed  << std::setprecision(6) << bler.at(i_list).at(i_ebno) << "\t";
        }
        std::cout << std::endl;
    }

#ifdef _MSC_VER
	getchar();
#endif
    return 0;
}


