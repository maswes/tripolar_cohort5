#ifndef POLARC_POLARCODE_H
#define POLARC_POLARCODE_H

#define MAX_LOGN 11
#define MAX_BLOCK_LEN (1 << MAX_LOGN)
#define MAX_INFO_LEN MAX_BLOCK_LEN
#define MAX_CRC_LEN 32
#define MAX_LIST_SIZE 4

#define STATIC_INIT

//#define DISABLE_HLS_BROKEN
//#define DISABLE_BROKEN
//#define DUMP_LOG

#include <stack>          // hls_stack
#include<stdint.h>

//#ifdef __SYNTHESIS__
#if 0
#include <ap_fixed.h>
#include "hls_math.h"
#endif

#include "hls_vector.h"
#include "hls_stack.h"
#include "init_frozen_bits.h"

#define MAX_LOGN 11
#define MAX_BLOCK_LEN (1 << MAX_LOGN)
#define MAX_INFO_LEN MAX_BLOCK_LEN
#define MAX_CRC_LEN 32
#define MAX_LIST_SIZE 4

typedef float dtype;
//typedef double dtype;
//typedef ap_fixed<18,6,AP_RND> dtype;

using namespace std;

class PolarCode {


public:

    PolarCode(uint8_t num_layers, uint16_t info_length, dtype epsilon, uint16_t crc_size) :
            _n(num_layers), _info_length(info_length), _design_epsilon(epsilon),
            _crc_size(crc_size)
    {
        _block_length = (uint16_t) (1 << _n);
        create_bit_rev_order();
#ifndef STATIC_INIT
        initialize_frozen_bits();
#endif
		_list_size = MAX_LIST_SIZE;
		initializeDataStructures();
	}

	void encode(uint8_t info_bits[MAX_BLOCK_LEN], uint8_t coded_bits[MAX_BLOCK_LEN]);
    void decode_scl_llr(dtype llr_in[MAX_BLOCK_LEN], uint16_t list_size, uint8_t decoded_bits_out[MAX_INFO_LEN]);
    void decode_scl(dtype llr_in[MAX_BLOCK_LEN], uint8_t decoded_bits_out[MAX_INFO_LEN]);

 	hls_vector<hls_vector<dtype> > get_bler_quick(hls_vector<dtype> ebno_vec, hls_vector<uint8_t> list_size_vec);

private:

    uint8_t _n;
    uint16_t _info_length;
    uint16_t _block_length;
    uint16_t _crc_size;

    dtype _design_epsilon;

#ifndef STATIC_INIT
	bool _frozen_bits[MAX_BLOCK_LEN];
	uint16_t _channel_order_descending[MAX_BLOCK_LEN];
	uint32_t _crc_matrix[MAX_INFO_LEN];
#endif // !STATIC_INIT

	uint16_t _bit_rev_order[MAX_BLOCK_LEN];

    void initialize_frozen_bits();
    void create_bit_rev_order();

#define  _llr_based_computation true;

    hls_vector<dtype> _pathMetric_LLR;

    uint16_t _list_size;

	stack<uint16_t> test;
	

    hls_stack<uint16_t> _inactivePathIndices;						//_list_size
    hls_vector<bool> _activePath;									//_list_size
    hls_vector<hls_vector<uint16_t> > _pathIndexToArrayIndex;		//_n+1; _list_size
    hls_vector<hls_stack<uint16_t> > _inactiveArrayIndices;			//_n+1; _list_size
    hls_vector<hls_vector<uint16_t> > _arrayReferenceCount;			//_n+1; _list_size

	dtype array_LLR[MAX_LIST_SIZE * 2 * MAX_BLOCK_LEN];
	uint8_t array_C[MAX_LIST_SIZE * 4 * MAX_BLOCK_LEN];
	uint8_t array_Info[MAX_LIST_SIZE][MAX_BLOCK_LEN];

	uint32_t _arrayIndex_LLR[MAX_LOGN+1][MAX_LIST_SIZE];
	uint32_t _arrayIndex_C[MAX_LOGN+1][MAX_LIST_SIZE];

    void initializeDataStructures();
	void resetDataStructures();
	uint16_t assignInitialPath();
    uint16_t clonePath(uint16_t);
    void killPath(uint16_t l);

    uint32_t getArrayIndex_LLR(uint16_t lambda, uint16_t  l);
    uint32_t getArrayIndex_C(uint16_t lambda, uint16_t  l);

	void CalcLLRLoop(uint16_t lambda, uint16_t phi);
	void CalcLLR(uint16_t lambda, uint16_t phi);
	void UpdateCLoop(uint16_t lambda, uint16_t phi);
	void UpdateC(uint16_t lambda, uint16_t phi);

    void continuePaths_FrozenBit(uint16_t phi);
    void continuePaths_UnfrozenBit(uint16_t phi);

    uint16_t findMostProbablePath(bool check_crc);

    bool crc_check(uint8_t * info_bits_padded);

};


#endif //POLARC_POLARCODE_H
