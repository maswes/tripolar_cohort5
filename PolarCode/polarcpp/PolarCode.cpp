#include "PolarCode.h"
#include <stdint.h>
#include <cmath>       /* log */

#ifdef _MSC_VER
#include <iostream>
#include <sstream>      // std::stringstream
#include <fstream>
#include <iomanip>      // std::setprecision
#include <functional>
#include <algorithm>
#endif


void PolarCode::initialize_frozen_bits() {

#ifndef	STATIC_INIT

	dtype channel_vec[MAX_BLOCK_LEN];

    for (uint16_t i = 0; i < _block_length; ++i) {
        channel_vec[i] = _design_epsilon;
    }
    for (uint8_t iteration = 0; iteration < _n; ++iteration) {
        uint16_t  increment = 1 << iteration;
        for (uint16_t j = 0; j < increment; j +=  1) {
            for (uint16_t i = 0; i < _block_length; i += 2 * increment) {
                dtype c1 = channel_vec[i + j];
                dtype c2 = channel_vec[i + j + increment];
                channel_vec[i + j] = c1 + c2 - c1*c2;
                channel_vec[i + j + increment] = c1*c2;
            }
        }
    }

    std::size_t n_t(0);

#ifdef _MSC_VER
    std::generate(std::begin(_channel_order_descending), std::end(_channel_order_descending), [&]{ return n_t++; });
    std::sort(  std::begin(_channel_order_descending),
                std::end(_channel_order_descending),
                [&](int i1, int i2) { return channel_vec[_bit_rev_order[i1]] < channel_vec[_bit_rev_order[i2]]; } );
#endif
    uint16_t  effective_info_length = _info_length + _crc_size;

    for (uint16_t i = 0; i < effective_info_length; ++i) {
		_frozen_bits[_channel_order_descending[i]] = 0;
	}
    for (uint16_t i = effective_info_length; i < _block_length; ++i) {
		_frozen_bits[_channel_order_descending[i]] = 1;
	}

#if 0
    _crc_matrix.resize(_crc_size);
    for (uint8_t bit = 0; bit < _crc_size; ++bit) {
        _crc_matrix.at(bit).resize(_info_length);
        for (uint16_t info_bit = 0; info_bit < _info_length; ++info_bit ) {
            //TODO: _crc_matrix.at(bit).at(info_bit) = (uint8_t) (rand() % 2);
            _crc_matrix.at(bit).at(info_bit) = (uint8_t) (info_bit % 2);
        }
    }
#endif
	uint32_t random = 0x35c985fa;
	for (uint16_t info_bit = 0; info_bit < _info_length; ++info_bit) {
		random = (random << 3) ^ (random >> 2) ^ (random >> 5) ^ (random >> 7);
		_crc_matrix[info_bit] = (uint32_t)(random & ((1 << MAX_CRC_LEN) - 1));
	}

#ifdef DUMP_LOG
	std::cout << "\n const static bool _frozen_bits[] = {";

	for (uint16_t i = 0; i < _block_length; ++i) {
		std::cout << _frozen_bits[i] << ", ";
	}

	std::cout << "};\n const static uint16_t _channel_order_descending[] = {";

	for (uint16_t i = 0; i < _block_length; ++i) {
		std::cout << hex << "0x" << _channel_order_descending[i] << ", ";
	}


	std::cout << "};\n\n const static uint32_t _crc_matrix[] = {";

	for (uint16_t info_bit = 0; info_bit < _info_length; ++info_bit) {
			std::cout << hex << "0x" << _crc_matrix[info_bit] << ", ";
	}

	std::cout << "};";

#endif

#endif	//STATIC_INIT

}

void PolarCode::encode(uint8_t info_bits[MAX_BLOCK_LEN], uint8_t coded_bits[MAX_BLOCK_LEN]) {

	uint8_t info_bits_padded[MAX_BLOCK_LEN];

    for (uint16_t i = 0; i < _info_length; ++i) {
#pragma HLS LOOP_TRIPCOUNT min=128 max=2048
        info_bits_padded[_channel_order_descending[i]] = info_bits[i];
    }

    for (uint16_t i = _info_length; i < _block_length; ++i) {
#pragma HLS LOOP_TRIPCOUNT min=128 max=2048
    	info_bits_padded[_channel_order_descending[i]] = 0;
    }


#if 0
	for (uint16_t i = _info_length; i < _info_length + _crc_size; ++i) {
        uint8_t  crc_bit = 0;
        for (uint16_t j = 0; j < _info_length; ++j) {
            crc_bit = (uint8_t) ((crc_bit + _crc_matrix.at(i - _info_length).at(j) * info_bits.at(j)) % 2);
        }
        info_bits_padded.at(_channel_order_descending[i]) = crc_bit;
    }
#endif
	for (uint16_t i = 0; i < _crc_size; ++i) {
#pragma HLS LOOP_TRIPCOUNT min=0 max=16
		uint8_t  crc_bit = 0;
		for (uint16_t j = 0; j < _info_length; ++j) {
#pragma HLS LOOP_TRIPCOUNT min=128 max=2048
			crc_bit = (uint8_t)((crc_bit + ((_crc_matrix[j] >> i) & 1) * info_bits[j]) % 2);
		}
		info_bits_padded[_channel_order_descending[i + _info_length]] = crc_bit;
	}

    for (uint8_t iteration = 0; iteration < _n; ++iteration) {
#pragma HLS LOOP_TRIPCOUNT min=7 max=12
        uint16_t  increment = (uint16_t) (1 << iteration);
        for (uint16_t j = 0; j < increment; j +=  1) {
#pragma HLS LOOP_TRIPCOUNT min=80 max=4096
            for (uint16_t i = 0; i < _block_length; i += 2 * increment) {
#pragma HLS LOOP_TRIPCOUNT min=1 max=1
                info_bits_padded[i + j] = (uint8_t)((info_bits_padded[i + j] + info_bits_padded[i + j + increment]) % 2);
            }
        }
    }

    for (uint16_t i = 0; i < _block_length; ++i) {
#pragma HLS LOOP_TRIPCOUNT min=256 max=4192
        coded_bits[i] = info_bits_padded[_bit_rev_order[i]];
    }

}

bool PolarCode::crc_check(uint8_t * info_bit_padded) {
    bool crc_pass = true;
#if 0
	for (uint16_t i = _info_length; i < _info_length + _crc_size; ++i) {
        uint8_t  crc_bit = 0;
        for (uint16_t j = 0; j < _info_length; ++j) {
            crc_bit = (uint8_t) ((crc_bit + _crc_matrix.at(i - _info_length).at(j) * info_bit_padded[_channel_order_descending.at(j)]) % 2);
        }

        if (crc_bit != info_bit_padded[_channel_order_descending[i]]) {
            crc_pass = false;
            break;
        }
#endif

	for (uint16_t i = 0; i < _crc_size; ++i) {
#pragma HLS LOOP_TRIPCOUNT min=0 max=16
		uint8_t  crc_bit = 0;
		for (uint16_t j = 0; j < _info_length; ++j) {
#pragma HLS LOOP_TRIPCOUNT min=128 max=2048
			crc_bit = (uint8_t)((crc_bit + ((_crc_matrix[j] >> i) & 1) * info_bit_padded[_channel_order_descending[j]]) % 2);
		}

		if (crc_bit != info_bit_padded[_channel_order_descending[i + _info_length]]) {
			crc_pass = false;
			break;
		}

    }

    return crc_pass;
}

void PolarCode::decode_scl_llr(dtype llr_in[MAX_BLOCK_LEN], uint16_t list_size, uint8_t decoded_bits_out[MAX_INFO_LEN]) {

    _list_size = list_size;

    //initializeDataStructures();
	resetDataStructures();

    uint16_t  l = assignInitialPath();

    dtype * llr_0 = &array_LLR[getArrayIndex_LLR(0,l)];

    for (uint16_t beta = 0; beta < _block_length; ++beta ) {
#pragma HLS LOOP_TRIPCOUNT min=256 max=4096
        llr_0[beta] = llr_in[beta];
    }

    decode_scl(llr_in, decoded_bits_out);

}

void PolarCode::decode_scl(dtype llr_in[MAX_BLOCK_LEN], uint8_t decoded_bits_out[MAX_INFO_LEN]) {

    for (uint16_t phi = 0; phi < _block_length; ++phi ){
#pragma HLS LOOP_TRIPCOUNT min=256 max=4096

        //recursivelyCalcLLR(_n, phi);
		CalcLLRLoop(_n, phi);

		if (_frozen_bits[phi] == 1)
            continuePaths_FrozenBit(phi);
        else
            continuePaths_UnfrozenBit(phi);

        if ((phi%2) == 1)
            //recursivelyUpdateC(_n, phi);
			UpdateCLoop(_n, phi);

    }
    uint16_t l = findMostProbablePath((bool) _crc_size);

    uint8_t * c_0 = &array_Info[l][0];
	for (uint16_t beta = 0; beta < _info_length; ++beta) {
#pragma HLS LOOP_TRIPCOUNT min=128 max=2048
		decoded_bits_out[beta] = c_0[_channel_order_descending[beta]];
	}

}

void PolarCode::resetDataStructures() {

	_inactivePathIndices.reset();

	for (int i = 0; i < _n + 1; ++i) {
#pragma HLS LOOP_TRIPCOUNT min=8 max=13
		_inactiveArrayIndices.at(i).reset();

	}

#if 1
	uint32_t ptr = 0;
	resetDataStructures_label0:for (uint16_t s = 0; s < _list_size; ++s) {
#pragma HLS LOOP_TRIPCOUNT min=1 max=32
		for (uint16_t lambda = 0; lambda < _n + 1; ++lambda) {
#pragma HLS LOOP_TRIPCOUNT min=8 max=13
			//_arrayIndex_LLR[lambda][s] = ptr;
			//_arrayIndex_C[lambda][s] = ptr << 1;

			_arrayReferenceCount.at(lambda).at(s) = 0;
			_inactiveArrayIndices.at(lambda).push(s);

			//ptr += (1 << (_n - lambda));
		}
		//ptr++;
	}
#endif // 1

	for (uint16_t l = 0; l < _list_size; ++l) {
#pragma HLS LOOP_TRIPCOUNT min=1 max=32
		_activePath.at(l) = 0;
		_inactivePathIndices.push(l);
		_pathMetric_LLR.at(l) = 0;
	}
}


void PolarCode::initializeDataStructures() {

	_inactivePathIndices.reset();
    _activePath.resize(_list_size);

#if 0
    if (_llr_based_computation) {
        _pathMetric_LLR.resize(_list_size);
        _arrayPointer_LLR.resize(_n + 1);
        for (int i = 0; i < _n + 1; ++i)
            _arrayPointer_LLR.at(i).resize(_list_size);
    }
    else {
        _arrayPointer_P.resize(_n + 1);
        for (int i = 0; i < _n + 1; ++i)
            _arrayPointer_P.at(i).resize(_list_size);
    }

    _arrayPointer_C.resize(_n + 1);
    for (int i = 0; i < _n + 1; ++i)
        _arrayPointer_C.at(i).resize(_list_size);

    _arrayPointer_Info.resize(_list_size);
#endif

	_pathMetric_LLR.resize(_list_size);

    _pathIndexToArrayIndex.resize(_n + 1);
    for (int i = 0; i < _n + 1; ++i)

#pragma HLS LOOP_TRIPCOUNT min=8 max=13
_pathIndexToArrayIndex.at(i).resize(_list_size);

    _inactiveArrayIndices.resize(_n + 1);
    for (int i = 0; i < _n + 1; ++i) {
#pragma HLS LOOP_TRIPCOUNT min=8 max=13
		_inactiveArrayIndices.at(i).reset();
    }
    _arrayReferenceCount.resize(_n + 1);
    for (int i = 0; i < _n + 1; ++i)

#pragma HLS LOOP_TRIPCOUNT min=8 max=13
_arrayReferenceCount.at(i).resize(_list_size);

#if 0
	for (uint16_t s = 0; s < _list_size; ++s) {
        _arrayPointer_Info.at(s) = new uint8_t[_block_length]();
        for (uint16_t lambda = 0; lambda < _n + 1; ++lambda) {
            if (_llr_based_computation) {
                _arrayPointer_LLR.at(lambda).at(s) = new dtype[(1 << (_n - lambda))]();
            }
            else {
                _arrayPointer_P.at(lambda).at(s) = new dtype[2 * (1 << (_n - lambda))]();
            }
            _arrayPointer_C.at(lambda).at(s) = new uint8_t[2 * (1 << (_n - lambda))]();
            _arrayReferenceCount.at(lambda).at(s) = 0;
            _inactiveArrayIndices.at(lambda).push(s);
        }
    }
#endif // 0

#if 1
	uint32_t ptr = 0;
	for (uint16_t s = 0; s < _list_size; ++s) {
#pragma HLS LOOP_TRIPCOUNT min=1 max=32
		for (uint16_t lambda = 0; lambda < _n + 1; ++lambda) {
#pragma HLS LOOP_TRIPCOUNT min=8 max=13

			_arrayIndex_LLR[lambda][s] = ptr;
			_arrayIndex_C[lambda][s] = ptr << 1;

			_arrayReferenceCount.at(lambda).at(s) = 0;
			_inactiveArrayIndices.at(lambda).push(s);

			ptr += (1 << (_n - lambda));
		}
		ptr++;
	}
#endif // 1

    for (uint16_t l = 0; l < _list_size; ++l) {
#pragma HLS LOOP_TRIPCOUNT min=1 max=32
        _activePath.at(l) = 0;
        _inactivePathIndices.push(l);
        _pathMetric_LLR.at(l) = 0;
    }
}

uint16_t PolarCode::assignInitialPath() {

    uint16_t  l = _inactivePathIndices.top();
    _inactivePathIndices.pop();
    _activePath.at(l) = 1;
    // Associate arrays with path index
    for (uint16_t lambda = 0; lambda < _n + 1; ++lambda) {
#pragma HLS LOOP_TRIPCOUNT min=8 max=13
        uint16_t  s = _inactiveArrayIndices.at(lambda).top();
        _inactiveArrayIndices.at(lambda).pop();
        _pathIndexToArrayIndex.at(lambda).at(l) = s;
        _arrayReferenceCount.at(lambda).at(s) = 1;
    }
    return l;
}

uint16_t PolarCode::clonePath(uint16_t l) {
    uint16_t l_p = _inactivePathIndices.top();
    _inactivePathIndices.pop();
    _activePath.at(l_p) = 1;
    _pathMetric_LLR.at(l_p) = _pathMetric_LLR.at(l);

    for (uint16_t lambda = 0; lambda < _n + 1; ++lambda ) {
#pragma HLS LOOP_TRIPCOUNT min=8 max=13
        uint16_t s = _pathIndexToArrayIndex.at(lambda).at(l);
        _pathIndexToArrayIndex.at(lambda).at(l_p) = s;
        _arrayReferenceCount.at(lambda).at(s)++;
    }
    return l_p;
}

void PolarCode::killPath(uint16_t l) {
    _activePath.at(l) = 0;
    _inactivePathIndices.push(l);
    _pathMetric_LLR.at(l) = 0;

    for (uint16_t lambda = 0; lambda < _n + 1; ++lambda ) {
#pragma HLS LOOP_TRIPCOUNT min=0 max=10
        uint16_t s = _pathIndexToArrayIndex.at(lambda).at(l);
        _arrayReferenceCount.at(lambda).at(s)--;
        if (_arrayReferenceCount.at(lambda).at(s) == 0 ) {
            _inactiveArrayIndices.at(lambda).push(s);
        }
    }
}

#if 1

uint32_t PolarCode::getArrayIndex_LLR(uint16_t lambda, uint16_t  l) {

    uint16_t  s = _pathIndexToArrayIndex.at(lambda).at(l);
    uint16_t s_p;
getArrayIndex_LLR_loop_if_1:
    if (_arrayReferenceCount.at(lambda).at(s) == 1) {
        s_p = s;
    }
    else {
        s_p = _inactiveArrayIndices.at(lambda).top();
        _inactiveArrayIndices.at(lambda).pop();
	getArrayIndex_LLR_loop_if_1_else:

        //copy
        for (uint16_t i = 0; i < (1 << (_n - lambda + 1)); i++) {
#pragma HLS LOOP_TRIPCOUNT min=40 max=1280
        	array_C[_arrayIndex_C[lambda][s_p]+i] = array_C[_arrayIndex_C[lambda][s]+i];
        }

        for (uint16_t i = 0; i < (1 << (_n - lambda)); i++) {
#pragma HLS LOOP_TRIPCOUNT min=40 max=640
        	array_LLR[_arrayIndex_LLR[lambda][s_p]+i] = array_LLR[_arrayIndex_LLR[lambda][s]+i];
        }

        _arrayReferenceCount.at(lambda).at(s)--;
        _arrayReferenceCount.at(lambda).at(s_p) = 1;
        _pathIndexToArrayIndex.at(lambda).at(l) = s_p;
    }
    return _arrayIndex_LLR[lambda][s_p];
}

uint32_t PolarCode::getArrayIndex_C(uint16_t lambda, uint16_t  l) {

    uint16_t  s = _pathIndexToArrayIndex.at(lambda).at(l);
    uint16_t s_p;
    if (_arrayReferenceCount.at(lambda).at(s) == 1) {
        s_p = s;
    }
    else {
        s_p = _inactiveArrayIndices.at(lambda).top();
        _inactiveArrayIndices.at(lambda).pop();

        //copy
        getArrayIndex_C_loop_1: for (uint16_t i = 0; i < (1 << (_n - lambda + 1)); i++) {
#pragma HLS LOOP_TRIPCOUNT min=40 max=1280
        	array_C[_arrayIndex_C[lambda][s_p]+i] = array_C[_arrayIndex_C[lambda][s]+i];
        }

        getArrayIndex_C_loop_2:for (uint16_t i = 0; i < (1 << (_n - lambda)); i++) {
#pragma HLS LOOP_TRIPCOUNT min=40 max=640
        	array_LLR[_arrayIndex_LLR[lambda][s_p]+i] = array_LLR[_arrayIndex_LLR[lambda][s]+i];
        }

        _arrayReferenceCount.at(lambda).at(s)--;
        _arrayReferenceCount.at(lambda).at(s_p) = 1;
        _pathIndexToArrayIndex.at(lambda).at(l) = s_p;
    }
    return _arrayIndex_C[lambda][s_p];
}


void PolarCode::CalcLLRLoop(uint16_t lambda, uint16_t phi) {

	// Detect start index for lambda loop

	uint16_t psi = phi;
	uint16_t lambdaStart;

	//Compute index of leading non-zero bit in phi
	for (lambdaStart = lambda; lambdaStart > 1; lambdaStart--) {
#pragma HLS LOOP_TRIPCOUNT min=7 max=12
		if (psi % 2) break;
		psi = psi >> 1;
	}

	for (uint16_t lambdaI = lambdaStart; lambdaI <= lambda; lambdaI++) {
#pragma HLS LOOP_TRIPCOUNT min=7 max=12
		psi = phi >> (lambda - lambdaI);
		CalcLLR(lambdaI, psi);

	}

}

void PolarCode::CalcLLR(uint16_t lambda, uint16_t phi) {

	for (uint16_t l = 0; l < _list_size; ++l) {
#pragma HLS LOOP_TRIPCOUNT min=1 max=32
		if (_activePath.at(l) == 0)
			continue;

		dtype * llr_lambda = &array_LLR[getArrayIndex_LLR(lambda, l)];
		dtype * llr_lambda_1 = &array_LLR[getArrayIndex_LLR(lambda - 1, l)];

		uint8_t * c_lambda = &array_C[getArrayIndex_C(lambda, l)];
		for (uint16_t beta = 0; beta < (1 << (_n - lambda)); ++beta) {
#pragma HLS LOOP_TRIPCOUNT min=40 max=1280
			if ((phi % 2) == 0) {
				if (40 > std::max(std::abs(llr_lambda_1[beta << 1]), std::abs(llr_lambda_1[(beta << 1) + 1]))) {
					llr_lambda[beta] = std::log((std::exp(llr_lambda_1[beta << 1] + llr_lambda_1[(beta << 1) + 1]) + 1) /
						(std::exp(llr_lambda_1[(beta << 1)]) + std::exp(llr_lambda_1[(beta << 1) + 1])));
				}
				else {
					llr_lambda[beta] = (dtype)((llr_lambda_1[(beta << 1)] < 0) ? -1 : (llr_lambda_1[(beta << 1)] > 0)) *
						((llr_lambda_1[(beta << 1) + 1] < 0) ? -1 : (llr_lambda_1[(beta << 1) + 1] > 0)) *
						std::min(std::abs(llr_lambda_1[(beta << 1)]), std::abs(llr_lambda_1[(beta << 1) + 1]));
				}

			}
			else {
				uint8_t  u_p = c_lambda[(beta << 1)];
				llr_lambda[beta] = (1 - (u_p << 1)) * llr_lambda_1[(beta << 1)] + llr_lambda_1[(beta << 1) + 1];
			}

		}
	}
}

void PolarCode::UpdateCLoop(uint16_t lambda, uint16_t phi) {

	// Detect start index for lambda loop

	uint16_t psi = phi;
	uint16_t lambdaI;

	//Compute index of leading non-zero bit in phi
	for (lambdaI = lambda; lambdaI > 0; lambdaI--) {
#pragma HLS LOOP_TRIPCOUNT min=7 max=12  //confirm value
		psi = psi >> 1;
		UpdateC(lambdaI, psi);
		if (psi % 2 == 0) break;
	}

}

void PolarCode::UpdateC(uint16_t lambda, uint16_t phi) {

	uint16_t psi = phi;// >> 1;
	for (uint16_t l = 0; l < _list_size; ++l) {
#pragma HLS LOOP_TRIPCOUNT min=0 max=32
		if (_activePath.at(l) == 0)
			continue;

		uint8_t *c_lambda = &array_C[getArrayIndex_C(lambda, l)];
		uint8_t *c_lambda_1 = &array_C[getArrayIndex_C(lambda-1, l)];

		for (uint16_t beta = 0; beta < (1 << (_n - lambda)); ++beta) {
#pragma HLS LOOP_TRIPCOUNT min=40 max=1280
			c_lambda_1[2 * (2 * beta) + (psi % 2)] = (uint8_t)((c_lambda[2 * beta] + c_lambda[2 * beta + 1]) % 2);
			c_lambda_1[2 * (2 * beta + 1) + (psi % 2)] = c_lambda[2 * beta + 1];
		}
	}
}

#endif // 0

void PolarCode::continuePaths_FrozenBit(uint16_t phi) {
    for (uint16_t l = 0; l < _list_size; ++ l) {
#pragma HLS LOOP_TRIPCOUNT min=1 max=32
        if (_activePath.at(l) == 0)
            continue;
		uint8_t * c_m = &array_C[getArrayIndex_C(_n, l)];

        c_m[(phi % 2)] = 0; // frozen value assumed to be zero
        dtype *llr_p = &array_LLR[getArrayIndex_LLR(_n, l)];
        _pathMetric_LLR.at(l) += std::log(1 + std::exp(-llr_p[0]));
        array_Info[l][phi] = 0;
    }
}

void PolarCode::continuePaths_UnfrozenBit(uint16_t phi) {

	hls_vector<dtype>  probForks((unsigned long)(2 * _list_size));
	hls_vector<dtype> probabilities;
	hls_vector<uint8_t>  contForks((unsigned long)(2 * _list_size));


	uint16_t  i = 0;
	for (unsigned l = 0; l < _list_size; ++l) {
#pragma HLS LOOP_TRIPCOUNT min=1 max=32
		if (_activePath.at(l) == 0) {
			probForks.at(2 * l) = NAN;
			probForks.at(2 * l + 1) = NAN;
		}
		else {
			dtype *llr_p = &array_LLR[getArrayIndex_LLR(_n, l)];
			probForks.at(2 * l) = -(_pathMetric_LLR.at(l) + std::log(1 + std::exp(-llr_p[0])));
			probForks.at(2 * l + 1) = -(_pathMetric_LLR.at(l) + std::log(1 + std::exp(llr_p[0])));

			probabilities.push_back(probForks.at(2 * l));
			probabilities.push_back(probForks.at(2 * l + 1));

			i++;
		}
	}

	uint16_t  rho = _list_size;
	if ((2 * i) < _list_size)
		rho = (uint16_t)2 * i;

	for (uint8_t l = 0; l < 2 * _list_size; ++l) {
#pragma HLS LOOP_TRIPCOUNT min=2 max=64
		contForks.at(l) = 0;
	}
 

    probabilities.sort();

    //std::sort(probabilities.begin(), probabilities.end(), std::greater<dtype>());

    dtype threshold = probabilities.at((unsigned long) (rho - 1));
    uint16_t num_paths_continued = 0;

    for (uint8_t l = 0; l < 2 * _list_size; ++l) {
#pragma HLS LOOP_TRIPCOUNT min=2 max=64
    	if (probForks.at(l) > threshold) {
            contForks.at(l) = 1;
            num_paths_continued++;
        }
        if (num_paths_continued == rho) {
            break;
        }
    }

    if  ( num_paths_continued < rho ) {
        for (uint8_t l = 0; l < 2 * _list_size; ++l) {
#pragma HLS LOOP_TRIPCOUNT min=2 max=64
        	if (probForks.at(l) == threshold) {
                contForks.at(l) = 1;
                num_paths_continued++;
            }
            if (num_paths_continued == rho) {
                break;
            }
        }
    }

    for (unsigned l = 0; l < _list_size; ++l) {
#pragma HLS LOOP_TRIPCOUNT min=1 max=32
    	if (_activePath.at(l) == 0)
            continue;
        if ( contForks.at(2 * l)== 0 && contForks.at(2 * l + 1) == 0 )
            killPath(l);
    }

    for (unsigned l = 0; l < _list_size; ++l) {
#pragma HLS LOOP_TRIPCOUNT min=1 max=32
    	if ( contForks.at(2 * l) == 0 && contForks.at(2 * l + 1) == 0 )
            continue;
		uint8_t * c_m = &array_C[getArrayIndex_C(_n, l)];

        if ( contForks.at(2 * l) == 1 && contForks.at(2 * l + 1) == 1 ) {

            c_m[(phi%2)] = 0;
            uint16_t l_p = clonePath(l);
            c_m = &array_C[getArrayIndex_C(_n, l_p)];
            c_m[(phi%2)] = 1;

            /* phi is blocklenght */
            for (uint16_t i = 0; i < phi; i++) {
#pragma HLS LOOP_TRIPCOUNT min=128 max=2048
            	array_Info[l_p][i] = array_Info[l][i];
            }
            array_Info[l][phi] = 0;
            array_Info[l_p][phi] = 1;

            dtype *llr_p = &array_LLR[getArrayIndex_LLR(_n, l)];
            _pathMetric_LLR.at(l) += std::log(1 + std::exp(-llr_p[0]));
            llr_p = &array_LLR[getArrayIndex_LLR(_n, l_p)];
            _pathMetric_LLR.at(l_p) += std::log(1 + std::exp(llr_p[0]));

        }
        else {
            if ( contForks.at(2 * l) == 1) {
                c_m[(phi%2)] = 0;
                array_Info[l][phi] = 0;
                dtype *llr_p = &array_LLR[getArrayIndex_LLR(_n, l)];
                _pathMetric_LLR.at(l) += std::log(1 + std::exp(-llr_p[0]));
            }
            else {
                c_m[(phi%2)] = 1;
                array_Info[l][phi] = 1;
                dtype *llr_p = &array_LLR[getArrayIndex_LLR(_n, l)];
                _pathMetric_LLR.at(l) += log(1 + exp(llr_p[0]));
            }
        }
    }

}

uint16_t PolarCode::findMostProbablePath(bool check_crc) {

    uint16_t  l_p = 0;
    dtype p_p1 = 0;
    dtype p_llr = 1.7e+37f;
	bool path_with_crc_pass = false;
    for (uint16_t l = 0; l < _list_size; ++l) {
#pragma HLS LOOP_TRIPCOUNT min=1 max=32
        if (_activePath.at(l) == 0)
            continue;

        if ( (check_crc) && (! crc_check(&array_Info[l][0])))
            continue;

        path_with_crc_pass = true;

        if (_pathMetric_LLR.at(l) < p_llr ) {
            p_llr = _pathMetric_LLR.at(l);
            l_p  = l;
        }

    }
	return l_p;

#ifndef DISABLE_BROKEN
	if ( path_with_crc_pass)
        return l_p;
    else
        return findMostProbablePath(false);
#endif
}


void PolarCode::create_bit_rev_order() {
    for (uint16_t i = 0; i < _block_length; ++i) {
#pragma HLS LOOP_TRIPCOUNT min=256 max=4096
        uint16_t to_be_reversed = i;
        _bit_rev_order[i] = (uint16_t) ((to_be_reversed & 1) << (_n - 1));
        for (uint8_t j = (uint8_t) (_n - 1); j; --j) {
#pragma HLS LOOP_TRIPCOUNT min=7 max=12
            to_be_reversed >>= 1;
            _bit_rev_order[i] += (to_be_reversed & 1) << (j - 1);
        }
    }
}


