#include "PolarCode.h"

uint8_t n = 11;
uint16_t info_length = (uint16_t) (1 << (n - 1));
uint16_t crc_size = 0;
dtype design_epsilon = 0.32f;
PolarCode polar_code(n, info_length, design_epsilon, crc_size);

/* XILLYBUS WRAPPER */
//static float d_gain = 0.002122;

void xillybus_wrapper(int *in, int *out)
{
#pragma HLS DATAFLOW
#pragma AP interface ap_fifo port=in
#pragma AP interface ap_fifo port=out
#pragma AP interface ap_ctrl_none port=return

	int x1, y1;
	int i = 0;
	dtype inf[MAX_BLOCK_LEN];
//#pragma HLS ARRAY_PARTITION variable=inf complete dim=1

	uint8_t outf[MAX_INFO_LEN];
//#pragma HLS ARRAY_PARTITION variable=outf complete dim=1

	uint8_t y11;

	for( i = 0; i < MAX_BLOCK_LEN; i++) {
#pragma HLS LOOP_TRIPCOUNT min=0 max=4096
		x1 = *in++;
		inf[i] = *((dtype *)&x1);
	}

	uint8_t list_size = 4;
	polar_code.decode_scl_llr(inf, list_size, outf);

	for(i = 0; i < MAX_INFO_LEN; i++) {
#pragma HLS LOOP_TRIPCOUNT min=0 max=4096

		y11 = outf[i];
		y1 = *((int *) &y11);
		*out++ = y1;
	}
}
