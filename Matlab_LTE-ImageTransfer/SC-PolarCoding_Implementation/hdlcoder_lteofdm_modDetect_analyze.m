if exist('simLTETxData', 'var') && length(simLTETxData)>1
    
    % Only get valid transmission samples
    transmitSamples = simLTETxData(simLTETxDataValid);
    
    % Make signals the same length
    N = min(length(simParams.LTErefOutput), length(transmitSamples));
    LTErefMatched = simParams.LTErefOutput(1:N);
    TransmitMatched = transmitSamples(1:N);
    

%     % Perform cell search on the input waveform  
%     enb = lteRMCDL('R.4');
%     %enb = simParams.enb;
%     [SimCellID, SimTimingOffset] = lteCellSearch(enb,simLTERxData1); 
%     %[SimCellID, SimTimingOffset] = lteCellSearch(rmc,simParams.LTErefOutput); 
%     
%     % Synchronize input signal knowing the frame timing 
%     simLTERxData1 = simLTERxData1 (1+ SimTimingOffset:end);    
%     
%     % Perform OFDM demodulation including cyclic prefix removal 
%     %SimRxgrid = lteOFDMDemodulate(rmc, simParams.LTErefOutput);
%     rxgrid = lteOFDMDemodulate(enb, simLTERxData1);
%     
%     griddims = lteResourceGridSize(enb); % Resource grid dimensions
%     L = griddims(2);                     % Number of OFDM symbols in a subframe 
% 
%     % Estimate the channel on the middle 6 RBs 
%     cec.FreqWindow = 1;
%     cec.TimeWindow = 1;
%     cec.InterpType = 'cubic';
%     cec.PilotAverage = 'UserDefined';
%     cec.InterpWinSize = 3;
%     cec.InterpWindow = 'Causal';
%     [hest, nest] = lteDLChannelEstimate(enb,cec,rxgrid);
%     
%     % Extract resource elements corresponding to the PBCH from the first
%     % subframe across all receive antennas and channel estimates
%     pbchIndices = ltePBCHIndices(enb);
%     [pbchRx, pbchHest] = lteExtractResources( ...
%         pbchIndices, rxgrid(:,1:L,:), hest(:,1:L,:,:));
%     % Decode PBCH
%     [bchBits, pbchSymbols, nfmod4, mib, enb.CellRefP] = ltePBCHDecode( ...
%         enb, pbchRx, pbchHest, nest); 
% 
%     % Parse MIB bits
%     enb = lteMIB(mib, enb);    
%     
    % Decode PCFICH
%     pcfichIndices = ltePCFICHIndices(enb);  % Get PCFICH indices
%     [pcfichRx, pcfichHest] = lteExtractResources(pcfichIndices, rxgrid, hest);
% 
%     cfiBits = ltePCFICHDecode(enb, pcfichRx, pcfichHest, nest);
%     enb.CFI = lteCFIDecode(cfiBits);        % Get CFI
%     % PDCCH demodulation. The PDCCH is now demodulated and decoded using
%     % similar resource extraction and decode functions to those shown
%     % already for BCH and CFI reception    
%     pdcchIndices = ltePDCCHIndices(enb); % Get PDCCH indices
%     [pdcchRx, pdcchHest] = lteExtractResources(pdcchIndices, rxgrid, hest);
% 
%     % Decode PDCCH
%     [dciBits, pdcchSymbols] = ltePDCCHDecode(enb, pdcchRx, pdcchHest, nest);
%     
%     % PDCCH blind search for System Information (SI) and DCI decoding. The
%     % LTE System Toolbox provides full blind search of the PDCCH to find
%     % any DCI messages with a specified RNTI, in this case the SI-RNTI.
%     pdcch = struct('RNTI', 65535);  
%     dci = ltePDCCHSearch(enb, pdcch, dciBits); % Search PDCCH for DCI


    if exist('peakCorrelationOutput', 'var')
        % Extract primary Cell ID from output signal
        pCellID = primaryCellID(end);
        
        % Extract secondary Cell ID from output signal
        sCellID = secondaryCellID(end);
        
        % Extract cell ID from output signal
        CellID = cellID(end);
        
        % Extract peak PSS correlation output from signal
        PSSpeak = peakCorrelationOutput(end);
        
        % Extract timing offset value from signal
        PSStimingOffset = timingOffset(end);
    end
    

    
    % Show outputs of OFDM modulation
    Npsd = 64*1024; % Number of points for PSD FFT
    TxInfo = lteOFDMInfo(simParams.enb);
    BW1 = char(simParams.lteBW(simParams.BW)); % Extract transmit bandwidth
    fs = TxInfo.SamplingRate/1e6;
    ff = (-Npsd/2:1:Npsd/2-1)/Npsd*fs; % Frequency vector for X-Axis of plot
    
    % Plot transmitted signal PSD and time-domain
    fig.TX = figure;
    subplot(211)
    plot(ff,10*log10(abs(fftshift(pwelch(LTErefMatched,[],[],Npsd)))));
    hold on;
    plot(ff,10*log10(abs(fftshift(pwelch(TransmitMatched,[],[],Npsd)))),'--r');
    title(sprintf('PSD of OFDM modulated LTE signal (BW = %s)', BW1));
    xlabel('Frequency (MHz)');
    ylabel('PSD (dB)');
    legend('LST reference','HDL Implementation','Location', 'south');
    subplot(212)
    HDLError = abs(LTErefMatched-TransmitMatched);
    plotRange = 128:256;
    plot(plotRange,abs(LTErefMatched(plotRange)));
    hold on;
    plot(plotRange,abs(TransmitMatched(plotRange)),'--r')
    hold on;
    plot(plotRange,HDLError(plotRange),'k');
    legend('LST reference', 'HDL Implementation', 'Difference');
    title('Output of OFDM modulation');
    xlabel('Time [n]');
    ylabel('Amplitude');
    axis([plotRange(1) plotRange(end) -0.02 max(real(TransmitMatched(plotRange)))+0.06]);
    
    % Plot frequency estimate
    fig.FRQEST = figure;
    subcarrierSpacing = 15000;
    plot(subcarrierSpacing * freqEst);
    title('Frequency estimate');
    xlabel('Time [n]');
    ylabel('frequency estimate (Hz)');
    
    % Plot cross-correlation output for detected PSS sequence
%     fig.PSS = figure;
    plot(correlationOutput);
    hold on;
    plot(thresholdOut,'-r');
    title(sprintf('Cross-Correlation of received signal with PSS %d', pCellID));
    legend('Cross-Correlation', 'Threshold value');
    xlabel('Time [n]');
    ylabel('Power');
    
    % Plot correlation output for SSS sequences
    fig.SSS = figure;
    groups = 0:167;
    SSSData = simLTERxData(simLTERxDataValid);
    maxPeak = max(SSSData)+10;
    SSSData0 = SSSData(1:2:end);
    SSSData5 = SSSData(2:2:end);
    SSSPlotLength = min(length(groups),length(SSSData0));
    SSSPeaks0 = SSSData0(1:SSSPlotLength);
    GroupsPlot = groups(1:SSSPlotLength);
    subplot(211); plot(GroupsPlot,SSSPeaks0);
    title('Correlation of received signal with Subframe 0');
    xlabel('LTE cell group (SSS Sequence)');
    ylabel('Power');
    ylim([0 maxPeak]);
    
    SSSPlotLength = min(length(groups),length(SSSData5));
    SSSPeaks5 = SSSData5(1:SSSPlotLength);
    GroupsPlot = groups(1:SSSPlotLength);
    subplot(212); plot(GroupsPlot,SSSPeaks5);
    title('Correlation of received signal with Subframe 5');
    xlabel('LTE cell group (SSS Sequence)');
    ylabel('Power');
    ylim([0 maxPeak]);
    
    % Calculate EVM for LTE System Toolbox and Simulink HDL Coder
    % TX signals and output to the Diagnostic Viewer
    evm=lteEVM(LTErefMatched,TransmitMatched);
    
%     simString = {...
%         sprintf('===================================================='),...
%         sprintf('== Detected Cell ID ================================'),...
%         sprintf('===================================================='),...
%         sprintf('Detected Cell ID (position in group): %d',...
%         pCellID),...
%         sprintf('Detected Cell ID (group):             %d',...
%         sCellID),...
%         sprintf('Detected Physical layer Cell ID:      %d',...
%         CellID),...
%         sprintf('===================================================='),...
%         sprintf('== PSS Cross-Correlation Values ===================='),...
%         sprintf('===================================================='),...
%         sprintf('Peak cross-correlation power:         %0.4f',...
%         PSSpeak),...
%         sprintf('Timing offset (samples):              %d',...
%         PSStimingOffset),...
%         sprintf('===================================================='),...
%         sprintf('== HDL Implementation TX Error Vector Magnitude ===='),...
%         sprintf('===================================================='),...
%         sprintf('EVM peak:                             %0.3f%%',...
%         evm.Peak*100),...
%         sprintf('EVM RMS:                              %0.3f%%',...
%         evm.RMS*100),...
%         sprintf('===================================================='),...
%         };
%         % Set background color to white
    
%     % Output results in a 'msgbox' figure...
%     % Create figure
%     fig.Text = msgbox(simString, 'Simulation Results');
%     % Hide figure for font change and resize
%     fig.Text.Visible = 'off';
%     % Set background colour to white
%     fig.Text.Color = [1 1 1];
%     
%     % Get handle to UIControl
%     OKButton = findobj(fig.Text, 'Type', 'UIControl');
%     % Get handle to text
%     textMessage = findobj(fig.Text, 'Type', 'Text');
%     
%     % Text extent in old font
%     extent0 = textMessage.Extent;
%     % Set new font
%     set([OKButton, textMessage], 'FontName', 'Courier', 'FontSize', 12);
%     % Text extent in new font
%     extent1 = textMessage.Extent;
%     
%     % Change in extent between fonts
%     delta = extent1 - extent0;
%     % Current position of figure
%     pos = fig.Text.Position;
%     % Change size of figure
%     pos = pos + delta;
%     % Set new figure position
%     fig.Text.Position = pos;
%     % Reposition OK Button
%     OKButton.Position = OKButton.Position + [(delta(3)/2) 0 0 0];
%     
%     % Finally show figure
%     fig.Text.Visible = 'on';

    fprintf('====================================================\n');
    fprintf('== Detected Cell ID ================================\n');
    fprintf('====================================================\n');
    fprintf('Detected Cell ID (position in group): %d\n',...
    pCellID);
    fprintf('Detected Cell ID (group):             %d\n',...
    sCellID);
    fprintf('Detected Physical layer Cell ID:      %d\n',...
    CellID);
    fprintf('====================================================\n');
    fprintf('== PSS Cross-Correlation Values ====================\n');
    fprintf('====================================================\n');
    fprintf('Peak cross-correlation power:         %0.4f\n',...
    PSSpeak);
    fprintf('Timing offset (samples):              %d\n',...
    PSStimingOffset);
    fprintf('====================================================\n');
    fprintf('== HDL Implementation TX Error Vector Magnitude ====\n');
    fprintf('====================================================\n');
    fprintf('EVM peak:                             %0.3f%%\n',...
    evm.Peak*100);
    fprintf('EVM RMS:                              %0.3f%%\n',...
    evm.RMS*100);
    fprintf('====================================================\n');

    fprintf('  channelSNR = %0.5fdBs.\n', simParams.channelSNR);
    
    decodedRxDataStream = lteDemodDecode(simParams.enb, simLTERxData1);

    % Perform EVM calculation
%     if ~isempty(rxSymbols)
%         evmCalculator = comm.EVM();
%         evmCalculator.MaximumEVMOutputPort = true;
%         [evm.RMS,evm.Peak] = evmCalculator(txSymbols, rxSymbols);
%         fprintf('  EVM peak = %0.3f%%\n',evm.Peak);
%         fprintf('  EVM RMS  = %0.3f%%\n',evm.RMS);
%     else
%         fprintf('  No transport blocks decoded.\n');
%     end

    % Perform bit error rate (BER) calculation
    bitErrorRate = comm.ErrorRate;
    % ber = bitErrorRate(decodedRxDataStream(1:length(simParams.txData)), simParams.txData);
    % ber = bitErrorRate(decodedRxDataStream, simParams.txData(1:length(decodedRxDataStream)));
    numBits = min(length(decodedRxDataStream), length(simParams.txData));
    ber = bitErrorRate(decodedRxDataStream(1:numBits), simParams.txData(1:numBits));
    fprintf('  Bit Error Rate (BER) = %0.5f.\n', ber(1));
    fprintf('  Number of bit errors = %d.\n', ber(2));
    fprintf('  Number of transmitted bits = %d.\n',numBits);

    %PolarSnr = [PolarSnr simParams.channelSNR]
    %PolarBer = [PolarBer ber]    
%     % Recreate image from received data
%     fprintf('\nConstructing image from received data.\n');
%     str = reshape(sprintf('%d',decodedRxDataStream(1:length(simParams.txData))), 8, []).';
%     decdata = uint8(bin2dec(str));
%     receivedImage = reshape(decdata,imsize);
% 
%     % Plot receive image
%     if exist('imFig', 'var') && ishandle(imFig) % If TX figure is open
%         figure(imFig); subplot(212);
%     else
%         figure; subplot(212);
%     end
%     imshow(receivedImage);
%     title(sprintf('Received Image: %dx%d Antenna Configuration',txsim.NTxAnts, rxsim.NRxAnts));
    
end;

% Clean up workspace variables
clear   N BW1 f ff LTErefMatched TransmitMatched evm cellids corr ...
    idGroup idGroups ids SSSref receivedBlock ...
    enb transmitSamples simString groups fs ...
    primryCellID peakCorrelationOutput timingOffset Npsd textMessage ...
    OKButton extent0 extent1 pos delta HDLError plotRange cellID TxInfo...
    secondaryCellID sssMaxPow SSSData SSSPlotLength SSSPeaks GroupsPlot ...
    SSSData0 SSSData5 SSSPeaks0 SSSPeaks5;
