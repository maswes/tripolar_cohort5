% User defined parameters --- configure the same as sdrTransmitter
function decodedRxDataStream = lteDemodDecode(enb, rxWaveform)

    % Setup handle for image plot
%     if ~exist('imFig', 'var') || ~ishandle(imFig)
%         imFig = figure;
%         imFig.NumberTitle = 'off';
%         imFig.Name = 'Image Plot';
%         imFig.Visible = 'off';
%     else
%         clf(imFig); % Clear figure
%         imFig.Visible = 'off';
%     end

    % Setup handle for channel estimate plots
    if ~exist('hhest', 'var') || ~ishandle(hhest)
        hhest = figure('Visible','Off');
        hhest.NumberTitle = 'off';
        hhest.Name = 'Channel Estimate';
    else
        clf(hhest); % Clear figure
        hhest.Visible = 'off';
    end

    % Setup Spectrum viewer
    spectrumScope = dsp.SpectrumAnalyzer( ...
        'SpectrumType',    'Power density', ...
        'SpectralAverages', 10, ...
        'YLimits',         [-150 -60], ...
        'Title',           'Received Baseband LTE Signal Spectrum', ...
        'YLabel',          'Power spectral density');

    % Setup the constellation diagram viewer for equalized PDSCH symbols
    constellation = comm.ConstellationDiagram('Title','Equalized PDSCH Symbols',...
                                    'ShowReferenceConstellation',false);

    TxInfo = lteOFDMInfo(enb);

    rxsim = struct;
    % rxsim.RadioFrontEndSampleRate = sdrTransmitter.BasebandSampleRate; % Configure for same sample rate
                                                           % as transmitter
    % rxsim.RadioCenterFrequency = txsim.DesiredCenterFrequency;
    % rxsim.NRxAnts = txsim.NTxAnts;
    % rxsim.FramesPerBurst = txsim.TotFrames+1; % Number of LTE frames to capture in each burst.
                                              % Capture 1 more LTE frame than transmitted to
                                              % allow for timing offset wraparound...
    rxsim.RadioFrontEndSampleRate = TxInfo.SamplingRate;
    rxsim.NRxAnts = 1;
    %NRadioFrames = 4;
    NRadioFrames = enb.TotSubframes/10;
    rxsim.FramesPerBurst = NRadioFrames+1;

    rxsim.numBurstCaptures = 1; % Number of bursts to capture

    % Derived parameters
    samplesPerFrame = 10e-3*rxsim.RadioFrontEndSampleRate; % LTE frames period is 10 ms

    % Channel estimation configuration structure
    cec.PilotAverage = 'UserDefined';  % Type of pilot symbol averaging
    cec.FreqWindow = 9;                % Frequency window size in REs
    cec.TimeWindow = 9;                % Time window size in REs
    cec.InterpType = 'Cubic';          % 2D interpolation type
    cec.InterpWindow = 'Centered';     % Interpolation window type
    cec.InterpWinSize = 3;             % Interpolation window size

    enbDefault = enb;

    while rxsim.numBurstCaptures
        % Set default LTE parameters
        enb = enbDefault;

%         % SDR Capture
%         fprintf('\nStarting a new RF capture.\n\n')        
%         
%         len = 0;
%         for frame = 1:rxsim.FramesPerBurst
%             while len == 0
%                 % Store one LTE frame worth of samples
%                 [data,len,lostSamples] = sdrReceiver();
%                 burstCaptures(:,:,frame) = data;
%             end
%             if lostSamples
%                 warning('Dropped samples');
%             end
%             len = 0;
%         end
%         if rxsim.NRxAnts == 2
%             rxWaveform = reshape(permute(burstCaptures,[1 3 2]), ...
%                             rxsim.FramesPerBurst*samplesPerFrame,rxsim.NRxAnts);
%             spectrumScope.ShowLegend = true; % Turn on legend for spectrum analyzer
%             spectrumScope.ChannelNames = {'SDR Channel 1','SDR Channel 2'};
%         else
%             rxWaveform = burstCaptures(:);
%         end

        % Show power spectral density of captured burst
        spectrumScope.SampleRate = rxsim.RadioFrontEndSampleRate;
        spectrumScope(rxWaveform);

        % Perform frequency offset correction for known cell ID
        frequencyOffset = lteFrequencyOffset(enb,rxWaveform);
        rxWaveform = lteFrequencyCorrect(enb,rxWaveform,frequencyOffset);
        fprintf('\nCorrected a frequency offset of %i Hz.\n',frequencyOffset)

        % Perform the blind cell search to obtain cell identity and timing offset
        %   Use 'PostFFT' SSS detection method to improve speed
        cellSearch.SSSDetection = 'PostFFT'; cellSearch.MaxCellCount = 1;
        [NCellID,frameOffset] = lteCellSearch(enb,rxWaveform,cellSearch);
        fprintf('Detected a cell identity of %i.\n', NCellID);
        enb.NCellID = NCellID; % From lteCellSearch

        % Sync the captured samples to the start of an LTE frame, and trim off
        % any samples that are part of an incomplete frame.
        rxWaveform = rxWaveform(frameOffset+1:end,:);
        tailSamples = mod(length(rxWaveform),samplesPerFrame);
        rxWaveform = rxWaveform(1:end-tailSamples,:);
        enb.NSubframe = 0;
        fprintf('Corrected a timing offset of %i samples.\n',frameOffset)

        % OFDM demodulation
        rxGrid = lteOFDMDemodulate(enb,rxWaveform);

        % Perform channel estimation for 4 CellRefP as currently we do not
        % know the CellRefP for the eNodeB.
        [hest,nest] = lteDLChannelEstimate(enb,cec,rxGrid);

        sfDims = lteResourceGridSize(enb);
        Lsf = sfDims(2); % OFDM symbols per subframe
        LFrame = 10*Lsf; % OFDM symbols per frame
        numFullFrames = length(rxWaveform)/samplesPerFrame;

        rxDataFrame = zeros(sum(enb.PDSCH.TrBlkSizes(:)),numFullFrames);
        recFrames = zeros(numFullFrames,1);
        rxSymbols = []; txSymbols = [];

        % For each frame decode the MIB, PDSCH and DL-SCH
        for frame = 0:(numFullFrames-1)
            fprintf('\nPerforming DL-SCH Decode for frame %i of %i in burst:\n', ...
                frame+1,numFullFrames)

            % Extract subframe #0 from each frame of the received resource grid
            % and channel estimate.
            enb.NSubframe = 0;
            rxsf = rxGrid(:,frame*LFrame+(1:Lsf),:);
            hestsf = hest(:,frame*LFrame+(1:Lsf),:,:);

            % PBCH demodulation. Extract resource elements (REs)
            % corresponding to the PBCH from the received grid and channel
            % estimate grid for demodulation.
            enb.CellRefP = 4;
            pbchIndices = ltePBCHIndices(enb);
            [pbchRx,pbchHest] = lteExtractResources(pbchIndices,rxsf,hestsf);
            [~,~,nfmod4,mib,CellRefP] = ltePBCHDecode(enb,pbchRx,pbchHest,nest);

            % If PBCH decoding successful CellRefP~=0 then update info
            if ~CellRefP
                fprintf('  No PBCH detected for frame.\n');
                continue;
            end
            enb.CellRefP = CellRefP; % From ltePBCHDecode

            % Decode the MIB to get current frame number
            enb = lteMIB(mib,enb);

            % Incorporate the nfmod4 value output from the function
            % ltePBCHDecode, as the NFrame value established from the MIB
            % is the system frame number modulo 4.
            enb.NFrame = enb.NFrame+nfmod4;
            fprintf('  Successful MIB Decode.\n')
            fprintf('  Frame number: %d.\n',enb.NFrame);

            % The eNodeB transmission bandwidth may be greater than the
            % captured bandwidth, so limit the bandwidth for processing
            enb.NDLRB = min(enbDefault.NDLRB,enb.NDLRB);

            % Store received frame number
            recFrames(frame+1) = enb.NFrame;

            % Process subframes within frame (ignoring subframe 5)
            for sf = 0:9
                if sf~=5 % Ignore subframe 5
                    % Extract subframe
                    enb.NSubframe = sf;
                    rxsf = rxGrid(:,frame*LFrame+sf*Lsf+(1:Lsf),:);

                    % Perform channel estimation with the correct number of CellRefP
                    [hestsf,nestsf] = lteDLChannelEstimate(enb,cec,rxsf);

                    % PCFICH demodulation. Extract REs corresponding to the PCFICH
                    % from the received grid and channel estimate for demodulation.
                    pcfichIndices = ltePCFICHIndices(enb);
                    [pcfichRx,pcfichHest] = lteExtractResources(pcfichIndices,rxsf,hestsf);
                    [cfiBits,recsym] = ltePCFICHDecode(enb,pcfichRx,pcfichHest,nestsf);

                    % CFI decoding
                    enb.CFI = lteCFIDecode(cfiBits);

                    % Get PDSCH indices
                    [pdschIndices,pdschIndicesInfo] = ltePDSCHIndices(enb, enb.PDSCH, enb.PDSCH.PRBSet);
                    [pdschRx, pdschHest] = lteExtractResources(pdschIndices, rxsf, hestsf);

                    % Perform deprecoding, layer demapping, demodulation and
                    % descrambling on the received data using the estimate of
                    % the channel
                    [rxEncodedBits, rxEncodedSymb] = ltePDSCHDecode(enb,enb.PDSCH,pdschRx,...
                                                   pdschHest,nestsf);

                    % Append decoded symbol to stream
                    rxSymbols = [rxSymbols; rxEncodedSymb{:}]; %#ok<AGROW>

                    % Transport block sizes
                    outLen = enb.PDSCH.TrBlkSizes(enb.NSubframe+1);

                    % Decode DownLink Shared Channel (DL-SCH)
                    [decbits{sf+1}, blkcrc(sf+1)] = lteDLSCHDecode(enb,enb.PDSCH,...
                                                     outLen, rxEncodedBits);  %#ok<SAGROW>
                    % [decbits{sf+1}, blkcrc(sf+1)] = lteDLSCHDecodeCustom(enb,enb.PDSCH,...
                    %                                outLen, rxEncodedBits);  %#ok<SAGROW>

                    % Recode transmitted PDSCH symbols for EVM calculation
                    %   Encode transmitted DLSCH
                    txRecode = lteDLSCH(enb,enb.PDSCH,pdschIndicesInfo.G,decbits{sf+1});
                    %   Modulate transmitted PDSCH
                    txRemod = ltePDSCH(enb, enb.PDSCH, txRecode);
                    %   Decode transmitted PDSCH
                    [~,refSymbols] = ltePDSCHDecode(enb, enb.PDSCH, txRemod);
                    %   Add encoded symbol to stream
                    txSymbols = [txSymbols; refSymbols{:}]; %#ok<AGROW>

                    release(constellation); % Release previous constellation plot
                    constellation(rxEncodedSymb{:}); % Plot current constellation
                end
            end

            % Reassemble decoded bits
            fprintf('  Retrieving decoded transport block data.\n');
            rxdata = [];
            for i = 1:length(decbits)
                if i~=6 % Ignore subframe 5
                    rxdata = [rxdata; decbits{i}{:}]; %#ok<AGROW>
                end
            end

            % Store data from receive frame
            rxDataFrame(:,frame+1) = rxdata;

            % Plot channel estimate between CellRefP 0 and the receive antennae
            focalFrameIdx = frame*LFrame+(1:LFrame);
            figure(hhest);
            hhest.Visible = 'On';
            surf(abs(hest(:,focalFrameIdx,1,1)));
            shading flat;
            xlabel('OFDM symbol index');
            ylabel('Subcarrier index');
            zlabel('Magnitude');
            title('Estimate of Channel Magnitude Frequency Repsonse');
        end
        rxsim.numBurstCaptures = rxsim.numBurstCaptures-1;
    end
    % Release both transmit and receive objects once reception is complete
    % release(sdrTransmitter);
    % release(sdrReceiver);


    % Result Qualification and Display
    % The bit error rate (BER) between the transmitted and received data is
    % calculated to determine the quality of the received data. The received
    % data is then reformed into an image and displayed.

    % Determine index of first transmitted frame (lowest received frame number)
    [~,frameIdx] = min(recFrames);

    fprintf('\nRecombining received data blocks:\n');

    decodedRxDataStream = zeros(length(rxDataFrame(:)),1);
    frameLen = size(rxDataFrame,1);
    % Recombine received data blocks (in correct order) into continuous stream
    for n=1:numFullFrames
        currFrame = mod(frameIdx-1,numFullFrames)+1; % Get current frame index
        decodedRxDataStream((n-1)*frameLen+1:n*frameLen) = rxDataFrame(:,currFrame);
        frameIdx = frameIdx+1; % Increment frame index
    end

    % Perform EVM calculation
    if ~isempty(rxSymbols)
        evmCalculator = comm.EVM();
        evmCalculator.MaximumEVMOutputPort = true;
        [evm.RMS,evm.Peak] = evmCalculator(txSymbols, rxSymbols);
        fprintf('  EVM peak = %0.3f%%\n',evm.Peak);
        fprintf('  EVM RMS  = %0.3f%%\n',evm.RMS);
    else
        fprintf('  No transport blocks decoded.\n');
    end
% 
%     % Perform bit error rate (BER) calculation
%     bitErrorRate = comm.ErrorRate;
%     err = bitErrorRate(decodedRxDataStream(1:length(trData)), trData);
%     fprintf('  Bit Error Rate (BER) = %0.5f.\n', err(1));
%     fprintf('  Number of bit errors = %d.\n', err(2));
%     fprintf('  Number of transmitted bits = %d.\n',length(trData));
% 
%     % Recreate image from received data
%     fprintf('\nConstructing image from received data.\n');
%     str = reshape(sprintf('%d',decodedRxDataStream(1:length(trData))), 8, []).';
%     decdata = uint8(bin2dec(str));
%     receivedImage = reshape(decdata,imsize);
% 
%     % Plot receive image
%     if exist('imFig', 'var') && ishandle(imFig) % If TX figure is open
%         figure(imFig); subplot(212);
%     else
%         figure; subplot(212);
%     end
%     imshow(receivedImage);
%     title(sprintf('Received Image: %dx%d Antenna Configuration',txsim.NTxAnts, rxsim.NRxAnts));

end