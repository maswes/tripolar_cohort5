function simParams = hdlcoder_lteofdm_modDetecthdl_init(lteParams)
    %% Create initialization variables for HDL model
    % Assign input parameters
    simParams = lteParams;
    
    % Possible CP lengths
    CPLengths = [160 144]*simParams.nFFT/2048;
    windowing = simParams.windowing;
    
    % Take snapshot of windowing length used to generate LTE reference
    % signal
    tempWindow = windowing;
    
    % LUT windowing value for Simulink model as we can't initialize some values
    % to zero (RAMs, delays, etc.)
    % Ensure windowing value is an even integer >= 4
    % Ensure windowing value is no greater than max CP length
    if (windowing > 3 && mod(windowing,2)==0 )
        LUTwindowing = windowing;
    else
        if windowing == 0 % If no windowing is to be applied
            LUTwindowing = 4; % HDL implementation
        else
            if windowing < 4
                warning('Minimum number of windowing samples = 4. Default value (4) has been set.');
                windowing = 4;
                LUTwindowing = windowing;
            end;
            if ~(mod(windowing,1)==0)
                LUTwindowing = ceil(windowing);
                if~(mod(windowing,2)==0)
                    LUTwindowing = LUTwindowing+1;
                end;
                warning('Number of windowing samples (%0.3f) must be an integer value. Rounding to nearest even integer value (%d).\n',windowing,LUTwindowing);
                windowing = LUTwindowing;
            end;
            if ~(mod(windowing,2)==0)
                LUTwindowing = ceil(windowing+1);
                warning('Number of windowing samples was odd (%d). Rounded up to nearest even integer number (%d).\n',windowing,LUTwindowing);
                windowing = LUTwindowing;
            end;
        end;
    end;
    
    % Assign value for LUT window
    simParams.LUTwindowing = LUTwindowing;
    
    % If windowing value has changed, we need to regenerate the LTE
    % reference signal
    if (windowing ~= tempWindow)
        simParams.enb.Windowing = windowing;
        % Create OFDM modulated LTE transmit signal as a reference
        simParams.LTErefOutput = lteOFDMModulate(simParams.enb,simParams.LTEGrid);
        simParams.windowing = windowing;
    end;
    
    % Number of samples per radio frame (OFDM symbols plus cyclic prefix
    % lengths)
    simParams.NSamplesPerRadioFrame = 20*(CPLengths(1)+simParams.nFFT+6*(CPLengths(2)+simParams.nFFT));
    
    % Generate RC Window samples
    n = LUTwindowing;
    simParams.RCWindow = 0.5*(1-sin(pi*(n+1-2*(1:n))/(2*n)));
    
    % Calculate three possible time-domain PSS sequences
    simParams.PSS = complex(zeros(128,3));
    for i=0:2,
        simParams.PSS(:,i+1) = timeDomainPSS(i,128);
    end;
    
    % Generate a 62x1008 table of all possible SSS sequences
    % The actual table size is 64x504 so that dimension 1 is a power of 2
    
    % Subframe 0
    enb = struct('NCellID',1','NSubframe',0);
    possibleSSS0 = zeros(64,504);
    for i=1:504
        enb.NCellID = i-1; % zero-indexed
        % Use LTE System Toolbox to generate SSS sequence
        SSSseq = lteSSS(enb);
        % Only store real-valued samples
        possibleSSS0(1:62,i) = real(fftshift(SSSseq));
    end
    
    % Subframe 5.
    enb = struct('NCellID',1','NSubframe',5);
    possibleSSS5 = zeros(64,504);
    for i=1:504
        enb.NCellID = i-1; % zero-indexed
        % Use LTE System Toolbox to generate SSS sequence
        SSSseq = lteSSS(enb);
        % Only store real-valued samples
        possibleSSS5(1:62,i) = real(fftshift(SSSseq));
    end
    
    simParams.possibleSSS = [possibleSSS0 possibleSSS5];
    
    % Threshold Gain
    simParams.thresholdGain = ceil(length(simParams.PSS(:,1))/6);
    
    % Stop time
    simParams.stopTime = 25e-3;
    
    
function Pt = timeDomainPSS(NCellID,nFFT)
    % Function to generate time-domain PSS sequence from frequency-domain ZC sequence
    % INPUTS:
    %         NCellID:  Cell ID {0,1,2}
    %         nFFT:     FFT length {128, 256, 512, 1024, 2048}
    % OUTPUTS
    %         Pt:       Time-domain PSS signal
    
    % Number of zeros to pad either side of the PSS sequence
    pad = (nFFT - 62)/2;
    
    % Map PSS into central subcarriers (use ltePSS to generate
    % frequency-domain PSS sequence
    pFF = [zeros(pad,1); ltePSS(struct('NCellID',NCellID,'NSubframe',0,...
        'DuplexMode','FDD')); zeros(pad,1)];
    
    % Shift second half of PSS to free DC subcarrier
    pFF(nFFT/2+2:nFFT/2+32) = pFF(nFFT/2+1:nFFT/2+31);
    
    % NULL DC subcarrier
    pFF(nFFT/2+1) = 0;
    
    % Create time-domain PSS seq by performing shifted IFFT
    Pt = ifft(fftshift(pFF));
    
    %--------------------------------------------------------------------------
